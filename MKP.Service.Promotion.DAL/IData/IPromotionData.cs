﻿using System;
using System.Collections.Generic;
using System.Text;
using MKP.Service.Promotion.DTO.Promotion;

namespace MKP.Service.Promotion.DAL.IData
{
    public interface IPromotionData : IDisposable
    {
        // PromotionRequestEntity Promotions(short PromotionCategory, short PromotionType, int MaxLimit, short DisplayType, int PageIndex, int PageSize);
        PromotionList ListPublishPromotion(PromotionRequestEntity request);
    }
}
