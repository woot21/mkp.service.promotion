﻿using MKP.Service.Promotion.DTO.Promotion;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKP.Service.Promotion.DAL.IData
{
    public interface IBannerData : IDisposable
    {
        BannerList ListPublicBanner(BannerRequestEntity reuest);
    }
}
