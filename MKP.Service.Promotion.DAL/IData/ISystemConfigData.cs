﻿
using MKP.Service.Promotion.DTO;
//using Trove.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Service.Promotion.DAL.IData
{
    public interface ISystemConfigData : IDisposable
    {
        SystemConfigEntity GetSystemConfig(string moduleCode, string configName);
        List<string> ListModuleCode();
        SystemConfigList ListSystemConfig(string moduleCode, string configName, int pageIndex, int pageSize, string orderBy);
        SystemConfigList ListSystemConfigByModuleCode(string moduleCode);
        bool AddSystemConfig(SystemConfigEntity config);
        bool UpdateSystemConfig(SystemConfigEntity config);
        bool DeleteSystemConfig(string moduleCode, string configName);
    }
}
