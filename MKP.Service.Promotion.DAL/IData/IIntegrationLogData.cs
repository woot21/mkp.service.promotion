﻿using MKP.Service.Promotion.DTO.Log;
using MKP.Service.Promotion.DTO.Promotion;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKP.Service.Promotion.DAL.IData
{
    public interface IIntegrationLogData : IDisposable
    {
        ResultEntity AddIntegrationLog(IntegrationLogEntity request);
    }
}
