﻿using System;
using MKP.Service.Promotion.DTO;

namespace MKP.Service.Promotion.DAL.IData
{
    public interface IErrorMessageData : IDisposable
    {
        ErrorMessageList ListErrorMessage();

        ErrorMessageEntity GetErrorMessageByGlobalCode(string globalCode);
        ErrorMessageEntity GetErrorMessageByOriginalCode(string originalCode);
    }
}
