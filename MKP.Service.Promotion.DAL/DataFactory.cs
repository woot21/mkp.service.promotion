﻿using PSC.Frameworks.Utility.Core;
using System;
using MKP.Service.Promotion.DAL.Data;
using MKP.Service.Promotion.DAL.IData;

namespace MKP.Service.Promotion.DAL.Factory
{
    public sealed class DataFactory
    {
        public static IData.IErrorMessageData GetErrorMessageData()
        {
            try
            {
                return (IData.IErrorMessageData)Reflection.CreateObject<Data.ErrorMessageData>();
            }
            catch (Exception ex)
            {
                throw new DALException("Create object GetErrorMessageData occurs an error.", ex, false);
            }
        }

        public static IData.ISystemConfigData GetSystemConfigData()
        {
            try
            {
                //WalletData test = new WalletData(iConfig);
                //ISystemConfigData data = new SystemConfigData();
                //return data;

                return (IData.ISystemConfigData)Reflection.CreateObject<Data.SystemConfigData>();
            }
            catch (Exception ex)
            {
                throw ex;
                //throw new DALException("Create object GetWalletData() occurs an error.", ex, false);
            }
        }

        //beem
        public static IPromotionData GetPromotionData()
        {
            try
            {
                IPromotionData data = new PromotionData();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IBannerData GetBannerData()
        {
            try
            {
                IBannerData data = new BannerData();
                return data;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        //

        public static IData.IIntegrationLogData GetIntegrationLogData()
        {
            try
            {
                return (IData.IIntegrationLogData)Reflection.CreateObject<Data.IntegrationLogData>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}