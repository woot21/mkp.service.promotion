﻿
//using MKP.Service.Promotion.DAL.Base;
//using Trove.DTO;
using PSC.Frameworks.BSL.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MKP.Service.Promotion.DTO;

namespace MKP.Service.Promotion.DAL.Data
{
    public class SystemConfigData : Base.BaseData, IData.ISystemConfigData
    {
        #region *** Members section ***

        #region [ Private members ]

        #region Constant package store procedure name
        private const string SP_GET_SYSTEM_CONFIG_BY_CONFIG_NAME = "[TRV_MST_SYSTEM_CONFIG.SP_GET_SYSTEM_CONFIG_BY_CONFIG_NAME]";
        private const string SP_LIST_MODULE_CODE = "[TRV_MST_SYSTEM_CONFIG.SP_LIST_MODULE_CODE]";
        private const string SP_LIST_SYSTEM_CONFIG = "[TRV_MST_SYSTEM_CONFIG.SP_LIST_SYSTEM_CONFIG]";
        private const string SP_LIST_SYSTEM_CONFIG_BY_MODULE_CODE = "[TRV_MST_SYSTEM_CONFIG.SP_LIST_SYSTEM_CONFIG_BY_MODULE_CODE]";
        private const string SP_ADD_SYSTEM_CONFIG = "[TRV_MST_SYSTEM_CONFIG.SP_ADD_SYSTEM_CONFIG]";
        private const string SP_UPDATE_SYSTEM_CONFIG = "[TRV_MST_SYSTEM_CONFIG.SP_UPDATE_SYSTEM_CONFIG]";
        private const string SP_DELETE_SYSTEM_CONFIG = "[TRV_MST_SYSTEM_CONFIG.SP_DELETE_SYSTEM_CONFIG]";

        #endregion

        #endregion

        #endregion

        SqlConnection connection;
        Dictionary<string, string> _config;
        public Dictionary<string, string> Config { get => _config; set => _config = value; }
        public SystemConfigData()
        : base(typeof(SystemConfigData))
        {
        }

        //public SystemConfigData(Dictionary<string,string> iConfig)
        //{
        //    try
        //    {
        //        Config = iConfig;
        //        //if (!string.IsNullOrEmpty(Config["ConnectionString"]))
        //        //{
        //        //    connection = new SqlConnection(Config["ConnectionString"]);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}

        public SystemConfigEntity GetSystemConfig(string moduleCode, string configName)
        {
            try
            {
                return CBO.FillObject<SystemConfigEntity>(this.Database.ExecuteReader(SP_GET_SYSTEM_CONFIG_BY_CONFIG_NAME,
                    (moduleCode == null ? Convert.DBNull : moduleCode),
                    (configName == null ? Convert.DBNull : configName)));

                #region Raw code for direct test
                //using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
                //{
                //    using (var command = new SqlCommand(SP_GET_SYSTEM_CONFIG_BY_CONFIG_NAME, conn))
                //    {
                //        command.CommandType = CommandType.StoredProcedure;
                //        command.Parameters.AddWithValue("@MODULE_CODE", moduleCode);
                //        command.Parameters.AddWithValue("@CONFIG_NAME", configName);
                //        using (var adapter = new SqlDataAdapter(command))
                //        {
                //            conn.Open();

                //            DataSet dsResults = new DataSet();
                //            adapter.Fill(dsResults);
                //            DataTable dsTBL0 = dsResults.Tables[0];
                //            if(dsTBL0.Rows.Count > 0)
                //            {
                //                result = new SystemConfigEntity();
                //                DataRow rowdt = dsTBL0.Rows[0];
                //                #region mapping: dataset to entity

                //                result.moduleCode = SettingDataString(rowdt["MODULE_CODE"]);
                //                result.configName = SettingDataString(rowdt["CONFIG_NAME"].ToString());
                //                result.configValue = SettingDataString(rowdt["CONFIG_VALUE"]);
                //                result.configDescription = SettingDataString(rowdt["CONFIG_DESC"]);
                //                result.readOnly = SettingDataBool(rowdt["READ_ONLY"]);
                //                result.regularExpression = SettingDataString(rowdt["REGULAR_EXP"]);
                //                #endregion
                //            }
                //            else
                //            {
                //                result = null;
                //            }
                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                throw new DALException("GetSystemConfig occurs an error.[" + ex.Message + "]", true);
            }
        }

        public List<string> ListModuleCode()
        {
            try
            {
                List<string> result = new List<string>();
                DataSet dsResults = this.Database.ExecuteDataSet(SP_LIST_MODULE_CODE);

                if ((dsResults != null) && (dsResults.Tables[0].Rows.Count > 0))
                {
                    if (dsResults.Tables[0].Rows[0][0] != System.DBNull.Value)
                    {
                        foreach (DataRow item in dsResults.Tables[0].Rows)
                        {
                            result.Add(SettingDataString(item[0]));
                        }
                        //result.AddRange(CBO.FillCollection<string>(dsResults.Tables[0]));
                    }
                }
                #region Raw code for direct test
                //using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
                //{
                //    using (var command = new SqlCommand(SP_LIST_MODULE_CODE, conn))
                //    {
                //        command.CommandType = CommandType.StoredProcedure;
                //        using (var adapter = new SqlDataAdapter(command))
                //        {
                //            conn.Open();

                //            DataSet dsResults = new DataSet();
                //            adapter.Fill(dsResults);
                //            DataTable dsTBL0 = dsResults.Tables[0];
                //            for (int r = 0; r < dsTBL0.Rows.Count; r++)
                //            {
                //                SystemConfigEntity systemConfigEntity = new SystemConfigEntity();
                //                DataRow rowdt = dsTBL0.Rows[r];
                //                #region mapping: dataset to entity

                //                result.Add(SettingDataString(rowdt["MODULE_CODE"]));
                //                #endregion
                //            }
                //        }
                //    }
                //}
                #endregion
                return result;
            }
            catch (Exception ex)
            {
                throw new DALException("ListModuleCode occurs an error.[" + ex.Message + "]", true);
            }
        }

        public SystemConfigList ListSystemConfig(string moduleCode, string configName, int pageIndex, int pageSize, string orderBy)
        {
            try
            {
                SystemConfigList result = new SystemConfigList();
                DataSet dsResults = this.Database.ExecuteDataSet(SP_LIST_SYSTEM_CONFIG,
                   (string.IsNullOrEmpty(moduleCode)) ? Convert.DBNull : moduleCode,
                   (string.IsNullOrEmpty(configName)) ? Convert.DBNull : configName,
                   (pageIndex == null) ? Convert.DBNull : pageIndex,
                   (pageSize == null) ? Convert.DBNull : pageSize,
                   (string.IsNullOrEmpty(orderBy)) ? Convert.DBNull : orderBy);

                if ((dsResults != null) && (dsResults.Tables[0].Rows.Count > 0))
                {
                    if (dsResults.Tables[0].Rows[0][0] != System.DBNull.Value)
                    {
                        result.configs.AddRange(CBO.FillCollection<SystemConfigEntity>(dsResults.Tables[0]));
                        result.TotalRecord = Convert.ToInt64(((dsResults.Tables[1].Rows[0][0] == Convert.DBNull) ? 1 : dsResults.Tables[1].Rows[0][0]));
                    }
                }
                #region Raw code for direct test
                //using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
                //{
                //    using (var command = new SqlCommand(SP_LIST_SYSTEM_CONFIG, conn))
                //    {
                //        command.CommandType = CommandType.StoredProcedure;
                //        command.Parameters.AddWithValue("@MODULE_CODE", string.IsNullOrEmpty(moduleCode) ? Convert.DBNull : moduleCode);
                //        command.Parameters.AddWithValue("@CONFIG_NAME", string.IsNullOrEmpty(configName) ? Convert.DBNull : configName);
                //        command.Parameters.AddWithValue("@PAGE_INDEX", pageIndex);
                //        command.Parameters.AddWithValue("@PAGE_SIZE", pageSize);
                //        command.Parameters.AddWithValue("@ORDERBY", orderBy);
                //        using (var adapter = new SqlDataAdapter(command))
                //        {
                //            conn.Open();

                //            DataSet dsResults = new DataSet();
                //            adapter.Fill(dsResults);
                //            DataTable dsTBL0 = dsResults.Tables[0];
                //            result = new SystemConfigList();
                //            for (int r = 0; r < dsTBL0.Rows.Count; r++)
                //            {
                //                SystemConfigEntity systemConfigEntity = new SystemConfigEntity();
                //                DataRow rowdt = dsTBL0.Rows[r];
                //                #region mapping: dataset to entity

                //                systemConfigEntity.moduleCode = SettingDataString(rowdt["MODULE_CODE"]);
                //                systemConfigEntity.configName = SettingDataString(rowdt["CONFIG_NAME"].ToString());
                //                systemConfigEntity.configValue = SettingDataString(rowdt["CONFIG_VALUE"]);
                //                systemConfigEntity.configDescription = SettingDataString(rowdt["CONFIG_DESC"]);
                //                systemConfigEntity.readOnly = SettingDataBool(rowdt["READ_ONLY"]);
                //                systemConfigEntity.regularExpression = SettingDataString(rowdt["REGULAR_EXP"]);
                //                systemConfigEntity.updatedBy = SettingDataString(rowdt["UPD_BY"]);
                //                systemConfigEntity._updatedBy = SettingDataString(rowdt["UPD_BY"]);
                //                systemConfigEntity.updatedDate = SettingDataDateTime(rowdt["UPD_DATE"]);
                //                systemConfigEntity._updatedDate = SettingDataDateTime(rowdt["UPD_DATE"]);

                //                #endregion
                //                result.configs.Add(systemConfigEntity);
                //            }
                //        }
                //    }
                //}
                //result._total = result.configs.Count();
                #endregion
                return result;
            }
            catch (Exception ex)
            {
                throw new DALException("ListSystemConfig occurs an error.[" + ex.Message + "]", true);
            }
        }

        public SystemConfigList ListSystemConfigByModuleCode(string moduleCode)
        {
            try
            {
                SystemConfigList result = new SystemConfigList();
                DataSet dsResults = this.Database.ExecuteDataSet(SP_LIST_SYSTEM_CONFIG_BY_MODULE_CODE,
                   (string.IsNullOrEmpty(moduleCode)) ? Convert.DBNull : moduleCode);

                if ((dsResults != null) && (dsResults.Tables[0].Rows.Count > 0))
                {
                    if (dsResults.Tables[0].Rows[0][0] != System.DBNull.Value)
                    {
                        result.configs.AddRange(CBO.FillCollection<SystemConfigEntity>(dsResults.Tables[0]));
                        //result._total = Convert.ToInt64(((dsResults.Tables[1].Rows[0][0] == Convert.DBNull) ? 1 : dsResults.Tables[1].Rows[0][0]));
                    }
                }
                #region Raw code for direct test
                //using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
                //{
                //    using (var command = new SqlCommand(SP_LIST_SYSTEM_CONFIG_BY_MODULE_CODE, conn))
                //    {
                //        command.CommandType = CommandType.StoredProcedure;
                //        command.Parameters.AddWithValue("@MODULE_CODE", moduleCode);
                //        using (var adapter = new SqlDataAdapter(command))
                //        {
                //            conn.Open();

                //            DataSet dsResults = new DataSet();
                //            adapter.Fill(dsResults);
                //            DataTable dsTBL0 = dsResults.Tables[0];
                //            result = new SystemConfigList();
                //            for (int r = 0; r < dsTBL0.Rows.Count; r++)
                //            {
                //                SystemConfigEntity systemConfigEntity = new SystemConfigEntity();
                //                DataRow rowdt = dsTBL0.Rows[r];
                //                #region mapping: dataset to entity

                //                systemConfigEntity.moduleCode = SettingDataString(rowdt["MODULE_CODE"]);
                //                systemConfigEntity.configName = SettingDataString(rowdt["CONFIG_NAME"].ToString());
                //                systemConfigEntity.configValue = SettingDataString(rowdt["CONFIG_VALUE"]);
                //                systemConfigEntity.configDescription = SettingDataString(rowdt["CONFIG_DESC"]);
                //                systemConfigEntity.readOnly = SettingDataBool(rowdt["READ_ONLY"]);
                //                systemConfigEntity.regularExpression = SettingDataString(rowdt["REGULAR_EXP"]);
                //                systemConfigEntity.updatedBy = SettingDataString(rowdt["UPD_BY"]);
                //                systemConfigEntity._updatedBy = SettingDataString(rowdt["UPD_BY"]);
                //                systemConfigEntity.updatedDate = SettingDataDateTimeNullable(rowdt["UPD_DATE"]);
                //                systemConfigEntity._updatedDate = SettingDataDateTimeNullable(rowdt["UPD_DATE"]);

                //                #endregion
                //                result.configs.Add(systemConfigEntity);
                //            }
                //        }
                //    }
                //}
                #endregion
                return result;
            }
            catch (Exception ex)
            {
                throw new DALException("ListSystemConfigByModuleCode occurs an error.[" + ex.Message + "]", true);
            }
        }

        public bool AddSystemConfig(SystemConfigEntity config)
        {
            try
            {
                CBO.FillObject<SystemConfigEntity>(this.Database.ExecuteReader(SP_ADD_SYSTEM_CONFIG,
                    (string.IsNullOrEmpty(config.moduleCode)) ? Convert.DBNull : config.moduleCode,
                    (string.IsNullOrEmpty(config.configName)) ? Convert.DBNull : config.configName,
                    (string.IsNullOrEmpty(config.configValue)) ? Convert.DBNull : config.configValue,
                    (string.IsNullOrEmpty(config.configDescription)) ? Convert.DBNull : config.configDescription,
                    config.readOnly,
                    (string.IsNullOrEmpty(config.regularExpression)) ? Convert.DBNull : config.regularExpression,
                    (string.IsNullOrEmpty(config.updatedBy)) ? Convert.DBNull : config.updatedBy));

                //this.Database.ExecuteScalar(SP_ADD_LOG, config.MenuID, ActionType.ADD.GetHashCode());
                return true;
                #region Raw code for direct test

                //using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
                //{
                //    using (var command = new SqlCommand(SP_ADD_SYSTEM_CONFIG, conn))
                //    {
                //        conn.Open();
                //        command.CommandType = CommandType.StoredProcedure;
                //        command.Parameters.AddWithValue("@MODULE_CODE", config.moduleCode);
                //        command.Parameters.AddWithValue("@CONFIG_NAME", config.configName);
                //        command.Parameters.AddWithValue("@CONFIG_VALUE", config.configValue);
                //        command.Parameters.AddWithValue("@CONFIG_DESC", config.configDescription);
                //        command.Parameters.AddWithValue("@READ_ONLY", config.readOnly);
                //        command.Parameters.AddWithValue("@REGULAR_EXP", config.regularExpression);
                //        command.Parameters.AddWithValue("@UPD_BY", config._updatedBy);
                //        command.ExecuteNonQuery();
                //        return true;
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                throw new DALException("AddSystemConfig occurs an error.[" + ex.Message + "]", true);
            }
        }

        public bool UpdateSystemConfig(SystemConfigEntity config)
        {
            try
            {
                CBO.FillObject<SystemConfigEntity>(this.Database.ExecuteReader(SP_UPDATE_SYSTEM_CONFIG,
                    (string.IsNullOrEmpty(config.moduleCode)) ? Convert.DBNull : config.moduleCode,
                    (string.IsNullOrEmpty(config.configName)) ? Convert.DBNull : config.configName,
                    (string.IsNullOrEmpty(config.configValue)) ? Convert.DBNull : config.configValue,
                    (string.IsNullOrEmpty(config.configDescription)) ? Convert.DBNull : config.configDescription,
                    config.readOnly,
                    (string.IsNullOrEmpty(config.regularExpression)) ? Convert.DBNull : config.regularExpression,
                    (string.IsNullOrEmpty(config.updatedBy)) ? Convert.DBNull : config.updatedBy));

                //this.Database.ExecuteScalar(SP_ADD_LOG, config.MenuID, ActionType.ADD.GetHashCode());
                return true;
                #region Raw code for direct test
                //using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
                //{
                //    using (var command = new SqlCommand(SP_UPDATE_SYSTEM_CONFIG, conn))
                //    {
                //        conn.Open();
                //        command.CommandType = CommandType.StoredProcedure;
                //        command.Parameters.AddWithValue("@MODULE_CODE", config.moduleCode);
                //        command.Parameters.AddWithValue("@CONFIG_NAME", config.configName);
                //        command.Parameters.AddWithValue("@CONFIG_VALUE", config.configValue);
                //        command.Parameters.AddWithValue("@CONFIG_DESC", config.configDescription);
                //        command.Parameters.AddWithValue("@READ_ONLY", config.readOnly);
                //        command.Parameters.AddWithValue("@REGULAR_EXP", config.regularExpression);
                //        command.Parameters.AddWithValue("@UPD_BY", config._updatedBy);
                //        command.ExecuteNonQuery();
                //        return true;
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                throw new DALException("UpdateSystemConfig occurs an error.[" + ex.Message + "]", true);
            }
        }

        public bool DeleteSystemConfig(string moduleCode, string configName)
        {
            try
            {
                //this.Database.ExecuteScalar(SP_ADD_LOG, moduleCode, configName, ActionType.DELETE.GetHashCode());
                this.Database.ExecuteScalar(SP_DELETE_SYSTEM_CONFIG,
                    moduleCode,
                    configName);

                //this.Database.ExecuteScalar(SP_ADD_LOG, config.MenuID, ActionType.ADD.GetHashCode());
                return true;

                #region Raw code for direct test
                //using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
                //{
                //    using (var command = new SqlCommand(SP_DELETE_SYSTEM_CONFIG, conn))
                //    {
                //        conn.Open();
                //        command.CommandType = CommandType.StoredProcedure;
                //        command.Parameters.AddWithValue("@MODULE_CODE", moduleCode);
                //        command.Parameters.AddWithValue("@CONFIG_NAME", configName);
                //        command.ExecuteNonQuery();
                //        return true;
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                throw new DALException("DeleteSystemConfig occurs an error.[" + ex.Message + "]", true);
            }
        }

    }
}