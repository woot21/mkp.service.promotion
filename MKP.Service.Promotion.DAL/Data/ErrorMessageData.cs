﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PSC.Frameworks.Utility;

using MKP.Service.Promotion.DTO;
using PSC.Frameworks.BSL.Core;

namespace MKP.Service.Promotion.DAL.Data
{
    public class ErrorMessageData : Base.BaseData, IData.IErrorMessageData
    {
        #region *** Members section ***

        #region [ Private members ]

        #region Constant package store procedure name

        private const string SP_LIST_ERROR_MESSAGE = "[TRV_MST_GLOBAL_ERROR_MESSAGE.SP_LIST_ERROR_MESSAGE]";

        private const string SP_GET_ERROR_MESSAGE_BY_GLOBAL_CODE = "[TRV_MST_GLOBAL_ERROR_MESSAGE.SP_GET_ERROR_MESSAGE_BY_GLOBAL_CODE]";
        private const string SP_GET_ERROR_MESSAGE_BY_ORIGINAL_CODE = "[TRV_MST_GLOBAL_ERROR_MESSAGE.SP_GET_ERROR_MESSAGE_BY_ORIGINAL_CODE]";

        #endregion

        #endregion

        #endregion

        #region *** Constructor section ***

        public ErrorMessageData()
            : base(typeof(ErrorMessageData))
        {
        }

        #endregion

        #region *** Methods section ***

        #region [ Public methods ]

        public ErrorMessageList ListErrorMessage()
        {
            try
            {
                ErrorMessageList list = new ErrorMessageList();

                DataSet dsResults = this.Database.ExecuteDataSet(SP_LIST_ERROR_MESSAGE);

                if ((dsResults != null) && (dsResults.Tables.Count > 0) && (dsResults.Tables[0].Rows.Count > 0))
                {
                    if (dsResults.Tables[0].Rows[0][0] != System.DBNull.Value)
                    {
                        list.Error.AddRange(CBO.FillCollection<ErrorMessageEntity>(dsResults.Tables[0]));
                        if (list.Error != null) list.TotalRecord = list.Error.Count();

                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw new DALException(this, "1102", "ListErrorMessage occurs an error.[" + ex.Message + "]", ex, true);
            }
        }

        public ErrorMessageEntity GetErrorMessageByGlobalCode(string globalCode)
        {
            try
            {
                return CBO.FillObject<ErrorMessageEntity>(this.Database.ExecuteReader(SP_GET_ERROR_MESSAGE_BY_GLOBAL_CODE,
                  globalCode));
            }
            catch (Exception ex)
            {
                throw new DALException(this, "1104", "GetErrorMessageByGlobalCode occurs an error.[" + ex.Message + "]", ex, true);
            }
        }

        public ErrorMessageEntity GetErrorMessageByOriginalCode(string originalCode)
        {
            try
            {
                return CBO.FillObject<ErrorMessageEntity>(this.Database.ExecuteReader(SP_GET_ERROR_MESSAGE_BY_ORIGINAL_CODE,
                  originalCode));
            }
            catch (Exception ex)
            {
                throw new DALException(this, "1104", "GetErrorMessageByOriginalCode occurs an error.[" + ex.Message + "]", ex, true);
            }
        }


        #endregion

        #endregion
    }
}

