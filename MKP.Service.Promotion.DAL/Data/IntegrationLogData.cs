﻿using MKP.Service.Promotion.DTO.Log;
using MKP.Service.Promotion.DTO.Promotion;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKP.Service.Promotion.DAL.Data
{
    public class IntegrationLogData : Base.BaseData, IData.IIntegrationLogData
    {
        private const string SP_ADD_LOG = "[MKP_LOG_INTEGRATION.SP_ADD_LOG]";

        public IntegrationLogData() : base(typeof(SystemConfigData))
        {

        }

        public ResultEntity AddIntegrationLog(IntegrationLogEntity request)
        {
            ResultEntity result = new ResultEntity();

            try
            {
                this.Database.ExecuteNonQuery(SP_ADD_LOG,
                ((request.InteDate.HasValue) ? request.InteDate : DateTime.Now),
                ((string.IsNullOrEmpty(request.InteType)) ? Convert.DBNull : request.InteType),
                ((string.IsNullOrEmpty(request.TranRefCode)) ? Convert.DBNull : request.TranRefCode),
                ((string.IsNullOrEmpty(request.SessionID)) ? Convert.DBNull : request.SessionID),
                (request.ActID),
                ((string.IsNullOrEmpty(request.RequestID)) ? Convert.DBNull : request.RequestID),
                ((string.IsNullOrEmpty(request.ProcessType)) ? Convert.DBNull : request.ProcessType),
                ((string.IsNullOrEmpty(request.ServiceName)) ? Convert.DBNull : request.ServiceName),
                ((string.IsNullOrEmpty(request.InteStatus)) ? Convert.DBNull : request.InteStatus),
                ((string.IsNullOrEmpty(request.InteDetail)) ? Convert.DBNull : request.InteDetail),
                ((string.IsNullOrEmpty(request.ErrCode)) ? Convert.DBNull : request.ErrCode),
                ((string.IsNullOrEmpty(request.ErrDesc)) ? Convert.DBNull : request.ErrDesc),
                ((string.IsNullOrEmpty(request.CreatedBy)) ? Convert.DBNull : request.CreatedBy),
                (request.Channel),
                ((string.IsNullOrEmpty(request.PartnerID)) ? Convert.DBNull : request.PartnerID));

                result.Result = true;
                result.ErrorCode = "0000";
                result.ErrorDesc = "Success";

            }
            catch (Exception ex)
            {
                result.Result = false;
                result.ErrorCode = "";
                result.ErrorDesc = "AddIntegrationLog :" + ex.Message;
                new DALException("AddIntegrationLog occurs an error.[" + ex.Message + "]", true);
            }

            return result;
        }
    }
}
