﻿using MKP.Service.Promotion.DTO.Promotion;
using PSC.Frameworks.BSL.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace MKP.Service.Promotion.DAL.Data
{
    public class BannerData : Base.BaseData, IData.IBannerData 
    {
        private const string SP_GET_LIST_BANNER = "[MKP_MST_CONTENT_PUBLISHED.SP_LIST_PUBLISHED_BANNER]";

        public BannerData() : base(typeof(SystemConfigData))
        {

        }

        public BannerList ListPublicBanner(BannerRequestEntity request)
        {
            BannerList result = new BannerList();
            try
            {
                DataSet ds = this.Database.ExecuteDataSet(SP_GET_LIST_BANNER,
                                (request.CONTENT_TYPE == null ? Convert.DBNull : request.CONTENT_TYPE),
                                (request.DISPLAY_TYPE == null ? Convert.DBNull : request.DISPLAY_TYPE),
                                //(request.MKP_DISPLAY_MAX == null ? Convert.DBNull : request.MKP_DISPLAY_MAX));
                                (request.MAX_LIMIT == null ? Convert.DBNull : request.MAX_LIMIT),
                                (request.PAGE_INDEX == null ? Convert.DBNull : request.PAGE_INDEX),
                                (request.PAGE_SIZE == null ? Convert.DBNull : request.PAGE_SIZE));
                if(ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    if(ds.Tables[0].Rows[0][0] != System.DBNull.Value)
                    {
                        result.Banner_List.AddRange(CBO.FillCollection<BannerEntity>(ds.Tables[0]));
                    }
                }
                result.Result = true;
                result.ErrorCode = "0000";
                result.ErrorDesc = "Success";

            }
            catch(Exception ex)
            {
                result.Result = false;
                result.ErrorCode = "9999";
                result.ErrorDesc = "ListPublicBanner : " + ex.Message; 
                new DALException("ListPublicBanner occurs an error.[" + ex.Message + "]", true);
            }
            return result;
        }
    }
}
