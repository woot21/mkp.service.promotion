﻿using PSC.Frameworks.BSL.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using MKP.Service.Promotion.DTO.Promotion;

namespace MKP.Service.Promotion.DAL.Data
{
    public class PromotionData : Base.BaseData, IData.IPromotionData
    {
        private const string SP_GET_LIST_PROMOTION = "[MKP_MST_PROMOTION.SP_LIST_PUBLISHED_PROMOTION]";

        public PromotionData() : base(typeof(SystemConfigData))
        {

        }

        public PromotionList ListPublishPromotion(PromotionRequestEntity request)
        {
            PromotionList result = new PromotionList();
            try
            {
                DataSet dsResults = this.Database.ExecuteDataSet(SP_GET_LIST_PROMOTION,
                              (request.PromotionCategory == null ? Convert.DBNull : request.PromotionCategory),
                              (request.PromotionType == null ? Convert.DBNull : request.PromotionType),
                              (request.MaxLimit == null ? Convert.DBNull : request.MaxLimit),
                              (request.DisplayType == null ? Convert.DBNull : request.DisplayType),
                              (request.PageIndex == null ? Convert.DBNull : request.PageIndex),
                              (request.PageSize == null ? Convert.DBNull : request.PageSize));
                if ((dsResults != null) && (dsResults.Tables[0].Rows.Count > 0))
                {
                    if (dsResults.Tables[0].Rows[0][0] != System.DBNull.Value)
                    {
                        result.Promotion_List.AddRange(CBO.FillCollection<PromotionEntity>(dsResults.Tables[0]));
                        //result.TotalRecord = Convert.ToInt64(((dsResults.Tables[1].Rows[0][0] == Convert.DBNull) ? 1 : dsResults.Tables[1].Rows[0][0]));
                        //result.Result = true;
                        //result.ErrorCode = "0000";
                        //result.ErrorDesc = "Success";
                    }
                }
                result.Result = true;
                result.ErrorCode = "0000";
                result.ErrorDesc = "Success";
               
            }
            catch (Exception ex)
            {
                result.Result = false;
                result.ErrorCode = "9999";
                result.ErrorDesc = "ListPublishPromotion : " + ex.Message;
                new DALException("ListPublishPromotion occurs an error.[" + ex.Message + "]", true);
            }
            return result;
        }

    }
}
