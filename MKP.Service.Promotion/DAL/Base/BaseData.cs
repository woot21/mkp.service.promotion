﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using Trove.Wallet.Data.DAL.IData;
using Trove.Service.Main.DTO.Enumeration;
using PSC.Frameworks.DAL.Core;
using PSC.Frameworks.Utility;
using System.Data.SqlClient;

namespace Trove.Wallet.Data.DAL.Base
{
    /// <summary>
    /// Created By:     Prakasit Kitrakham
    /// Created Date:   22 Apr 2010 11:10
    /// Description:    Base class for data access layer.
    /// History:
    ///     [Modified By Date/Time: Description]
    /// </summary>
    //public abstract class BaseData
    //{
    //    #region *** Members section ***

    //    #region [ Private members ]

    //    private bool _isWriteLog = Convert.ToBoolean(ConfigurationManager.AppSettings["KOL_IsWriteLog"] ?? "false");
    //    private bool _disposed = false;
    //    private Database _database = null;
    //    private bool _isSMEMode = (ConfigurationManager.AppSettings["WEB_MODE"] != null ? ConfigurationManager.AppSettings["WEB_MODE"].ToString().Trim().ToUpper().Equals("SME") : false);

    //    #endregion

    //    #endregion

    //    #region *** Properties section ***

    //    #region [ Protected properties ]

    //    private int? _requestChannel;

    //    protected Database Database
    //    {
    //        get { return _database; }
    //        set { _database = value; }
    //    }
    //    public bool IsWriteLog { get { return _isWriteLog; } }
    //    public bool IsSMEMode
    //    {
    //        get
    //        {
    //            return _isSMEMode;
    //        }
    //    }
    //    public Channel CurrentKOLChannel
    //    {
    //        get
    //        {
    //            if (IsSMEMode)
    //            {
    //                if (RequestChannel.HasValue)
    //                    return ((Channel)RequestChannel.Value);
    //                else
    //                    return Channel.KBOL;
    //            }
    //            else
    //                return Channel.KOL;
    //        }
    //    }
    //    public int? RequestChannel
    //    {
    //        get { return _requestChannel; }
    //        set { _requestChannel = value; }
    //    }

    //    private static string _currentServerNo = "";
    //    protected string ServerNo
    //    {
    //        get
    //        {
    //            string result = "";
    //            //try
    //            //{
    //            //    result = System.Net.Dns.GetHostName();
    //            //}
    //            //catch (Exception exs) { }

    //            //if (string.IsNullOrEmpty(_currentServerNo.GetValueOrDefault(serverNo.GetValueOrDefault().Trim()).Trim()))
    //            //{
    //            //    result = System.Net.Dns.GetHostName();
    //            //}
    //            //else
    //            //{
    //            //    result = _currentServerNo.GetValueOrDefault(serverNo.GetValueOrDefault().Trim()).Trim();
    //            //}

    //            if (string.IsNullOrEmpty(_currentServerNo.GetValueOrDefault().Trim()))
    //            {
    //                _currentServerNo = System.Net.Dns.GetHostName();
    //            }
    //            result = _currentServerNo;


    //            return result;

    //            //    ip = ip.GetValueOrDefault().Trim();

    //            //    if (string.IsNullOrEmpty(ip) == false)
    //            //    {
    //            //        // KMA 6 Server
    //            //        // KOL & KBOL 2 Server
    //            //        switch (ip)
    //            //        {
    //            //            case "192.168.22.1": // KMA
    //            //            case "192.168.22.39": // KOL
    //            //            case "192.168.45.4": // KOL & KMA (UAT)
    //            //            case "192.168.35.49": // KOL & KMA (DEV)
    //            //                result = "1";
    //            //                break;
    //            //            case "192.168.22.2": // KMA
    //            //            case "192.168.22.40": // KOL
    //            //                result = "2";
    //            //                break;
    //            //            case "192.168.22.5":
    //            //                result = "3";
    //            //                break;
    //            //            case "192.168.22.6":
    //            //                result = "4";
    //            //                break;
    //            //            case "192.168.22.37":
    //            //                result = "5";
    //            //                break;
    //            //            case "192.168.22.38":
    //            //                result = "6";
    //            //                break;
    //            //        }
    //            //    }
    //        }
    //    }

    //    #endregion

    //    #endregion

    //    #region *** Constructor section ***

    //    public BaseData(Type objType)
    //    {
    //        this._database = DatabaseFactory.CreateDatabase(objType);

    //        //addIntegrationLogDelegate = new AddIntegrationLogDelegate(AddIntegrationLogOrg);
    //    }

    //    #endregion

    //    #region *** Destructor section ***

    //    ~BaseData()
    //    {
    //        Dispose(false);
    //    }

    //    #endregion

    //    #region *** Methods section ***

    //    #region [ Private methods ]

    //    private void Dispose(bool disposing)
    //    {
    //        if (!this._disposed)
    //        {
    //            if (disposing)
    //            {
    //                Database.Dispose();
    //            }
    //        }
    //        this._disposed = true;
    //    }

    //    #endregion

    //    #region [ Public methods ]

    //    public void Dispose()
    //    {
    //        Dispose(true);
    //        GC.SuppressFinalize(this);
    //    }

    //    #endregion

    //    #region Integration Log
    //    // Delegate Function // 
    //    public delegate void AddIntegrationLogDelegate(string requestID, string integrationType, string processType, string methodName, string integrationDetail, string status, string errorCode, string errorMessage, string createBy);
    //    //public AddIntegrationLogDelegate addIntegrationLogDelegate;
    //    //public void AddIntegrationLog(string requestID, string integrationType, string processType, string methodName, string integrationDetail, string status, string errorCode, string errorMessage, string createBy)
    //    //{
    //    //    try
    //    //    {
    //    //        List<IAsyncResult> inteLog = new List<IAsyncResult>();
    //    //        inteLog.Add(this.addIntegrationLogDelegate.BeginInvoke(requestID, integrationType, processType, methodName, integrationDetail, status, errorCode, errorMessage, createBy, null, null));
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        new DALException(this, "1101", "BaseData AddIntegrationLog occurs an error.[" + ex.Message + "]", ex, true);
    //    //    }
    //    //}
    //    // WriteLog Function // 
    //    //public void AddIntegrationLogOrg(string requestID, string integrationType, string processType, string methodName, string integrationDetail, string status, string errorCode, string errorMessage, string createBy)
    //    //{
    //    //    try
    //    //    {
    //    //        using (IIntegrationLogData eaiData = DataFactory.GetIntegrationLogData())
    //    //            eaiData.AddIntegrationLog(DateTime.Now, integrationType, null, null, null,
    //    //                                 requestID, processType, methodName, status, integrationDetail, errorCode, errorMessage, createBy);
    //    //    }
    //    //    catch (Exception ex)
    //    //    {
    //    //        new DALException(this, "1101", "BaseData AddIntegrationLog occurs an error.[" + ex.Message + "]", ex, true);
    //    //    }
    //    //}

    //    #endregion
    //    public static string Serialize<T>(T obj)
    //    {
    //        try
    //        {
    //            String XmlizedString = null;
    //            MemoryStream memoryStream = new MemoryStream();
    //            XmlSerializer xs = new XmlSerializer(obj.GetType());
    //            XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
    //            xs.Serialize(xmlTextWriter, obj);
    //            memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
    //            XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
    //            return XmlizedString.Trim();
    //        }
    //        catch (Exception e) { Console.WriteLine(e); return null; }
    //    }

    //    public static string Serialize<T>(T obj, Type[] types)
    //    {
    //        try
    //        {
    //            XmlSerializer xs = new XmlSerializer(obj.GetType(), types);
    //            StringWriter stringWriter = new StringWriter();
    //            xs.Serialize(stringWriter, obj);
    //            stringWriter.Flush();
    //            return stringWriter.ToString().Trim();
    //        }
    //        catch (Exception e) { Console.WriteLine(e); return null; }
    //    }
    //    private static String UTF8ByteArrayToString(Byte[] characters)
    //    {

    //        UTF8Encoding encoding = new UTF8Encoding();
    //        String constructedString = encoding.GetString(characters);
    //        return (constructedString);
    //    }

    //    private static Byte[] StringToUTF8ByteArray(String pXmlString)
    //    {
    //        UTF8Encoding encoding = new UTF8Encoding();
    //        Byte[] byteArray = encoding.GetBytes(pXmlString);
    //        return byteArray;
    //    }
    //    public string GetConnectionString(int mode)
    //    {
    //        string connectionStrings = "";
    //        try
    //        {
    //            if (mode == 1)
    //                connectionStrings = ConfigurationManager.ConnectionStrings["KMADataProvider"].ConnectionString;
    //            else if (mode == 2)
    //                connectionStrings = ConfigurationManager.ConnectionStrings["KOLSMEDataProvider"].ConnectionString;
    //            else if (mode == 3)
    //                connectionStrings = ConfigurationManager.ConnectionStrings["KOLOTPDataProvider"].ConnectionString;
    //            else
    //                connectionStrings = ConfigurationManager.ConnectionStrings["KOLCMSDataProvider"].ConnectionString;
    //            //if (mode == 1)
    //            //    connectionStrings = ConfigurationManager.ConnectionStrings["NEWKOLDataProvider"].ConnectionString;
    //            //else if (mode == 2)
    //            //    connectionStrings = ConfigurationManager.ConnectionStrings["NEWKOLCORPDataProvider"].ConnectionString;
    //            //else
    //            //    connectionStrings = ConfigurationManager.ConnectionStrings["NEWKOLDataProvider"].ConnectionString;
    //        }
    //        catch (Exception ex)
    //        {
    //            new DALException(this, "1101", "BaseData GetConnectionString occurs an error.[" + ex.Message + "]", ex, true);
    //        }

    //        return connectionStrings;
    //    }

    //    public byte[] SettingDataByteArray(object col)
    //    {
    //        //return (string)(Null.SetNull(col, typeof(string)).ToString());
    //        //Fix string Nullable Type
    //        return (byte[])Null.SetNull(col, typeof(byte[]));
    //    }

    //    public string SettingDataString(object col)
    //    {
    //        //return (string)(Null.SetNull(col, typeof(string)).ToString());
    //        //Fix string Nullable Type
    //        return (string)Null.SetNull(col, typeof(string));
    //    }

    //    public int SettingDataInt(object col)
    //    {
    //        return int.Parse(Null.SetNull(col, typeof(int)).ToString());
    //    }

    //    public decimal SettingDataDecimal(object col)
    //    {
    //        return decimal.Parse(Null.SetNull(col, typeof(decimal)).ToString());
    //    }

    //    public DateTime SettingDataDateTime(object col)
    //    {
    //        return DateTime.Parse(Null.SetNull(col, typeof(DateTime)).ToString());
    //    }

    //    public bool SettingDataBool(object col)
    //    {
    //        return bool.Parse(Null.SetNull(col, typeof(bool)).ToString());
    //    }

    //    //Fix Int Nullable Type
    //    public int? SettingDataIntNullable(object col)
    //    {
    //        //return int.Parse(Null.SetNull(col, typeof(int)).ToString());
    //        //return (int?)Null.SetNull(col, typeof(int?));
    //        if (col != DBNull.Value)
    //            return int.Parse(col.ToString());
    //        else
    //            return (new System.Nullable<int>());
    //    }
    //    //Fix DateTime Nullable Type
    //    public DateTime? SettingDataDateTimeNullable(object col)
    //    {
    //        //return DateTime.Parse(Null.SetNull(col, typeof(DateTime)).ToString());
    //        //return (DateTime?)Null.SetNull(col, typeof(DateTime?));
    //        if (col != DBNull.Value)
    //            return DateTime.Parse(col.ToString());
    //        else
    //            return (new System.Nullable<DateTime>());
    //    }

    //    //Fix decimal Nullable Type
    //    public decimal? SettingDataDecimalNullable(object col)
    //    {
    //        //return decimal.Parse(Null.SetNull(col, typeof(decimal)).ToString());
    //        //return (decimal?)Null.SetNull(col, typeof(decimal?));
    //        if (col != DBNull.Value)
    //            return decimal.Parse(col.ToString());
    //        else
    //            return (new System.Nullable<decimal>());
    //    }

    //    public long? SettingDataLongNullable(object col)
    //    {
    //        //return long.Parse(Null.SetNull(col, typeof(long)).ToString());
    //        //return (long?)Null.SetNull(col, typeof(long?));
    //        if (col != DBNull.Value)
    //            return long.Parse(col.ToString());
    //        else
    //            return (new System.Nullable<long>());
    //    }

    //    public bool? SettingDataBoolNullable(object col)
    //    {
    //        //return bool.Parse(Null.SetNull(col, typeof(bool)).ToString());
    //        //return (bool?)Null.SetNull(col, typeof(bool?));
    //        if (col != DBNull.Value)
    //            return bool.Parse(col.ToString());
    //        else
    //            return (new System.Nullable<bool>());
    //    }

    //    #region [Protected]
    //    protected object SetDBValue(int? value)
    //    {
    //        if (value.HasValue)
    //            return value.Value;
    //        return Convert.DBNull;
    //    }
    //    protected object SetDBValue(decimal? value)
    //    {
    //        if (value.HasValue)
    //            return value.Value;
    //        return Convert.DBNull;
    //    }
    //    protected object SetDBValue(DateTime? value)
    //    {
    //        if (value.HasValue)
    //            return value.Value;
    //        return Convert.DBNull;
    //    }
    //    protected object SetDBValue(string value)
    //    {
    //        if (!String.IsNullOrEmpty(value))
    //            return value;
    //        return Convert.DBNull;
    //    }
    //    #endregion
    //    #endregion
    //}
    public abstract class BaseData
    {
        protected bool isWriteLog = (ConfigurationManager.AppSettings["SQS_IsWriteLog"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["SQS_IsWriteLog"].ToString()) : true);
        private bool isEncrypt = (ConfigurationManager.AppSettings["SQS_IsEncrypt"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["SQS_IsEncrypt"].ToString()) : true);
        #region *** Members section ***

        #region [ Private members ]

        private bool _disposed = false;
        private Database _database = null;

        #endregion

        #endregion

        #region *** Properties section ***

        #region [ Protected properties ]

        protected Database Database
        {
            get { return _database; }
            set { _database = value; }
        }

        #endregion

        #endregion

        #region *** Constructor section ***

        public BaseData(Type objType)
        {
            this._database = DatabaseFactory.CreateDatabase(objType);
            if (isEncrypt)
            {
                string connString = this._database.ConnectionString;
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connString);
                string password = builder.Password;
                #region Encodeเพื่อเอาไปใส่Config
                //string[] passTemp = { password };
                //string temp = PSCToken.Encode(passTemp);
                #endregion
                //string[] resultString = (string[])PSCToken.Decode<object[]>(password);
                //builder.Password = resultString[0];
                this._database.ConnectionString = builder.ConnectionString;
            }
        }

        #endregion

        #region *** Destructor section ***

        ~BaseData()
        {
            Dispose(false);
        }

        #endregion

        #region *** Methods section ***

        #region [ Private methods ]

        private void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    Database.Dispose();
                }
            }
            this._disposed = true;
        }

        #endregion

        #region [ Public methods ]

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        #region *** Setting Data to Entity ***

        public string SettingDataString(object col)
        {
            if (col != null)
            {
                return col.ToString();
            }
            else return null;
        }

        public int SettingDataInt32(object col)
        {
            return int.Parse(col.ToString());
        }


        public decimal SettingDataDecimal(object col)
        {
            return decimal.Parse(col.ToString());
        }

        public DateTime SettingDataDateTime(object col)
        {
            return DateTime.Parse(col.ToString());
            //return DateTime.Parse(Null.SetNull(col, typeof(DateTime)).ToString());
        }

        public bool SettingDataBool(object col)
        {
            return bool.Parse(col.ToString());
            //return bool.Parse(Null.SetNull(col, typeof(bool)).ToString());
        }

        public float SettingDataFloat(object col)
        {
            return float.Parse(col.ToString());
            //return float.Parse(Null.SetNull(col, typeof(float)).ToString());
        }

        //Fix Int Nullable Type
        public int? SettingDataIntNullable(object col)
        {
            if (col != null)
            {
                return int.Parse(col.ToString());
            }
            else return null;
            //return int.Parse(Null.SetNull(col, typeof(int)).ToString());
            //return (int?)Null.SetNull(col, typeof(int?));
        }

        //Fix DateTime Nullable Type
        public DateTime? SettingDataDateTimeNullable(object col)
        {
            if (col != null)
            {
                return DateTime.Parse(col.ToString());
            }
            else return null;
            //return DateTime.Parse(Null.SetNull(col, typeof(DateTime)).ToString());
            //return (DateTime?)Null.SetNull(col, typeof(DateTime?));
        }

        //Fix decimal Nullable Type
        public decimal? SettingDataDecimalNullable(object col)
        {
            if (col != null)
            {
                return decimal.Parse(col.ToString());
            }
            else return null;
            //return decimal.Parse(Null.SetNull(col, typeof(decimal)).ToString());
            //return (decimal?)Null.SetNull(col, typeof(decimal?));
        }

        public bool? SettingDataBoolNullable(object col)
        {
            if (col != null)
            {
                return bool.Parse(col.ToString());
            }
            else return null;
            //return bool.Parse(Null.SetNull(col, typeof(bool)).ToString());
            //return (bool?)Null.SetNull(col, typeof(bool?));
        }

        public float? SettingDataFloatNullable(object col)
        {
            if (col != null)
            {
                return float.Parse(col.ToString());
            }
            else return null;
            //return (float?)Null.SetNull(col, typeof(float?));
        }
        #endregion


        #endregion

        #endregion

    }

    public static class BaseClassStatic
    {
        public static string GetValueOrDefault(this string value)
        {
            return value.GetValueOrDefault("");
        }

        public static string GetValueOrDefault(this string value, string defaultValue)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                return defaultValue;
            else
                return value;
        }
    }
}
