﻿using BAU.SMA.DTO;
using Trove.Wallet.Data.DAL.Base;
using Trove.Service.Main.DTO;
//using Trove.DTO;
using Newtonsoft.Json;
using PSC.Frameworks.BSL.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Trove.Wallet.Data.DAL.Data
{
    public class SystemConfigData : Base.BaseData, IData.ISystemConfigData
    {
        #region *** Members section ***

        #region [ Private members ]

        #region Constant package store procedure name
        private const string SP_GET_SYSTEM_CONFIG_BY_CONFIG_NAME = "[TRV_MST_SYSTEM_CONFIG.SP_GET_SYSTEM_CONFIG_BY_CONFIG_NAME]";
        private const string SP_LIST_MODULE_CODE = "[TRV_MST_SYSTEM_CONFIG.SP_LIST_MODULE_CODE]";
        private const string SP_LIST_SYSTEM_CONFIG = "[TRV_MST_SYSTEM_CONFIG.SP_LIST_SYSTEM_CONFIG]";
        private const string SP_LIST_SYSTEM_CONFIG_BY_MODULE_CODE = "[TRV_MST_SYSTEM_CONFIG.SP_LIST_SYSTEM_CONFIG_BY_MODULE_CODE]";
        private const string SP_ADD_SYSTEM_CONFIG = "[TRV_MST_SYSTEM_CONFIG.SP_ADD_SYSTEM_CONFIG]";
        private const string SP_UPDATE_SYSTEM_CONFIG = "[TRV_MST_SYSTEM_CONFIG.SP_UPDATE_SYSTEM_CONFIG]";
        private const string SP_DELETE_SYSTEM_CONFIG = "[TRV_MST_SYSTEM_CONFIG.SP_DELETE_SYSTEM_CONFIG]";

        #endregion

        #endregion

        #endregion

        SqlConnection connection;
        Dictionary<string, string> _config;
        public Dictionary<string, string> Config { get => _config; set => _config = value; }
        public SystemConfigData()
        : base(typeof(SystemConfigData))
        {
        }

        //public SystemConfigData(Dictionary<string,string> iConfig)
        //{
        //    try
        //    {
        //        Config = iConfig;
        //        //if (!string.IsNullOrEmpty(Config["ConnectionString"]))
        //        //{
        //        //    connection = new SqlConnection(Config["ConnectionString"]);
        //        //}
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}

        public SystemConfigEntity GetSystemConfig(string moduleCode, string configName)
        {
            try
            {
                return CBO.FillObject<SystemConfigEntity>(this.Database.ExecuteReader(SP_GET_SYSTEM_CONFIG_BY_CONFIG_NAME,
                    (moduleCode == null ? Convert.DBNull : moduleCode),
                    (configName == null ? Convert.DBNull : configName)));

                #region Raw code for direct test
                //using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
                //{
                //    using (var command = new SqlCommand(SP_GET_SYSTEM_CONFIG_BY_CONFIG_NAME, conn))
                //    {
                //        command.CommandType = CommandType.StoredProcedure;
                //        command.Parameters.AddWithValue("@MODULE_CODE", moduleCode);
                //        command.Parameters.AddWithValue("@CONFIG_NAME", configName);
                //        using (var adapter = new SqlDataAdapter(command))
                //        {
                //            conn.Open();

                //            DataSet dsResults = new DataSet();
                //            adapter.Fill(dsResults);
                //            DataTable dsTBL0 = dsResults.Tables[0];
                //            if(dsTBL0.Rows.Count > 0)
                //            {
                //                result = new SystemConfigEntity();
                //                DataRow rowdt = dsTBL0.Rows[0];
                //                #region mapping: dataset to entity

                //                result.moduleCode = SettingDataString(rowdt["MODULE_CODE"]);
                //                result.configName = SettingDataString(rowdt["CONFIG_NAME"].ToString());
                //                result.configValue = SettingDataString(rowdt["CONFIG_VALUE"]);
                //                result.configDescription = SettingDataString(rowdt["CONFIG_DESC"]);
                //                result.readOnly = SettingDataBool(rowdt["READ_ONLY"]);
                //                result.regularExpression = SettingDataString(rowdt["REGULAR_EXP"]);
                //                #endregion
                //            }
                //            else
                //            {
                //                result = null;
                //            }
                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public List<string> ListModuleCode()
        //{
        //    try
        //    {
        //        List<string> result = new List<string>();
        //        using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
        //        {
        //            using (var command = new SqlCommand(SP_LIST_MODULE_CODE, conn))
        //            {
        //                command.CommandType = CommandType.StoredProcedure;
        //                using (var adapter = new SqlDataAdapter(command))
        //                {
        //                    conn.Open();

        //                    DataSet dsResults = new DataSet();
        //                    adapter.Fill(dsResults);
        //                    DataTable dsTBL0 = dsResults.Tables[0];
        //                    for (int r = 0; r < dsTBL0.Rows.Count; r++)
        //                    {
        //                        SystemConfigEntity systemConfigEntity = new SystemConfigEntity();
        //                        DataRow rowdt = dsTBL0.Rows[r];
        //                        #region mapping: dataset to entity

        //                        result.Add(SettingDataString(rowdt["MODULE_CODE"]));
        //                        #endregion
        //                    }
        //                }
        //            }
        //        }
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public SystemConfigList ListSystemConfig(string moduleCode, string configName, int pageIndex, int pageSize, string orderBy)
        //{
        //    try
        //    {
        //        SystemConfigList result = new SystemConfigList();
        //        using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
        //        {
        //            using (var command = new SqlCommand(SP_LIST_SYSTEM_CONFIG, conn))
        //            {
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.AddWithValue("@MODULE_CODE", string.IsNullOrEmpty(moduleCode) ? Convert.DBNull : moduleCode);
        //                command.Parameters.AddWithValue("@CONFIG_NAME", string.IsNullOrEmpty(configName) ? Convert.DBNull : configName);
        //                command.Parameters.AddWithValue("@PAGE_INDEX", pageIndex);
        //                command.Parameters.AddWithValue("@PAGE_SIZE", pageSize);
        //                command.Parameters.AddWithValue("@ORDERBY", orderBy);
        //                using (var adapter = new SqlDataAdapter(command))
        //                {
        //                    conn.Open();

        //                    DataSet dsResults = new DataSet();
        //                    adapter.Fill(dsResults);
        //                    DataTable dsTBL0 = dsResults.Tables[0];
        //                    result = new SystemConfigList();
        //                    for (int r = 0; r < dsTBL0.Rows.Count; r++)
        //                    {
        //                        SystemConfigEntity systemConfigEntity = new SystemConfigEntity();
        //                        DataRow rowdt = dsTBL0.Rows[r];
        //                        #region mapping: dataset to entity

        //                        systemConfigEntity.moduleCode = SettingDataString(rowdt["MODULE_CODE"]);
        //                        systemConfigEntity.configName = SettingDataString(rowdt["CONFIG_NAME"].ToString());
        //                        systemConfigEntity.configValue = SettingDataString(rowdt["CONFIG_VALUE"]);
        //                        systemConfigEntity.configDescription = SettingDataString(rowdt["CONFIG_DESC"]);
        //                        systemConfigEntity.readOnly = SettingDataBool(rowdt["READ_ONLY"]);
        //                        systemConfigEntity.regularExpression = SettingDataString(rowdt["REGULAR_EXP"]);
        //                        systemConfigEntity.updatedBy = SettingDataString(rowdt["UPD_BY"]);
        //                        systemConfigEntity._updatedBy = SettingDataString(rowdt["UPD_BY"]);
        //                        systemConfigEntity.updatedDate = SettingDataDateTime(rowdt["UPD_DATE"]);
        //                        systemConfigEntity._updatedDate = SettingDataDateTime(rowdt["UPD_DATE"]);
        //                        walletEntity.CreatedBy = rowdt["CREATED_BY"].ToString();
        //                        walletEntity.CreatedDate = DateTime.Parse(rowdt["CREATED_DATE"].ToString());
        //                        kioskMenu.ErrorCode = SettingDataBool(rowdt["KIOSK_MENU_SHOW_SUGGESTION"].ToString());
        //                        kioskMenu.ErrorMessage = SettingDataBool(rowdt["ENABLED"].ToString());
        //                        kioskMenu.ErrorMessageEN = SettingDataBool(rowdt["ENABLED"].ToString());
        //                        kioskMenu.EffectiveDate = SettingDataDateTime(rowdt["EFFECTIVE_DATE"].ToString());
        //                        kioskMenu.ExpireDate = SettingDataDateTime(rowdt["EXPIRE_DATE"].ToString());
        //                        kioskMenu.CreatedDate = SettingDataDateTime(rowdt["CRE_DATE"].ToString());
        //                        kioskMenu.CreatedBy = SettingDataString(rowdt["CRE_DATE"].ToString());
        //                        kioskMenu.UpdatedDate = SettingDataDateTime(rowdt["UPD_DATE"].ToString());
        //                        kioskMenu.UpdatedBy = SettingDataString(rowdt["UPD_BY"].ToString());

        //                        #endregion
        //                        result.configs.Add(systemConfigEntity);
        //                    }
        //                }
        //            }
        //        }
        //        result._total = result.configs.Count();
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public SystemConfigList ListSystemConfigByModuleCode(string moduleCode)
        //{
        //    try
        //    {
        //        SystemConfigList result = new SystemConfigList();
        //        using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
        //        {
        //            using (var command = new SqlCommand(SP_LIST_SYSTEM_CONFIG_BY_MODULE_CODE, conn))
        //            {
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.AddWithValue("@MODULE_CODE", moduleCode);
        //                using (var adapter = new SqlDataAdapter(command))
        //                {
        //                    conn.Open();

        //                    DataSet dsResults = new DataSet();
        //                    adapter.Fill(dsResults);
        //                    DataTable dsTBL0 = dsResults.Tables[0];
        //                    result = new SystemConfigList();
        //                    for (int r = 0; r < dsTBL0.Rows.Count; r++)
        //                    {
        //                        SystemConfigEntity systemConfigEntity = new SystemConfigEntity();
        //                        DataRow rowdt = dsTBL0.Rows[r];
        //                        #region mapping: dataset to entity

        //                        systemConfigEntity.moduleCode = SettingDataString(rowdt["MODULE_CODE"]);
        //                        systemConfigEntity.configName = SettingDataString(rowdt["CONFIG_NAME"].ToString());
        //                        systemConfigEntity.configValue = SettingDataString(rowdt["CONFIG_VALUE"]);
        //                        systemConfigEntity.configDescription = SettingDataString(rowdt["CONFIG_DESC"]);
        //                        systemConfigEntity.readOnly = SettingDataBool(rowdt["READ_ONLY"]);
        //                        systemConfigEntity.regularExpression = SettingDataString(rowdt["REGULAR_EXP"]);
        //                        systemConfigEntity.updatedBy = SettingDataString(rowdt["UPD_BY"]);
        //                        systemConfigEntity._updatedBy = SettingDataString(rowdt["UPD_BY"]);
        //                        systemConfigEntity.updatedDate = SettingDataDateTimeNullable(rowdt["UPD_DATE"]);
        //                        systemConfigEntity._updatedDate = SettingDataDateTimeNullable(rowdt["UPD_DATE"]);

        //                        walletEntity.CreatedBy = rowdt["CREATED_BY"].ToString();
        //                        walletEntity.CreatedDate = DateTime.Parse(rowdt["CREATED_DATE"].ToString());
        //                        kioskMenu.ErrorCode = SettingDataBool(rowdt["KIOSK_MENU_SHOW_SUGGESTION"].ToString());
        //                        kioskMenu.ErrorMessage = SettingDataBool(rowdt["ENABLED"].ToString());
        //                        kioskMenu.ErrorMessageEN = SettingDataBool(rowdt["ENABLED"].ToString());
        //                        kioskMenu.EffectiveDate = SettingDataDateTime(rowdt["EFFECTIVE_DATE"].ToString());
        //                        kioskMenu.ExpireDate = SettingDataDateTime(rowdt["EXPIRE_DATE"].ToString());
        //                        kioskMenu.CreatedDate = SettingDataDateTime(rowdt["CRE_DATE"].ToString());
        //                        kioskMenu.CreatedBy = SettingDataString(rowdt["CRE_DATE"].ToString());
        //                        kioskMenu.UpdatedDate = SettingDataDateTime(rowdt["UPD_DATE"].ToString());
        //                        kioskMenu.UpdatedBy = SettingDataString(rowdt["UPD_BY"].ToString());

        //                        #endregion
        //                        result.configs.Add(systemConfigEntity);
        //                    }
        //                }
        //            }
        //        }
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public bool AddSystemConfig(SystemConfigEntity config)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
        //        {
        //            using (var command = new SqlCommand(SP_ADD_SYSTEM_CONFIG, conn))
        //            {
        //                conn.Open();
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.AddWithValue("@MODULE_CODE", config.moduleCode);
        //                command.Parameters.AddWithValue("@CONFIG_NAME", config.configName);
        //                command.Parameters.AddWithValue("@CONFIG_VALUE", config.configValue);
        //                command.Parameters.AddWithValue("@CONFIG_DESC", config.configDescription);
        //                command.Parameters.AddWithValue("@READ_ONLY", config.readOnly);
        //                command.Parameters.AddWithValue("@REGULAR_EXP", config.regularExpression);
        //                command.Parameters.AddWithValue("@UPD_BY", config._updatedBy);
        //                command.ExecuteNonQuery();
        //                return true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public bool UpdateSystemConfig(SystemConfigEntity config)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
        //        {
        //            using (var command = new SqlCommand(SP_UPDATE_SYSTEM_CONFIG, conn))
        //            {
        //                conn.Open();
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.AddWithValue("@MODULE_CODE", config.moduleCode);
        //                command.Parameters.AddWithValue("@CONFIG_NAME", config.configName);
        //                command.Parameters.AddWithValue("@CONFIG_VALUE", config.configValue);
        //                command.Parameters.AddWithValue("@CONFIG_DESC", config.configDescription);
        //                command.Parameters.AddWithValue("@READ_ONLY", config.readOnly);
        //                command.Parameters.AddWithValue("@REGULAR_EXP", config.regularExpression);
        //                command.Parameters.AddWithValue("@UPD_BY", config._updatedBy);
        //                command.ExecuteNonQuery();
        //                return true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public bool DeleteSystemConfig(string moduleCode, string configName)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(Config["ConnectionStrings:DefaultConnection:ConnectionString"]))
        //        {
        //            using (var command = new SqlCommand(SP_DELETE_SYSTEM_CONFIG, conn))
        //            {
        //                conn.Open();
        //                command.CommandType = CommandType.StoredProcedure;
        //                command.Parameters.AddWithValue("@MODULE_CODE", moduleCode);
        //                command.Parameters.AddWithValue("@CONFIG_NAME", configName);
        //                command.ExecuteNonQuery();
        //                return true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

    }
}