﻿using Microsoft.Extensions.Configuration;
using PSC.Frameworks.Exception.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MKP.Service.Promotion
{
    [Serializable]
    public class UILException : CustomException
    {
        public UILException(object oSource, string sCode, string sMessage, System.Exception oInnerException, bool bLog, bool bThrowSoap)
            : base(oSource, sCode, sMessage, oInnerException, bLog, bThrowSoap)
        {
        }

        public UILException(object oSource, string sCode, string sMessage, System.Exception oInnerException, bool bLog)
            : base(oSource, sCode, sMessage, oInnerException, bLog)
        {
        }

        public UILException(object oSource, string sCode, string sMessage, bool bLog)
            : base(oSource, sCode, sMessage, bLog)
        {
        }

        public UILException(string sMessage, System.Exception oInnerException, bool bLog)
            : base(sMessage, oInnerException, bLog)
        {
        }

        public UILException(string sMessage, bool bLog)
            : base(sMessage, bLog)
        {
        }

        public UILException()
        {
        }

        public override string Format(object oSource, string sCode, string sMessage, System.Exception oInnerException)
        {
            StringBuilder sNewMessage = new StringBuilder();
            string sErrorStack = null;

            // if sPNR is not null or not empty, insert PNR into sMessage
            if (MessageExt != null && this.MessageExt != string.Empty)
                sMessage = MessageExt.ToUpper() + " - " + sMessage;

            // get the error stack, if InnerException is null, sErrorStack will be "exception was not chained" and should never be null
            sErrorStack = BuildErrorStack(oInnerException);

            // we want immediate gradification 
            Trace.AutoFlush = true;

            sNewMessage.Append("Exception Summary \r\n")
                .Append("---------------------------------------------------------------------------------------\r\n")
                .Append(DateTime.Now.ToShortDateString())
                .Append(":")
                .Append(DateTime.Now.ToShortTimeString())
                .Append(" - ")
                .Append(sMessage)
                .Append("\r\n\r\n")
                .Append(sErrorStack);

            return sNewMessage.ToString();
        }

        public override void SetLogFileName()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            string webPath = configuration.GetSection("AppSettings:SERVER_PATH").Value ?? @"D:\Data\Trove\";

            try
            {
                fileName = configuration.GetSection("AppSettings:UIL_LOG_FILE").Value ?? @"\Logs\UILErrorLog.txt";
            }
            catch
            {
                fileName = @"\Logs\UILErrorLog.txt";

            }

            try
            {
                fileName = webPath + fileName;
            }
            catch
            {
                //fileName = (string)ConfigurationManager.AppSettings["BSL_ASYN_LOG_FILE"] ?? @" D:\Data\BAY.KMA.MobileGateway.Service\AsyLogs\BSLErrorLog.txt";
                fileName = @"D:\Data\Trove\" + @"\Logs\UILErrorLog.txt";
            }

        }
    }
}
