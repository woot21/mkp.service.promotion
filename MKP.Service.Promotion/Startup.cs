﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using MKP.Service.Promotion.Helper;
using Newtonsoft.Json.Linq;

namespace MKP.Service.Promotion
{
    public class Startup
    {

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            //Configuration = configuration;
            var builder = new ConfigurationBuilder()
                        .AddJsonFile($"appsettings.json");
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins("http://localhost")
                                         .AllowAnyHeader()
                                         .AllowAnyMethod()
                                         .AllowAnyOrigin()
                                         .AllowCredentials(); ;
                });
            });


            //// configure strongly typed settings objects
            //var appSettingsSection = Configuration.GetSection("AppSettings");
            //services.Configure<AppSettings>(appSettingsSection);

            //// configure jwt authentication
            //var appSettings = appSettingsSection.Get<AppSettings>();


            IConfigurationBuilder builder2 = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = builder2.Build();
            var JWTSecretKey = configuration.GetValue<string>("JWTSettings:Secret");

            var key = Encoding.ASCII.GetBytes(JWTSecretKey);

            services.AddAuthentication((AuthenticationOptions x) =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(async (JwtBearerOptions options) =>
           {

               options.Events = new JwtBearerEvents()
               {

                   OnMessageReceived = (MessageReceivedContext context) =>
                   {
                       return Task.CompletedTask;
                   },
                   OnChallenge = (JwtBearerChallengeContext context) =>
                   {

                       return Task.CompletedTask;
                   },
                   OnTokenValidated = (TokenValidatedContext context) =>
                   {
                       return Task.CompletedTask;
                   },
                   OnAuthenticationFailed = (AuthenticationFailedContext context) =>
                   {
                       if (context.Exception is SecurityTokenExpiredException expiredException)
                       {
                           context.Response.Headers.Add("Token-Expired", "true");
                       }
                       else if (context.Exception is SecurityTokenInvalidSignatureException tokenException)
                       {
                           context.Response.Headers.Add("Token-Authentication-Failed", "Invalid Signature");
                       }
                       else
                       {
                           context.Response.Headers.Add("Token-Authentication-Failed", "Unknow Error");
                       }


                       context.Response.ContentType = new MediaTypeHeaderValue("application/json").ToString();
                       var jsonString = new JObject
                       {
                           ["ErrorCode"] = context.Exception.GetType().Name,
                           ["ErrorMessage"] = context.Exception.Message
                       };

                       context.Response.StatusCode = 401;

                       return context.Response.WriteAsync(jsonString.ToString());
                   }
               };
               options.RequireHttpsMetadata = false;
               options.SaveToken = true;
               options.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuerSigningKey = true,
                   IssuerSigningKey = new SymmetricSecurityKey(key),
                   ValidateIssuer = false,
                   ValidateAudience = false
               };
           });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseStaticFiles();


            app.UseCookiePolicy(new CookiePolicyOptions
            {
                HttpOnly = HttpOnlyPolicy.Always
            });

            app.UseCors(MyAllowSpecificOrigins);

            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseMvc();




            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("No api found!");
            });

        }
    }
}
