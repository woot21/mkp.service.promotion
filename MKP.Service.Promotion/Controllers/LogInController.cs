﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MKP.Service.Promotion.DTO.LogIn;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MKP.Service.Promotion.Controllers
{



    [ApiController]
    public class LogInController : ControllerBase
    {

        [HttpGet]
        [Route("api/[controller]/ec2")]
        public IActionResult Getec2()
        {
            return Ok("Hello From Jenkins Deploy");
        }

        [Route("/api/protected")]
        [Authorize]
        public string Protected()
        {
            return "Only if you have a valid token!";
        }


        [HttpGet]
        [Route("api/[controller]/test")]
        public IActionResult Get()
        {
            bool found = false;
            var r = Request;
            string jwtValue = "";
            foreach (var item in r.Cookies)
            {
                if (item.Key == "JWTKey")
                {
                    found = true;
                    jwtValue = item.Value;
                }
            }

            if (found)
            {
                bool isAuthorized = Authenticate(jwtValue);
                if (isAuthorized)
                {
                    return Ok(new string("OK To Go"));
                }
            }


            return Unauthorized();
        }


        [HttpPost]
        [Route("api/[controller]/test")]
        public IEnumerable<string> Post()
        {
            var r = Request;
            foreach (var item in r.Cookies)
            {
                System.Diagnostics.Debug.WriteLine(item);
            }

            return new string[] { "value3", "value4" };
        }



        [HttpPost()]
        [Route("api/[controller]/LogIn")]
        public async Task<IActionResult> LogIn([FromBody]Request_LogInEntity request)
        {
            if (request == null)
            {
                return BadRequest("Invalid client request");
            }

            if (request.UserName == "bankith" && request.Password == "bankith")
            {

                var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
                var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, request.UserName),
                    new Claim(ClaimTypes.Role, "Manager"),
                    new Claim("MyCustom", "BankithCustom")
                };

                var tokeOptions = new JwtSecurityToken(
                    issuer: "http://localhost:5555",
                    audience: "bankith",
                    claims: claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: signinCredentials
                );

                var tokenString = new JwtSecurityTokenHandler().WriteToken(tokeOptions);


                HttpContext.Response.Cookies.Append(
                "JWTKey",
                tokenString,
                new CookieOptions
                {
                    HttpOnly = true
                });

                return Ok(new { Token = tokenString });
            }
            else
            {
                return Unauthorized();
            }
        }


        internal bool Authenticate(string token)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("superSecretKey@345"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            List<Exception> validationFailures = null;
            SecurityToken validatedToken;
            var validator = new JwtSecurityTokenHandler();

            TokenValidationParameters validationParameters = new TokenValidationParameters();
            validationParameters.ValidIssuer = "http://localhost:5555";
            validationParameters.ValidAudience = "bankith";
            validationParameters.IssuerSigningKey = key;
            validationParameters.ValidateIssuerSigningKey = true;
            validationParameters.ValidateAudience = true;

            if (validator.CanReadToken(token))
            {
                ClaimsPrincipal principal;
                try
                {
                    principal = validator.ValidateToken(token, validationParameters, out validatedToken);
                    if (principal.HasClaim(c => c.Type == "MyCustom"))
                    {
                        if (principal.Claims.Where(c => c.Type == "MyCustom").First().Value == "BankithCustom")
                        {
                            return true;
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return false;
        }
    }
}
