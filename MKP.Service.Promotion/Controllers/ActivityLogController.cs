﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MKP.Service.Promotion.BSL.Factory;
using MKP.Service.Promotion.BSL.IService;
using MKP.Service.Promotion.DTO.Log;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MKP.Service.Promotion.Controllers
{
    [ApiController]
    public class ActivityLogController : ControllerBase
    {
        [HttpGet]
        [Route("api/[controller]/test")]
        public IEnumerable<string> Get()
        {

            HttpContext.Response.Cookies.Append(
                "CookieKey",
                "CookieValue",
                new CookieOptions
                {
                    HttpOnly = true
                });

            HttpContext.Response.Cookies.Append(
                "CookieKeyNotHttpOnly",
                "CookieValueNotHttpOnly",
                new CookieOptions
                {
                    HttpOnly = false
                });

            return new string[] { "value1", "value2" };
        }

        [HttpPost]
        [Route("api/[controller]/test")]
        public IEnumerable<string> Post()
        {
            var r = Request;
            foreach (var item in r.Cookies)
            {
                System.Diagnostics.Debug.WriteLine(item);
            }

            return new string[] { "value3", "value4" };
        }




        [HttpPost()]
        [Route("api/[controller]/SaveActivityLog")]
        public async Task<IActionResult> SaveActivityLog([FromBody] ActivityLogEntity request)
        {
            string result = "";

            try
            {
                using (IActivityLogService svc = ServiceFactory.GetActivityLogService())
                {
                    svc.SaveActivityLog(request);
                }

            }
            catch (Exception ex)
            {
                new UILException("AllBankInfo Error : " + ex.Message, true);
            }
            return StatusCode(200, result);
        }


    }
}
