﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using MKP.Service.Promotion.BSL.Factory;
using MKP.Service.Promotion.BSL.IService;
using MKP.Service.Promotion.DTO;
using MKP.Service.Promotion.DTO.Promotion;
using Microsoft.AspNetCore.Authorization;

namespace MKP.Service.Promotion.Controllers
{
    [ApiController]
    public class PromotionController : ControllerBase
    {
        private IConfiguration _configuration;

        SqlConnection connection;
        Dictionary<string, string> _config;
        public Dictionary<string, string> Config { get => _config; set => _config = value; }

        public PromotionController(IConfiguration Configuration)
        {
            _configuration = Configuration;
        }


        [HttpPost()]
        [Route("api/[controller]/ListPublishPromotion")]
        public async Task<IActionResult> ListPublishPromotion([FromBody]PromotionRequestEntity request)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            PromotionList reqData = new PromotionList();

            try
            {
                using (IPromotionService svc = ServiceFactory.GetListPublishPromotion())
                {
                    reqData = svc.ListPublishPromotion(request);
                }
            }
            catch (Exception ex)
            {
                reqData.Result = false;
                reqData.ErrorCode = "9999";
                reqData.ErrorDesc = "ListPublishPromotion : " + ex.Message;
                new UILException("ListPublishPromotion Error : " + ex, true);
            }
            return StatusCode(200, reqData);
        }

        //test
        //[HttpGet()]
        //[Route("api/[controller]/test")]
        //public PromotionRequestEntity test()
        //{
        //    PromotionRequestEntity xxx = new PromotionRequestEntity();
        //    return xxx;
        //}

        //test
        //[HttpGet()]
        //[Route("api/[controller]/test")]
        //public PromotionList test()
        //{
        //    PromotionList xxx = new PromotionList();
        //    return xxx;
        //}
    }
}
