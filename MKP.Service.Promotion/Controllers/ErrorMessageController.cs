﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using MKP.Service.Promotion.BSL.Factory;
using MKP.Service.Promotion.BSL.IService;
using MKP.Service.Promotion.DTO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MKP.Service.Promotion.Controllers
{
    [ApiController]
    public class ErrorMessageController : ControllerBase
    {
        [HttpGet()]
        [Route("api/[controller]/GetErrorMessageByGlobalCode/{errorCode}")]
        public async Task<IActionResult> GetErrorMessageByGlobalCode(string errorCode)
        {
            string result = "";
            ErrorMessageEntity respData = new ErrorMessageEntity();

            try
            {
                using (IErrorMessageService svc = ServiceFactory.GetErrorMessageService())
                    respData = svc.GetErrorMessageByGlobalCode(errorCode);

                result = JsonConvert.SerializeObject(respData);
            }
            catch (Exception ex)
            {
                new UILException("GetErrorMessageByGlobalCode Error : " + ex, true);
            }
            return StatusCode(200, result);
        }

    }
}
