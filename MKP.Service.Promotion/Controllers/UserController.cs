﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MKP.Service.Promotion.Helper;
using MKP.Service.Promotion.DTO;
using MKP.Service.Promotion.BSL.Factory;
using MKP.Service.Promotion.BSL.IService;
using System.Collections.Generic;

namespace MKP.Service.Promotion.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]User userParam)
        {
            var user = new DTO.User();
            try
            {
                using (IUsersService svc = ServiceFactory.GetUsersService())
                {
                    user = svc.Authenticate(userParam.Username, userParam.Password);

                    if (user == null)
                        return BadRequest(new { message = "Username or password is incorrect" });
                }

            }
            catch (Exception ex)
            {
                new UILException("Authenticate Error : " + ex.Message, true);
            }
            return StatusCode(200, user);
        }

        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            IEnumerable<User> users = new List<User>();
            try
            {
                using (IUsersService svc = ServiceFactory.GetUsersService())
                {
                    users = svc.GetAll();
                }
            }
            catch (Exception ex)
            {
                new UILException("GetAll Error : " + ex.Message, true);
            }
            return StatusCode(200, users);

        }
    }
}
