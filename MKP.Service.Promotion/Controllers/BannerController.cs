﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MKP.Service.Promotion.BSL.Factory;
using MKP.Service.Promotion.BSL.IService;
using MKP.Service.Promotion.DTO.Promotion;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MKP.Service.Promotion.Controllers
{
    [ApiController]
    public class BannerController : ControllerBase
    {
        private IConfiguration _configuration;

        public BannerController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost]
        [Route("api/[controller]/ListPublishBanner")]
        public async Task<IActionResult> ListPublishBanner([FromBody]BannerRequestEntity request)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            BannerList reqData = new BannerList();

            try
            {
                using (IBannerService svc = ServiceFactory.GetListPublicBanner())
                {
                    reqData = svc.ListPublicBanner(request);
                }
            }
            catch (Exception ex)
            {
                reqData.Result = false;
                reqData.ErrorCode = "9999";
                reqData.ErrorDesc = "ListPublishBanner : " + ex.Message;
                new UILException("ListPublishBanner Error : " + ex, true);
            }
            return StatusCode(200, reqData);
        }

        //test
        [HttpGet()]
        [Route("api/[controller]/test")]
        public BannerRequestEntity test()
        {
            BannerRequestEntity xxx = new BannerRequestEntity();
            return xxx;
        }
    }
}
