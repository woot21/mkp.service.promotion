﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
//using BOApi.Service.Factory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using MKP.Service.Promotion.BSL.Factory;
using MKP.Service.Promotion.BSL.IService;
//using MKP.Service.Promotion.DAL.Factory;
//using MKP.Service.Promotion.DAL.IData;
using MKP.Service.Promotion.DTO;
//using Trove.DTO;

namespace MKP.Service.Promotion.Controllers
{
    //[Route("api/[controller]/[action]")]
    //[Route("api/[controller]")]
    [ApiController]
    public class SystemConfigController : ControllerBase
    {
        private IConfiguration _configuration;

        public SystemConfigController(IConfiguration Configuration)
        {
            _configuration = Configuration;
        }

        [HttpGet()]
        [Route("api/[controller]/GetConfigByName/{moduleCode}/{configName}")]
        public async Task<IActionResult> GetSystemConfig(string moduleCode, string configName)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            SystemConfigEntity reqData = new SystemConfigEntity();
            try
            {
                using(ISystemConfigService svc = ServiceFactory.GetSystemConfigService())
                    reqData = svc.GetSystemConfig(moduleCode, configName);
                //using (ISystemConfigData svc = DataFactory.GetSystemConfigData())
                //    reqData = svc.GetSystemConfig(moduleCode, configName);
            }
            catch (Exception ex)
            {
                new UILException("GetSystemConfig Error : " + ex, true);
            }
            return StatusCode(200, reqData);
        }


        [HttpGet()]
        [Route("api/[controller]/ListModuleCode")]
        public async Task<IActionResult> ListModuleCode()
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            List<string> reqData = new List<string>();
            try
            {
                using (ISystemConfigService svc = ServiceFactory.GetSystemConfigService())
                    reqData = svc.ListModuleCode();
            }
            catch (Exception ex)
            {
                new UILException("ListModuleCode Error : " + ex, true);
            }
            return StatusCode(200, reqData);
        }

        

        [HttpGet()]
        [Route("api/[controller]/ListSystemConfig/{moduleCode}/{configName}/{pageIndex}/{pageSize}/{orderBy}")]
        //[Route("api/[controller]/ListSystemConfig/{moduleCode}/{pageIndex}/{configName}/{pageSize}/{orderBy}")]
        //[Route("api/[controller]/ListSystemConfig/{moduleCode}/{pageIndex}/{pageSize}/{orderBy}")]
        //[Route("api/[controller]/ListSystemConfig/{pageIndex}/{configName}/{pageSize}/{orderBy}")]
        //[Route("api/[controller]/ListSystemConfig/{pageIndex}/{pageSize}/{orderBy}")]
        public async Task<IActionResult> ListSystemConfig(string moduleCode = "", string configName = "", int pageIndex = 0, int pageSize = 50, string orderBy = "[MODULE_CODE]")
        {
            moduleCode = moduleCode.Replace("|||", "");
            configName = configName.Replace("|||", "");
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            SystemConfigList reqData = new SystemConfigList();
            try
            {
                using (ISystemConfigService svc = ServiceFactory.GetSystemConfigService())
                    reqData = svc.ListSystemConfig(moduleCode, configName, pageIndex, pageSize, orderBy);
            }
            catch (Exception ex)
            {
                new UILException("ListSystemConfig Error : " + ex, true);
            }
            return StatusCode(200, reqData);
        }

        [HttpGet()]
        [Route("api/[controller]/ListConfigByModuleCode/{moduleCode}")]
        public async Task<IActionResult> ListSystemConfigByModuleCode(string moduleCode)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            SystemConfigList reqData = new SystemConfigList();
            try
            {
                using (ISystemConfigService svc = ServiceFactory.GetSystemConfigService())
                    reqData = svc.ListSystemConfigByModuleCode(moduleCode);
            }
            catch (Exception ex)
            {
                new UILException("ListSystemConfigByModuleCode Error : " + ex, true);
            }
            return StatusCode(200, reqData);
        }

        [HttpPost()]
        [Route("api/[controller]/AddSystemConfig")]
        public async void AddSystemConfig([FromBody]SystemConfigEntity config)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                using (ISystemConfigService svc = ServiceFactory.GetSystemConfigService())
                    svc.AddSystemConfig(config);
            }
            catch (Exception ex)
            {
                new UILException("AddSystemConfig Error : " + ex, true);
            }
        }
        [HttpPut()]
        [Route("api/[controller]/UpdateSystemConfig")]
        public async void UpdateSystemConfig([FromBody]SystemConfigEntity config)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                using (ISystemConfigService svc = ServiceFactory.GetSystemConfigService())
                    svc.UpdateSystemConfig(config);
            }
            catch (Exception ex)
            {
                new UILException("UpdateSystemConfig Error : " + ex, true);
            }
        }
        [HttpDelete()]
        [Route("api/[controller]/DeleteSystemConfig/{moduleCode}/{configName}")]
        public async void DeleteSystemConfig(string moduleCode, string configName)
        {
            var resp = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                using (ISystemConfigService svc = ServiceFactory.GetSystemConfigService())
                    svc.DeleteSystemConfig(moduleCode, configName);
            }
            catch (Exception ex)
            {
                new UILException("DeleteSystemConfig Error : " + ex, true);
            }
        }



    }
}