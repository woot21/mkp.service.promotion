FROM mcr.microsoft.com/dotnet/core/sdk:2.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.sln ./
COPY MKP.Service.Promotion/*.csproj ./MKP.Service.Promotion/
COPY MKP.Service.Promotion.BSL/*.csproj ./MKP.Service.Promotion.BSL/
COPY MKP.Service.Promotion.DAL/*.csproj ./MKP.Service.Promotion.DAL/
COPY MKP.Service.Promotion.DTO/*.csproj ./MKP.Service.Promotion.DTO/
RUN dotnet restore

# Copy everything else and build
COPY . ./
WORKDIR MKP.Service.Promotion
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.1
WORKDIR /app
COPY --from=build-env app/MKP.Service.Promotion/out .
ENTRYPOINT ["dotnet", "MKP.Service.Promotion.dll"]