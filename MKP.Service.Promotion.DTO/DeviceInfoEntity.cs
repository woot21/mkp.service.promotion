﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKP.Service.Promotion.DTO
{
    public class DeviceInfoObject
    {
        [JsonProperty("deviceID")]
        public string DeviceID { get; set; }

        [JsonProperty("pageCode")]
        public string PageCode { get; set; }

        [JsonProperty("opercarrier")]
        public string Opercarrier { get; set; }

        [JsonProperty("macAddress")]
        public string MacAddress { get; set; }

        [JsonProperty("deviceSerialNo")]
        public string DeviceSerialNo { get; set; }

        [JsonProperty("deviceOS")]
        public string DeviceOS { get; set; }

        [JsonProperty("deviceOSVersion")]
        public string DeviceOSVersion { get; set; }

        [JsonProperty("appVersion")]
        public string AppVersion { get; set; }

        [JsonProperty("language")]
        public string Language { get; set; }

        [JsonProperty("myDeviceGuid")]
        public string MyDeviceGuid { get; set; }

        [JsonProperty("deviceModel")]
        public string DeviceModel { get; set; }

        [JsonProperty("deviceHardware")]
        public string DeviceHardware { get; set; }

        [JsonProperty("deviceToken")]
        public string DeviceToken { get; set; }
    }
}
