﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PSC.Frameworks.BSL.Core;

namespace MKP.Service.Promotion.DTO
{
    [Serializable]
    public class SystemConfigEntity : Base.BaseEntity
    {
        #region *** Members section ***

        #region [ Private members ]

        private string _configDescription;
        private string _configName;
        private string _configValue;
        private string _moduleCode;
        private string _providerCode;
        private bool _readOnly;
        private string _regularExpression;
        private string _updateBy;
        private DateTime? _updateDate;

        #endregion

        #endregion

        #region *** Properties section ***

        [DataMapping("MODULE_CODE")]
        public string moduleCode
        {
            get { return _moduleCode; }
            set { _moduleCode = value; }
        }


        [DataMapping("CONFIG_NAME")]
        public string configName
        {
            get { return _configName; }
            set { _configName = value; }
        }

        [DataMapping("CONFIG_VALUE")]
        public string configValue
        {
            get { return _configValue; }
            set { _configValue = value; }
        }


        [DataMapping("CONFIG_DESC")]
        public string configDescription
        {
            get { return _configDescription; }
            set { _configDescription = value; }
        }

        [DataMapping("READ_ONLY")]
        public bool readOnly
        {
            get { return _readOnly; }
            set { _readOnly = value; }
        }

        [DataMapping("REGULAR_EXP")]
        public string regularExpression
        {
            get { return _regularExpression; }
            set { _regularExpression = value; }
        }

        [DataMapping("PROVIDER_CODE")]
        public string providerCode
        {
            get { return _providerCode; }
            set { _providerCode = value; }
        }

        [DataMapping("UPDATED_DATE")]
        public DateTime? updatedDate
        {
            get { return _updateDate; }
            set { _updateDate = value; }
        }

        [DataMapping("UPDATED_BY")]
        public string updatedBy
        {
            get { return _updateBy; }
            set { _updateBy = value; }
        }

        #endregion
    }

    [Serializable]
    public class SystemConfigList : Base.TotalRecords
    {
        #region *** Members section ***

        #region [ Private members ]

        private List<SystemConfigEntity> _configs;

        #endregion

        #endregion

        public SystemConfigList()
        {
            _configs = new List<SystemConfigEntity>();
        }

        #region *** Properties section ***

        #region [ Public properties ]

        public List<SystemConfigEntity> configs
        {
            get { return _configs; }
            set { _configs = value; }
        }

        #endregion

        #endregion
    }
}
