﻿using System;
using MKP.Service.Promotion.DTO.Base;

namespace MKP.Service.Promotion.DTO.LogIn
{
    public class Request_LogInEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
