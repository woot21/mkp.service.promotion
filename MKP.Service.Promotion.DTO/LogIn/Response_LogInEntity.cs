﻿using System;

namespace MKP.Service.Promotion.DTO.LogIn
{
    public class Response_LogInEntity : Base.BaseEntity
    {
        public bool IsLogIn { get; set; }
    }
}
