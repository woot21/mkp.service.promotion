﻿using System;
using Newtonsoft.Json;
using PSC.Frameworks.BSL.Core;
using MKP.Service.Promotion.DTO.Enumeration;

namespace MKP.Service.Promotion.DTO.Log
{
    [Serializable]
    public class ActivityLogEntity : Base.BaseEntity
    {
        [DataMapping("ACT_TYPE")]
        [JsonProperty("ActType")]
        public ActivityType ActType { get; set; }

        [DataMapping("ACT_DATE")]
        [JsonProperty("ActDate")]
        public DateTime ActDate { get; set; }

        [DataMapping("ACT_DETAIL")]
        [JsonProperty("ActDetail")]
        public string ActDetail { get; set; }

        [DataMapping("CUST_ID")]
        [JsonProperty("CustID")]
        public string CustID { get; set; }

        [DataMapping("CIF_ID")]
        [JsonProperty("CifID")]
        public string CifID { get; set; }

        [DataMapping("SESSION_ID")]
        [JsonProperty("SessionID")]
        public string SessionID { get; set; }

        [DataMapping("MOBILE_NO")]
        [JsonProperty("MobileNo")]
        public string MobileNo { get; set; }

        [DataMapping("TRAN_ID")]
        [JsonProperty("TranID")]
        public int TranID { get; set; }

        [DataMapping("TRAN_TYPE")]
        [JsonProperty("TranType")]
        public TransactionType TranType { get; set; }

        [DataMapping("TRAN_CODE")]
        [JsonProperty("TranCode")]
        public string TranCode { get; set; }

        [DataMapping("DEVICE_ID")]
        [JsonProperty("DeviceID")]
        public string DeviceID { get; set; }

        [DataMapping("DEVICE_MODEL")]
        [JsonProperty("DeviceModel")]
        public string DeviceModel { get; set; }

        [DataMapping("DEVICE_OS")]
        [JsonProperty("DeviceOS")]
        public string DeviceOS { get; set; }

        [DataMapping("APP_VERSION")]
        [JsonProperty("AppVersion")]
        public string AppVersion { get; set; }

        [DataMapping("ERR_CODE")]
        [JsonProperty("ErrCode")]
        public string ErrCode { get; set; }

        [DataMapping("ERR_DESC")]
        [JsonProperty("ErrDesc")]
        public string ErrDesc { get; set; }

        [DataMapping("ACT_STATUS")]
        [JsonProperty("ActStatus")]
        public ActionStatus ActStatus { get; set; }

        [DataMapping("CREATED_DATE")]
        [JsonProperty("CreateDate")]
        public DateTime CreateDate { get; set; }

        [DataMapping("CREATED_BY")]
        [JsonProperty("CreatedBy")]
        public string CreatedBy { get; set; }

        [DataMapping("PAGE_CODE")]
        [JsonProperty("PageCode")]
        public string PageCode { get; set; }

        [DataMapping("PAGE_NAME")]
        [JsonProperty("PageName")]
        public string PageName { get; set; }

        [DataMapping("ACTION_BUTTON")]
        [JsonProperty("ActionButton")]
        public string ActionButton { get; set; }

    }
}
