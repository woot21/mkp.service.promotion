﻿using PSC.Frameworks.BSL.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKP.Service.Promotion.DTO.Log
{
    public class IntegrationLogEntity
    {
        [DataMapping("INTE_ID")]
        public Int64 InteID { get; set; }

        [DataMapping("INTE_DATE")]
        public DateTime? InteDate { get; set; }

        [DataMapping("INTE_TYPE")]
        public string InteType { get; set; }

        [DataMapping("TRAN_REF_CODE")]
        public string TranRefCode { get; set; }

        [DataMapping("CIF_ID")]
        public string CIFID { get; set; }

        [DataMapping("CUST_ID")]
        public string CustID { get; set; }

        [DataMapping("SESSION_ID")]
        public string SessionID { get; set; }

        //[DataMapping("CUSTOMER_NAME")]
        public string CustomerName { get; set; }

        [DataMapping("ACT_ID")]
        public int ActID { get; set; }

        [DataMapping("REQUEST_ID")]
        public string RequestID { get; set; }

        [DataMapping("PROCESS_TYPE")]
        public string ProcessType { get; set; }

        [DataMapping("SERVICE_NAME")]
        public string ServiceName { get; set; }

        [DataMapping("INTE_STATUS")]
        public string InteStatus { get; set; }

        [DataMapping("INTE_DETAIL")]
        public string InteDetail { get; set; }

        [DataMapping("INTE_DETAIL_1")]
        public string InteDetail1 { get; set; }

        [DataMapping("INTE_DETAIL_2")]
        public string InteDetail2 { get; set; }

        [DataMapping("INTE_DETAIL_3")]
        public string InteDetail3 { get; set; }

        [DataMapping("ERR_CODE")]
        public string ErrCode { get; set; }

        [DataMapping("ERR_DESC")]
        public string ErrDesc { get; set; }

        [DataMapping("CREATED_DATE")]
        public DateTime? CreatedDate { get; set; }

        [DataMapping("CREATED_BY")]
        public string CreatedBy { get; set; }

        [DataMapping("CHANNEL")]
        public int Channel { get; set; }

        [DataMapping("PARTNER_ID")]
        public string PartnerID { get; set; }
    }
}
