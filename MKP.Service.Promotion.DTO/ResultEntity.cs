﻿using System;
using System.Collections.Generic;
using PSC.Frameworks.Utility.Core;

namespace MKP.Service.Promotion.DTO
{
    [Serializable]
    public class ResultEntity : Base.BaseEntity
    {
        #region *** Members section ***

        #region [ Private members ]


        private bool _status;
        private string _resultID;
        private string _resultValue;
        //private string _errorCode;
        //private string _errorMessageTH;
        //private string _errorMessageEN;

        #endregion

        #endregion

        public ResultEntity Clone()
        {
            ////Serialize////
            byte[] arrByte = Serialization.SerializeArrayByte(this);
            ///Deserialize///
            return (ResultEntity)Serialization.DeserializeArrayByte(arrByte);
        }

        #region *** Properties section ***

        #region [ Public properties ]


        public bool Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string ResultID
        {
            get { return _resultID; }
            set { _resultID = value; }
        }

        public string ResultValue
        {
            get { return _resultValue; }
            set { _resultValue = value; }
        }


        //public string ErrorCode
        //{
        //    get { return _errorCode; }
        //    set { _errorCode = value; }
        //}

        //public string ErrorMessageTH
        //{
        //    get { return _errorMessageTH; }
        //    set { _errorMessageTH = value; }
        //}

        //public string ErrorMessageEN
        //{
        //    get { return _errorMessageEN; }
        //    set { _errorMessageEN = value; }
        //}

        #endregion

        #endregion

    }

    [Serializable]
    //[DataContract]
    public class ResultList : Base.TotalRecords
    {
        #region *** Members section ***

        #region [ Private members ]

        private List<ResultEntity> _result;

        //private string _errorCode;
        //private string _errorMessage;

        #endregion

        #endregion

        public ResultList()
        {
            _result = new List<ResultEntity>();
        }

        #region *** Properties section ***

        #region [ Public properties ]

        //[DataMember]
        public List<ResultEntity> Result
        {
            get { return _result; }
            set { _result = value; }
        }

        #endregion

        #endregion
    }
}
