﻿using PSC.Frameworks.BSL.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKP.Service.Promotion.DTO.Promotion
{
    [Serializable]
    public class PromotionEntity 
    {
        [DataMapping("PROMOTION_ID")]
        public int PromotionID { get; set; }

        [DataMapping("PARTNER_ID")]
        public int PartnerID { get; set; }

        [DataMapping("PRODUCT_ID")]
        public string ProductID { get; set; }

        [DataMapping("CAMPAIGN_ID")]
        public string CampaignID { get; set; }

        [DataMapping("PROMOTION_CODE")]
        public string PromotionCode { get; set; }

        [DataMapping("PROMOTION_NAME")]
        public string PromotionName { get; set; }

        [DataMapping("PROMOTION_TYPE")]
        public short PromotionType { get; set; }

        [DataMapping("PROMOTION_CATEGORY")]
        public short PromotionCategory { get; set; }

        [DataMapping("DISPLAY_HEADER")]
        public string DisplayHeader { get; set; }

        [DataMapping("DISPLAY_HEADER_EN")]
        public string DisplayHeaderEN { get; set; }

        [DataMapping("DISPLAY_DETAILS")]
        public string DisplayDetails { get; set; }

        [DataMapping("DISPLAY_DETAILS_EN")]
        public string DisplayDetailsEN { get; set; }

        [DataMapping("DISPLAY_TYPE")]
        public short DisplayType { get; set; }

        [DataMapping("DISCOUNT_PRICE")]
        public decimal DiscountPrice { get; set; }

        [DataMapping("PRICE")]
        public decimal Price { get; set; }

        [DataMapping("PROMOTION_IMAGE_URL")]
        public string PromotionImageURL { get; set; }

        [DataMapping("PROMOTION_IMAGE_URL_EN")]
        public string PromotionImageURLEN { get; set; }

        [DataMapping("PROMOTION_DETAIL_IMAGE_URL")]
        public string PromotionDetailImageURL { get; set; }

        [DataMapping("PROMOTION_DETAIL_IMAGE_URL_EN")]
        public string PromotionDetailImageURLEN { get; set; }

        [DataMapping("LINK_TO_PAGE")]
        public string LinkToPage { get; set; }

        [DataMapping("EFFECTIVE_DATE")]
        public DateTime EffectiveDate { get; set; }

        [DataMapping("EXPIRE_DATE")]
        public DateTime ExpireDate { get; set; }

        [DataMapping("PUBLISHED_DATE")]
        public DateTime PublishedDate { get; set; }

        [DataMapping("PUBLISHED_BY")]
        public string PublishedBy { get; set; }

        [DataMapping("STATUS")]
        public short Status { get; set; }

        [DataMapping("SORT_ORDER")]
        public decimal SortOrder { get; set; }

    }


    [Serializable]
    public class PromotionRequestEntity 
    {
        public short PromotionCategory { get; set; }
        public short PromotionType { get; set; }
        public int MaxLimit { get; set; }
        public short DisplayType { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

    }
    
    [Serializable]
    public class PromotionList : ResultEntity
    {
        #region *** Members section ***

        #region [ Private members ]

        private List<PromotionEntity> _promotionList;

        #endregion

        #endregion

        public PromotionList()
        {
            _promotionList = new List<PromotionEntity>();
        }

        #region *** Properties section ***

        #region [ Public properties ]

        public List<PromotionEntity> Promotion_List
        {
            get { return _promotionList; }
            set { _promotionList = value; }
        }

        #endregion

        #endregion
    }

}
