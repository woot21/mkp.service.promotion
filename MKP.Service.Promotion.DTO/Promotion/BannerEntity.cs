﻿using PSC.Frameworks.BSL.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MKP.Service.Promotion.DTO.Promotion
{
    [Serializable]
    public class BannerEntity
    {
        [DataMapping("CONTENT_ID")]
        public int CONTENT_ID { get; set; }

        [DataMapping("CONTENT_TYPE")]
        public short CONTENT_TYPE { get; set; }

        [DataMapping("CONTENT_NAME")]
        public string CONTENT_NAME { get; set; }

        [DataMapping("PARTNER_ID")]
        public int PARTNER_ID { get; set; }

        [DataMapping("CATEGORY_CODE")]
        public string CATEGORY_CODE { get; set; }

        [DataMapping("CATEGORY_NAME")]
        public string CATEGORY_NAME { get; set; }

        [DataMapping("DISPLAY_TYPE")]
        public short DISPLAY_TYPE { get; set; }

        [DataMapping("BANNER_PATH_TH")]
        public string BANNER_PATH_TH { get; set; }

        [DataMapping("BANNER_PATH_EN")]
        public string BANNER_PATH_EN { get; set; }

        [DataMapping("LINKTOPAGE_TH")]
        public string LINKTOPAGE_TH { get; set; }

        [DataMapping("LINKTOPAGE_EN")]
        public string LINKTOPAGE_EN { get; set; }

        [DataMapping("DISPLAY_BEGIN")]
        public DateTime DISPLAY_BEGIN { get; set; }

        [DataMapping("DISPLAY_END")]
        public DateTime DISPLAY_END { get; set; }

        [DataMapping("PUBLISH_DATE")]
        public DateTime PUBLISH_DATE { get; set; }

        [DataMapping("PUBLISH_BY")]
        public string PUBLISH_BY { get; set; }

        [DataMapping("SORT_ORDER")]
        public decimal SORT_ORDER { get; set; }

        [DataMapping("IS_ACTIVE")]
        public bool IS_ACTIVE { get; set; }
    }

    [Serializable]
    public class BannerRequestEntity
    {
        public short CONTENT_TYPE { get; set; }
        public short DISPLAY_TYPE { get; set; }
        //public int MKP_DISPLAY_MAX { get; set; }
        public int MAX_LIMIT { get; set; }
        public int PAGE_INDEX { get; set; }
        public int PAGE_SIZE { get; set; }
    }

    [Serializable]
    public class BannerList : ResultEntity
    {
        #region *** Members section ***

        #region [ Private members ]

        private List<BannerEntity> _bannerList;

        #endregion

        #endregion

        public BannerList()
        {
            _bannerList = new List<BannerEntity>();
        }

        #region *** Properties section ***

        #region [ Public properties ]

        public List<BannerEntity> Banner_List
        {
            get { return _bannerList; }
            set { _bannerList = value; }
        }

        #endregion

        #endregion
    }
}
