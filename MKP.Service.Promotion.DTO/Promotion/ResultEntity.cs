﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MKP.Service.Promotion.DTO.Promotion
{
    [Serializable]
    public class ResultEntity
    {
        #region *** Members section ***

        #region [ Private members ]
        private bool _result;
        private string _errorCode;
        private string _errorDesc;
        #endregion

        #endregion

        #region *** Properties section ***

        public bool Result
        {
            get { return _result; }
            set { _result = value; }
        }

        public string ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }

        public string ErrorDesc
        {
            get { return _errorDesc; }
            set { _errorDesc = value; }
        }

        #endregion


    }

}
