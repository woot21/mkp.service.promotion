﻿using System;
using System.Collections.Generic;
using System.Text;
using PSC.Frameworks.BSL.Core;

namespace MKP.Service.Promotion.DTO
{
    [Serializable]
    public class ErrorMessageEntity : Base.BaseEntity
    {
        #region *** Members section ***

        #region [ Private members ]

        private int? oid;
        private string responseCode;
        private int? storeID;
        private DateTime? createTime;
        private bool? status;
        private bool? deleted;
        private DateTime? lastModTime;
        private string responseMessage;
        private string internetMessage;
        private string mobileMessage;
        private string mobileMessageTH;

        [DataMapping("OID")]
        public int? OID
        {
            get { return this.oid; }
            set { this.oid = value; }
        }

        [DataMapping("RESPONSE_CODE")]
        public string ResponseCode
        {
            get { return this.responseCode; }
            set { this.responseCode = value; }
        }

        [DataMapping("STORE_ID")]
        public int? StoreID
        {
            get { return this.storeID; }
            set { this.storeID = value; }
        }
        [DataMapping("CREATION_TIME")]
        public DateTime? CreateTime
        {
            get { return this.createTime; }
            set { this.createTime = value; }
        }

        [DataMapping("STATUS")]
        public bool? Status
        {
            get { return this.status; }
            set { this.status = value; }
        }
        [DataMapping("DELETED")]
        public bool? Deleted
        {
            get { return this.deleted; }
            set { this.deleted = value; }
        }

        [DataMapping("LAST_MOD_TIME")]
        public DateTime? LastModTime
        {
            get { return this.lastModTime; }
            set { this.lastModTime = value; }
        }

        [DataMapping("RESPONSE_MESSAGE")]
        public string ResponseMessage
        {
            get { return this.responseMessage; }
            set { this.responseMessage = value; }
        }

        [DataMapping("INTERNET_MESSAGE")]
        public string InternetMessage
        {
            get { return this.internetMessage; }
            set { this.internetMessage = value; }
        }

        [DataMapping("MOBILE_MESSAGE")]
        public string MobileMessage
        {
            get { return this.mobileMessage; }
            set { this.mobileMessage = value; }
        }

        [DataMapping("MOBILE_MESSAGE_THAI")]
        public string MobileMessageTH
        {
            get { return this.mobileMessageTH; }
            set { this.mobileMessageTH = value; }
        }



        #endregion

        #endregion

        #region *** Properties section ***

        #region [ Public properties ]


        #endregion

        #endregion
    }

    [Serializable]
    public class ErrorMessageList : Base.TotalRecords
    {
        #region *** Members section ***

        #region [ Private members ]

        private List<ErrorMessageEntity> _error;

        #endregion

        #endregion

        public ErrorMessageList()
        {
            _error = new List<ErrorMessageEntity>();
        }

        #region *** Properties section ***

        #region [ Public ErrorMessageEntity ]

        public List<ErrorMessageEntity> Error
        {
            get { return _error; }
            set { _error = value; }
        }

        #endregion

        #endregion
    }
}