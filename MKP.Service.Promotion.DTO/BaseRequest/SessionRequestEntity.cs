﻿using System;
namespace Trove.MobileAPI.Transfer.DTO.BaseRequest
{
    public class SessionRequestEntity : BaseRequest.BaseRequestEntity
    {
        public string SessionID { get; set; }
    }
}
