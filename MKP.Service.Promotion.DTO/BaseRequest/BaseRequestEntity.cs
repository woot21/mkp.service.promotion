﻿using System;
namespace Trove.MobileAPI.Transfer.DTO.BaseRequest
{
    public class BaseRequestEntity
    {
        public string SessionID { get; set; }
        public string UserID { get; set; }
    }
}
