﻿using System;
namespace Trove.MobileAPI.Transfer.DTO.BaseRequest
{
    public class UserIDRequestEntity : SessionRequestEntity
    {
        public string UserID { get; set; }
    }
}
