﻿using System;

namespace MKP.Service.Promotion.DTO.Base
{
    [Serializable]
    public abstract class TotalRecords : ErrorEntity
    {
        #region *** Members section ***

        #region [ Private members ]

        private long _totalField;
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        #endregion

        #endregion

        #region *** Properties section ***

        #region [ Public properties ]

        public long TotalRecord
        {
            get { return _totalField; }
            set { _totalField = value; }
        }

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        #endregion

        #endregion
    }
}
