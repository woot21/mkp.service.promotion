﻿using PSC.Frameworks.BSL.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MKP.Service.Promotion.DTO.Base
{
    [Serializable]
    public abstract class BaseEntity : ErrorEntity
    {
        //#region *** Members section ***

        //#region [ Private members ]

        //private string _createdBy;
        //private DateTime? _createdDate;
        //private string _updatedBy;
        //private DateTime? _updatedDate;

        //#endregion

        //#endregion

        //#region *** Constructure section ***

        //public BaseEntity()
        //{
        //    this._createdBy = "";
        //    this._createdDate = DateTime.Now;
        //    this._updatedBy = "";
        //    this._updatedDate = DateTime.Now;

        //}

        //#endregion

        //#region *** Properties section ***

        //#region [ Public properties ]

        //[DataMapping("CREATED_BY")]
        //public string CreatedBy
        //{
        //    get { return this._createdBy; }
        //    set { this._createdBy = value; }
        //}

        //[DataMapping("CREATED_DATE")]
        //public DateTime? CreatedDate
        //{
        //    get { return this._createdDate; }
        //    set { this._createdDate = value; }
        //}

        //[DataMapping("UPDATED_BY")]
        //public string UpdatedBy
        //{
        //    get { return this._updatedBy; }
        //    set { this._updatedBy = value; }
        //}

        //[DataMapping("UPDATED_DATE")]
        //public DateTime? UpdatedDate
        //{
        //    get { return this._updatedDate; }
        //    set { this._updatedDate = value; }
        //}

        //#endregion

        //#endregion

        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;

        private string _createdByField;

        private System.Nullable<System.DateTime> _createdDateField;

        private string _updatedByField;

        private System.Nullable<System.DateTime> _updatedDateField;

        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute(IsRequired = true)]
        public string _createdBy
        {
            get
            {
                return this._createdByField;
            }
            set
            {
                this._createdByField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute(IsRequired = true)]
        public System.Nullable<System.DateTime> _createdDate
        {
            get
            {
                return this._createdDateField;
            }
            set
            {
                this._createdDateField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute(IsRequired = true)]
        public string _updatedBy
        {
            get
            {
                return this._updatedByField;
            }
            set
            {
                this._updatedByField = value;
            }
        }

        [System.Runtime.Serialization.DataMemberAttribute(IsRequired = true)]
        public System.Nullable<System.DateTime> _updatedDate
        {
            get
            {
                return this._updatedDateField;
            }
            set
            {
                this._updatedDateField = value;
            }
        }

    }
}
