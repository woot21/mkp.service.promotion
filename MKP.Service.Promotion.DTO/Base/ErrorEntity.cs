﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace MKP.Service.Promotion.DTO.Base
{
    [Serializable]
    public abstract class ErrorEntity 
    {
        #region *** Members section ***

        #region [ Private members ]

        private string _errorCode;
        private string _errorMessageTH;
        private string _errorMessageEN;
        private string _eaiErrorCode;
        private string _eaiErrorMessage;
       
        #endregion

        #endregion

        /* public ErrorEntity Clone()
        {
            ////Serialize////
            byte[] arrByte = PSC.Frameworks.Utility.Core.Serialization.SerializeArrayByte(this);
            ///Deserialize///
            return (ErrorEntity)PSC.Frameworks.Utility.Core.Serialization.DeserializeArrayByte(arrByte);
        } */

        #region *** Properties section ***

        #region [ Public properties ]

        public string EAIErrorCode
        {
            get { return _eaiErrorCode; }
            set { _eaiErrorCode = value; }
        }

        public string EAIErrorMessage
        {
            get { return _eaiErrorMessage; }
            set { _eaiErrorMessage = value; }
        }

        public string ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }


        private string _errorMessage; // KMA : CR31
        public string ErrorMessage  // KMA : CR31
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        public string ErrorMessageTH
        {
            get { return _errorMessageTH; }
            set { _errorMessageTH = value; }
        }

        public string ErrorMessageEN
        {
            get { return _errorMessageEN; }
            set { _errorMessageEN = value; }
        }

        public bool IsError
        {
            get
            {
                int errorInt = -1;
                bool bl = int.TryParse(_errorCode, out errorInt);

                if (bl && errorInt == 0) { return false; }
                else if (string.IsNullOrEmpty(_errorCode)) { return false; } //กรณีไม่มี error code ถือว่าไม่ error
                else if (_errorCode == "M8000") { return false; } // เป็น success code 
                else if (_errorCode == "SGW200") { return false; } // เป็น success code  (AYCAL)
                else if (_errorCode == "08000") { return false; } // เป็น success code  (AYCAP)
                else if (_errorCode == "0000") { return false; } // เป็น success code 
                else
                {
                    return true;
                }
                //return (  );
            }
            //set
            //{
            //    _errorBool = value;
            //}
        }

        #endregion

        #endregion

    }
}
