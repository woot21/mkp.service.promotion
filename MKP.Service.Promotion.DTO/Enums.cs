﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MKP.Service.Promotion.DTO.Enumeration
{
    public enum WalletRegisterationState
    {
        Begin = 0,
        Stage1 = 1,
        Stage2 = 2,
        Stage3 = 3,
        Stage4 = 4,
    }

    public enum UserStatus
    {
        ACTIVE = 1,
        SUSPEND = 2,
        LOCKED = 3,
        CANCEL = 4
    }
    public enum ActionStatus
    {
        Success = 1,
        Failed = 2
    }
    public enum LastStatus
    {
        Success = 4,
        Failed = 1
    }
    public enum ActionType
    {
        None = 0,
        Add = 1,
        Edit = 2,
        Delete = 3
    }

    public enum ContentType
    {
        Product = 1,
        Banner = 2,
        Promotion = 3,
        Notification = 4,
        Newsfeed = 5,
        InvestmentUpdate = 6,
        EventBooking = 7

    }

    public enum ProductStatus
    {
        Draft = 1,
        //Waiting = 2,
        WaitingApprover1 = 21,
        WaitingApprover2 = 22,
        Publish = 3,
        Rejected = 4,
        Expired = 5,
    }

    public enum EcouponStatus
    {
        Draft = 1,
        WaitingApprover1 = 21,
        WaitingApprover2 = 22,
        Publish = 3,
        Rejected = 4,
        Expired = 5,
    }

    public enum CallActionType
    {
        ApplyNow = 1,
        MoreDetail = 2,
        eMailtoBank = 3,
        eMailtoFriend = 4,
        CommentUs = 5,
        RegisterNow = 6,
        Share = 7,
        FindourBranch = 8,
        WatchVDO = 9,
        CallBank = 10
        //ApplyNow = 1,
        //MoreDetail = 2,
        //CallBack = 3,
        //WatchVideo = 4
    }

    #region Banking Service

    #region exist queue

    [Serializable]
    public enum AccountCustType
    {
        /// <summary>
        /// NULL or Empty
        /// </summary>    
        Normal = 0,
        HighValue = 1,// High Value Customer ** Not Use
        /// <summary>
        /// EB
        /// </summary>
        VIP = 2,// High Value Customer  ***EB
        /// <summary>
        /// CE
        /// </summary>
        PRIME = 3 // ***CE
    }

    #endregion

    public enum AccountManageType
    {
        INCOME = 1,
        EXPENSE = 2
    }
    public enum AccountType
    {
        Keyin = 0,
        Saving = 1,
        Current = 2,
        Fixed = 3,
        PersonalLoan = 4,
        HomeLoan = 5,
        CreditCard = 6,
        MutualFund = 7,
        AutoLoan = 8,
        NoCASAHaveATM = 9,
        NoCASANoATM = 10,
        Securities = 11,
        SMELoan = 12,           //newkol
        MRTALoan = 13,          //newkol
        PromissoryNote = 14,    //newkol
        LoanOther = 15,         //newkol
        CorpLoan = 16,           //newkol
        EWallet = 17 // CR WalletSystem

        //frAccCode values depend on “frAccNum” filed by: 
        //put 01 for Current acct 
        //format : xxx0yyyyyz,
        //put 11 for Saving acct format : xxx1yyyyyz,         
        //             xxx4yyyyyz   
        //       and xxx9yyyyyz 

        //put 21 for Fixed acct format xxx2yyyyy
        //put 33 for Loan acct format xxx3yyyyyz
        //put 51 for Mutual Fund acct format xxx8yyyyyz
    }
    public enum AccountGroup
    {
        BAY3rdAndORFT3rd = -1, // Fix for query
        Owner = 1,
        BAY3rd = 2,
        ORFT3rd = 3

    }
    public enum SmartDeviceType
    {
        None = 0,
        Mobile = 1,
        Tablet = 2,
        CCS = 3
    }
    public enum AccountStatus
    {
        Deleted = 0,
        Active = 1,
        Temporary = 2, //สำหรับบอกว่าเป็น บช ของ temp user
        DeleteByEngine = 3
    }

    public enum TransactionType
    {
        None = 0,
        TransferOwner = 1, //lov = 2
        //TransferBay3rd = 2, //lov = 3
        //TransferORFT3rd = 3, //lov = 4
        //BillPayment = 4, //lov = 6
        //TransferCardless = 5, //lov = 5
        //MobileTopup = 6, //lov = 7
        //RedeemCash = 7, //lov = 8
        //RedeemGift = 8, //lov = 9
        //BuyFund = 9, //lov = 10 Subscription
        //SellFund = 10, //lov = 11 Redemption
        //SwitchFund = 11, //lov = 12 Switching
        //CustomerInbox = 12,
        //ChangeMobileNumber = 13,
        //UnLockOTP = 14,
        //ChangePassword = 15,
        //ResetPassword = 16,
        //CancelCardless = 17,
        //AYCAPRedemption = 18,
        //BillPaymentKSS = 19,

        //AYCAPGetCashRevolving = 20,
        //AYCAPGetCashInstallment = 21,

        //LoanWithDraw = 22,      //cr31

        //TransferDonation = 24,
        //TransferSmart = 25,
        //TransferBahtNet = 26,
        //TransferSwift = 27,
        //TransferForeignDraft = 28,

        //WesternUnionSendMoney = 36,

        //ApplyAutoDebit = 42,
        //RequestCreditBureau = 43,

        //LoanOwner = 46,         //cr31
        //LoanOther = 47,          //cr31



        //RegisterAnyIDByPID = 67,
        //RegisterAnyIDByMobile = 68,
        //ChangeAnyID = 69,
        //CancelAnyID = 70,
        //PreRegisterAnyIDByPID = 71,
        //PreRegisterAnyIDByMobile = 72,

        ////P2P
        //TransferP2PMobileOnUs = 78,
        //TransferP2PMobileOffUs = 79,
        //TransferP2PCitizenOnUs = 80,
        //TransferP2PCitizenOffUs = 81,
        //TransferP2PMobile = 82,
        //TransferP2PCitizen = 83,
        //ApplySMSBanking = 84,
        //MobileAtATM = 85,

        //ApplyLoan = 92,
        //ApplyCreditCard = 93,

        //TopupeWallet = 96, // CR eWallet
        //ScanToTransfer = 97, // CR Scan to pay
        //ScanToEWallet = 98, // CR Scan to pay
        //GenerateQRC = 99, // CR Scan to pay

        //ApplyEWallet = 100, // CR11 EwalletSystem
        //ScanToPay = 101, // CR11 EwalletSystem

        //RegisterSendRTP = 102,
        //RegisterReceiveRTP = 103,
        //DeRegisterSendRTP = 104,
        //DeRegisterReceiveRTP = 105,
        //SendRequestRTP = 106,
        //TransferRequestRTP = 107,
        //PaymentRequestRTP = 108,

    }
    public enum TransactionTypeDisplay
    {
        TransferOwner = 1,
        TransferBay3rd = 2,
        TransferORFT3rd = 3,
        BillPayment = 4,
        TransferCardless = 5,
        MobileTopup = 6,
        RedeemCash = 7,
        RedeemGift = 8,
        BuyFund = 9,
        SellFund = 10,
        SwitchFund = 11,
        CancelCardless = 17,
        AYCAPGetCashRevolving = 20,
        AYCAPGetCashInstallment = 21
    }

    public enum ActivityType
    {
        Login = 1,
        Logout = 2,
        Register = 3,
        //RegisterKOL = 3,
        //RegisterKCC = 4,
        //RegisterCASA = 5,
        //RegisterKOLTemp = 6,
        //RegisterKCCOffice = 7,
        AddProduct = 4,
        ChangeMobileNumber = 5,
        UnLockOTP = 6,
        ChangePassword = 7,
        ResetPassword = 8,
        EditProfile = 9,
        UnLockUser = 10,
        DeleteMyAccount = 11,
        DeleteFavoriteAccount = 12,

        TransferOwner = 13,
        TransferBay3rd = 14,
        TransferORFT3rd = 15,
        BillPayment = 16,
        TransferCardless = 17,
        MobileTopup = 18,
        RedeemCash = 19,
        RedeemGift = 20,
        BuyFund = 21,
        SellFund = 22,
        SwitchFund = 23,

        CheckRegisKOL = 24,
        //taweporn.ch add
        CancelUser = 25,
        ChangeCustomerProfile = 26,
        AYCAPGetCashRevolving = 27,
        AYCAPGetCashInstallment = 28,
        AYCAPRedemption = 29,



        // CR031
        AddFavoriteTransfer = 43,
        EditFavoriteTransfer = 44,
        //DeleteFavoriteTransfer = 45, ****ต้องใช้  DeleteFavoriteAccount = 12 ค่ะ เนื่องจากส่ง Fraud ด้วยเลขนี้

        ApplyAutoDebit = 52,      //surasa.so
        RequestBureau = 53,      //surasa.so
        LoanWithDraw = 63,  //Dilok
        LoanOwner = 64, //Dilok
        LoanOther = 65, //Dilok

        AuthenticationKMA = 69,
        PromoteUser = 70,     //promote temp - surasa.so

        RequestNewTempPassword = 76, // Added Pranop B. (on 10/02/2015)

        // Same KOL
        AddFavoriteBiller = 85,
        EditFavoriteBiller = 86,
        DeleteFavoriteBiller = 87,

        //P2P
        TransferP2PMobileOnUs = 114,
        TransferP2PMobileOffUs = 115,
        TransferP2PCitizenOnUs = 116,
        TransferP2PCitizenOffUs = 117,
        TransferP2PMobile = 118,
        TransferP2PCitizen = 119,

        CreditCardAutoUpdate = 120,
        ApplySMSBanking = 121,

        //NeewKMA
        //UnLockPin = 6, // รวมกับ UnlockOTP
        ChangePin = 122,
        SetupPIN = 123,
        AddOrReplaceDevice = 124,
        NotYouDevice = 125,

        ResetPINAllDevice = 128,
        MobileATM = 129,


        ApplyLoan = 132,
        ApplyCreditCard = 133,


        TopupeWallet = 138, // CR eWallet
        ScanToTransfer = 139, // CR Scan to pay
        ScanToEWallet = 140, // CR Scan to pay
        GenerateQRC = 141, // CR Scan to pay
        ApplyEWallet = 142, // CR11 EwalletSystem
        ScanToPay = 143,

        //RTP
        RegisterSendRTP = 145,
        RegisterReceiveRTP = 146,
        DeRegisterSendRTP = 147,
        DeRegisterReceiveRTP = 148,
        SendRequestRTP = 149,
        TransferRequestRTP = 150,
        PaymentRequestRTP = 151,
        RegisterRTP = 152, // for accept t&c และ requestOTP เนื่องจากยังไม่ทราบว่าจะเลือกอะไร ระหว่าง send / receive
        DeRegisterRTP = 153, // for requestOTP เนื่องจากยังไม่ทราบว่าจะเลือกอะไร ระหว่าง send / receive



        CheckDevice = 7777,
        SystemCheck = 8888,
        CatchException = 9999       //for insert LOG_ACT
    }

    public enum ActionLoginType
    {
        LogIn = 0,
        LogOut = 1
    }
    public enum RecurringType
    {
        None = 0,
        Now = 1,
        Daily = 2,
        Weekly = 3,
        Monthly = 4,
        OneTimeSchedule = 5,
        Biweekly = 6,       //cr31: for display newkol
        Bimonthly = 7,      //cr31: for display newkol
        Quarterly = 8,      //cr31: for display newkol
        Semiannual = 9,     //cr31: for display newkol
        Annual = 10         //cr31: for display newkol
    }

    public enum TransactionState
    {
        PreTran = 1,
        ConfirmTran = 2,
        ConfirmOTP = 3,
        TranFinish = 4,
        RequestMobileAtATMSuccess = 14, //สร้างรายการทำสำเร็จรอกดเงินที่ตู้ NEWKMA Mobile@ATM
        ATMTransactionFail = 15, //NEWKMA Mobile@ATM
    }
    public enum TxnStatus
    {
        Success = 1,
        Failed = 2
    }

    public enum OTPStatus
    {
        INACTIVE = 0,
        ACTIVE = 1,
        LOCKED = 2,
        SUCCESS = 3,
        EXPIRED = 4,
        FAILED = 5
    }

    public enum PINStatus
    {
        INACTIVE = 0,
        ACTIVE = 1,
        LOCKED = 2,
        SUCCESS = 3,
        FAILED = 4
    }

    #endregion

    #region LandingPage
    public enum ScheduleType
    {
        Transfer = 1,
        BillPayment = 2,
        Duedate = 3
    }

    public enum GateWayMessageCode
    {
        Success = 0000,
        //TransactionComplete = 70000,
        TransactionComplete = 0000,
        UnSuccessTransaction = 00499,

        //LDAP CustInfo
        GetCustInfoDataNotFound = 9951,
        ConvertCustInfoToEntityFailed = 9952,


        //AYCAP
        AYCAPVerifyFailed = 9941,
        AYCAPCreditLimitNotMatch = 9942,


        DataNotFound = 9961,
        InvalidParameter = 9962,
        //InvalidParameter = 0012,
        //MaxStatementPeriod = 0013,
        AlreadyExistAccount = 9963,

        //RequestIsNullOrEmpty = 0015,
        //ResponseIsNullOrEmpty = 0016,
        //ResponseIsFailed = 0017,

        ////FailedDelete = 0020,
        ////FailedAdd = 0021,
        ////FailedUpdate = 0022,
        //TransactionFailed = 0020,

        ////FailedEAIAuthen = 0031,
        ////FailedEAIProvideInfo = 0032,
        //GatewayFailed = 0030,

        //FailedDataRequired = 0040,
        //FailedAgeBelow = 0041,
        //FailedAlready = 0042,
        //FailedNoAccountTypeCASAOrCreditCard = 0043,
        //FailedDataNotMatch = 0044,

        ////tranfer/pay flow
        //InvalidTransferPaymentFlow = 9864,

        ////OTP        
        OTPVerifyFailed = 9971,
        OTPExpired = 9972,
        OTPLocked = 9973,
        OTPRequestFail = 9974,

        IOSRejected = 9975,
        ////LDAPFailed = 0071,//LDAP
        ////AYCAPFaile = 0081,//AYCAP

        Other = 99999
    }

    public enum NotifyType
    {
        SMS = 1,
        Email = 2
    }

    public enum NotifyStatus
    {
        Success = 1,
        Failed = 2
    }

    public enum LocationType
    {
        Branch = 1,
        ATM = 2
        //,
        //Restaurant =3,
        //Shopping = 4
    }

    #endregion

    #region Register
    public enum RegisterType
    {
        KOL = 1,
        KCC = 2,
        CASA = 3,
        CASAWOCard = 4,
        KCCOffline = 5,
        AYCALOnline = 6,
        AYCAL = 7,
        // CR31
        KOLOnline = 8,
        KOLAtmOrBranch = 9, //KOLTempUser = 9,
        KOLAppform = 10,
        KOLMigrate = 11,
        KOLBizUser = 12, //for new user by bizadmin
        KMAAppform = 13,
    }

    public enum RegisterChannel
    {
        KMA = 1
    }

    #endregion

    #region Account

    public enum RequestBy
    {
        Customer = 1,
        Operation = 2
    }

    public enum RequestType
    {
        //กรณี เพิ่ม        = หากต้องการให้แสดงที่ BackEnd ให้ไปเพิ่มที่ RequestTypeDisplay ด้วย
        //กรณีแก้ไข หรือ ลบ = ต้องทำไปทำที่ RequestTypeDisplay ด้วย!!!
        AddProduct = 1,
        UnlockOTP = 2,
        ChangeMobileNumber = 3,
        UnlockUser = 4,
        RequestCancelUser = 5,
        RequestUnlockUserOTP = 6, //kma
        ChangeCustomerProfile = 7,
        Register = 8,
        SwitchOTP24H = 11,
        SuspendUser = 12,
        ActiveUser = 13,
        DeleteMobile = 14,
        ClearSession = 15,
        CancelKOL = 16,
        CancelTempUser = 18,
        BlockCRM = 19,
        ChangeOTPChannel = 20,
        ForgotPassword = 21,
        UnlockTempUser = 22,
        ChangeMobileTempUser = 23,
        ChangeOTPMobileAndEmail = 24,
        AddBizAdmin = 25,        //surasa.so
        RegisterWU = 26,
        EditBizAdmin = 27,        //surasa.so
        EditProduct = 31,        //surasa.so
        DeleteProduct = 32,        //surasa.so
        ChangeEmail = 45,
        RequestNewTempPassword = 46,
        EditCompanyProfile = 47
    }

    public enum RequestTypeOnline
    {
        AddAccount = 1,
        UnlockOTP = 2,
        ChangeMobileNumber = 3,
        RequestUnlockUser = 4,
        RequestCancelUser = 5,
        RequestUnlockUserOTP = 6,
        ChangeCustomerProfile = 7,
        ChangePassword = 8,
        ResetPassword = 9,
        DeleteAccount = 10,
        CreditCardAutoUpdate = 11,
        RequestSuspendCard = 12,
        RequestSuspendAccount = 13,
        RequestRegisterYellowPoints = 17,
        ApplyAISmPay = 33,
        ApplyAutoDebit = 36,
        RequestCreditBureau = 37,
        RequestNewTempPassword = 46,
        EditCompanyProfile = 47,
        ChangePin = 50,
        ResetPINAllDevice = 51,
    }

    public enum RequestStatus
    {
        Waiting = 1,
        WaitingForApprove = 2,
        WaitingForFinalApprove = 3,
        Approve = 4,
        Reject = 5
        //,Cancel = 6
    }

    #endregion

    public enum DeviceOS
    {
        IOS = 0,
        ANDROID = 1,

        // pittaya.ph - 17/7/2556
        IOS_IPHONE = 2,
        IOS_IPAD = 3,
        AMAZON = 4,
        BLACKBERRY = 5,
        CHROME = 6,
        WINDOWS = 7,
        WINDOWSPHONE = 8
    }

    public enum BU
    {
        Krungsri = 1,
        CreditCard = 2,
        KSAM = 3,
        KrungsriExclusiveBanking = 4,
        KrungsriSME = 5,
        CorporateBanking = 6,
        KrungsriAuto = 7,
        Srisawad = 8,
        KSS = 9
    }



    public enum CategoryType
    {
        PersonalLoan = 9
    }

    public enum SystemStatus
    {

        Closed = 0,
        Open = 1
    }

    public enum FileTransferType
    {
        Image = 1,
        Document = 2
    }
    public enum CustType
    {
        All = 1,
        Personal = 2,
        Corporation = 3,
        ExclusiveBanking = 4
    }
    //CustType = 
    //1	All
    //2	Personal
    //3	Corporation
    //4	Exclusive Banking

    public enum AYCALVerifyType
    {
        AddOrForget = 2,
        Registration = 4
    }

    public enum TriggerType
    {
        BillPayment = 1,
        Transfer = 2
    }

    public enum EventTrigger
    {
        FirstRegister = 1,
        BillPayment = 2,
        Transfer = 3,
        Lead = 4,
        Login = 5,
        Birthday = 6
    }

    public enum CampaignAction
    {
        Display = 11,
        Click_OwnAccountTransfer = 1,
        Click_OtherAccountTransfer = 2,
        Click_CardlessTransfer = 3,
        Click_CancelCardless = 4,
        Click_BillPayment = 5,
        Click_MobileTopup = 6,
        Click_MutualFunds = 7,
        Click_ContentDetail = 8,
        Click_URLLanding = 9,
        Click_Applynow = 10
    }

    public enum BannerButtonTheme
    {
        White = 1,
        MaroonGray = 2
    }

    public enum EAppResult
    {
        Failed = 0,
        Success = 1
    }


    public enum ListCustomerBy
    {
        MobileNo,
        Email,
        CitizenID,
        UserName
    }
    public enum UpdateCustomerBy
    {
        MobileNo,
        Email
    }

    #region CR31
    public enum Channel
    {
        KMA = 1,
        KOL = 2,
        KBOL = 3
    }
    public enum TranFavType
    {
        // KMA ใช้แค่ TrdPartyBAY, ORFT //
        TrdPartyBAY = 1,
        ORFT = 2,
        SMART = 3,
        BAHTNET = 4,
        SWIFT = 5,
        ForeignDRAFT = 6,
        P2P = 7,
        EWallet = 8,
    }
    public enum RequestChannel //changemobno offline
    {
        KMA = 1,
        KOL = 2,
        KBOL = 3
    }
    public enum CustomerStatus
    {
        TEMP_EXISTING_KOL = -1,     //คือ temp ของ kol ระบบเดิม  //new kol ห้ามใช้
        INACTIVE = 0,
        ACTIVE = 1,
        LOCK = 2,
        SUSPEND = 3,
        CANCEL = 4,
        CANCEL_FROM_MERGE = 5,
        TEMP_PROMOTED = 6,
        NONE = 9,
        CANCEL_BY_SYSTEM = 7
    }
    public enum AppFormType
    {
        Register = 1,
        Signin = 2,
        AddAccount = 3,
        ApplicationForm = 4,
        ForgotPassword = 5,
        RequestUnlockTempPassword = 6,
        AddBizAdmin = 7,
        EditBizAdmin = 8,
        RegisterWesternUnion = 9,
        EditAccount = 10,
        DeleteAccount = 11,
        RequestChangeServiceAppForm = 12
    }
    public enum OTPChannel
    {
        SMS = 1,
        Email = 2,
        HardToken = 3
    }
    public enum RequestChangeType
    {
        None = 0,
        AddAccount = 1,
        CancelKrungsri = 2,
        CancelTempUser = 3,
        ChangeEmail = 4,
        ChangeMobileOTP = 5,
        ChangeOTPChannel = 6,
        ChangePassword = 7,
        UnlockOTP = 8,
        UnlockTempUser = 9,
        ChangeMobileOTPAndEmail = 10,

        AddBizAdmin = 11,
        EditBizAdmin = 12,
        EditAccount = 13,
        DeleteAccount = 14,

        ForgotResetPassword = 15,
        ChangeMobile = 16,
    }
    #endregion

    /// <summary>
    /// OTP Template ID by Table KOL_MST_OTP_TEMPLATE
    /// </summary>
    public enum OTPTemplate
    {
        Change_Mobile_Email = 1,
        Change_Reset_Password = 2,
        Unlock_OTP = 3,
        AddAccount_Deposit = 4,
        AddAccount_Loan = 5,
        AddAccount_MutualFunds = 6,
        AddAccount_Securities = 7,
        AddAccount_CreditCard = 8,
        CancelKOL = 9,
        Transfer_Now = 10,
        Transfer_Schedule = 11,
        TransferSwift = 12,
        TransferForeignDraft = 13,
        TransferDonation = 14,
        Western_Register = 15,
        Western_Send = 16,
        Western_Recive = 17,
        BillPay_Now = 18,
        BillPay_Schedule = 19,
        LoanWithdrawal = 20,
        LoanPayment_Now = 21,
        LoanPayment_Schedule = 22,
        CoopDeposit_Now = 23,
        CoopDeposit_Schedule = 24,
        CoopPayment_Now = 25,
        CoopPayment_Schedule = 26,
        Apply_Auto_Debit = 27,
        Apply_AIS_mPay = 28,
        NCB = 29,
        SuspendAccount = 30,
        SuspendCard = 31,
        TransferGroup_Now = 32,
        TransferGroup_Schedule = 33,
        AddGroup = 34,
        EditGroup = 35,
        DeleteGroup = 36,
        TransferMulti_Now = 37,
        TransferMulti_Schedule = 38,
        AddNewUser_BizAdmin = 39,
        EditNewUser_BizAdmin = 40,
        AddNewUser_Normal = 41,
        EditNewUser_Normal = 42,
        DeleteNewUser_Normal = 43,
        Regiter_YellowPoint = 44,
        TransferCardLess = 45,
        AddAccount_AYCAL = 46, // phase 2
        EditRoleUser = 47, // 2015-05-27
        //AddAccount_SME = 44,
        //EditAccount_SME = 45,
        Change_Mobile_KMA = 48, // 2015-11-10 CR037

        // NewKMA
        Change_Pin = 54,
        Unlock_Pin = 55,

        Default = 999,

    }
    /// <summary>InboxTemplate</summary>
    public enum InboxTemplate
    {
        TransferOwner = 900,
        TransferBay3rd = 901,
        TransferCardless = 902,
        CancelCardless = 903,
        BillPayment = 904,
        MobileTopup = 905,
        BuyFund = 906,
        SellFund = 907,
        SwitchFund = 908,
        RedeemCash = 909,
        RedeemGift = 910,
        StopCheque = 911,
        TransferSwift = 912,
        TransferForeignDraft = 913,
        TransferSmart = 914,
        TransferBahtNet = 915,
        TransferORFT3rd = 916,
        COOPWithdraw = 917,
        COOPDeposit = 918,
        COOPLoanPayment = 919,
        COOPLoan = 920,
        LoanOwner = 921,
        LoanOther = 922,
        LoanWithDraw = 923,
        AccountAlertEmail = 924,
        AccountAlertInBox = 925,
        TransferGroup = 926,
        TransferDonation = 927,
        WUSendMoney = 928,
        WUReceiveMoney = 929,
        TransferFCDOwnerSameCurrency = 930,
        TransferFCDOwnerFCD2Thai = 931,
        TransferFCDOwnerCrossCurrency = 932,
        TransferFCDBay3rdSameCurrency = 933,
        TransferFCDBay3rdFCD2Thai = 934,
        TransferFCDBay3rdCrossCurrency = 935,
        TransferSmartCredit = 936,
        AutoUpdateCreditCard = 937
    }

    /* เลิกใช้

    public enum MergeCase
    {
        UNIQUE_KOL = 1,     //AX = unique exkol 
        UNIQUE_KMA = 2,     //AM = unique kma

        SAMECID_KOL = 3,    //B1 KOL = same username + same citizen & select prefer exkol
        SAMECID_KMA = 4,    //B1 KMA = same username + same citizen & select prefer kma

        SAMECID_KOL_J = 8,    //B1 KOL = same username + same citizen & login by exkol + hv JOINT acc
        SAMECID_KMA_J = 9,    //B1 KMA = same username + same citizen & login by kma + hv JOINT acc

        DIFFCID_KOL = 5,    //B2X = same username + diff citizen & login by exkol
        DIFFCID_KMA = 6,    //B2M = same username + diff citizen & login by kma

        INIT_CORP_USER = 7  //generate corp user (เป็น general user ที่ generate จาก admin [ax] มา 1stSignin )
    } */

    public enum WebSEALChannel
    {
        KMA,
        KOL,
        ICE
    }

    public enum GroupManageType  //CR49
    {
        Asset = 1,
        Liabilities = 2,
        CASA = 3,
        CASAFixLoan = 4,
        CASAEWallet = 5 // CR11 WalletSystem
    }

    public enum LoanType
    {
        None = 0,
        Installment = 1,
        Revolving = 2,
    }

    //NewKMA
    public enum LoginBy
    {
        None = 0,           //ไม่ส่ง
        FingerPrint = 1,    //touch id iOS
        Button = 2,         //customer click
        Automatic = 3,      //re-authen for register or promote temp
    }

    public enum BillerInputType
    {
        Required = 1,
        Optional = 2,
        Invisible = 3
    }

    public enum ReceiptAddressType
    {
        Draft = 1,
        Residential = 2,
        Office = 3,
    }

    public enum PIN_OTP_Flow
    {
        NONE = 0,
        TransferBay3rd = 1,
        TransferORFT3rd = 2,
        TransferP2P = 3,
        LoanOther = 4,
        LoanWithDraw = 5,
        WithdrawWithCode = 6,

        BillPayment = 7,
        MobileTopup = 8,

        ApplyAutoDebit = 9,
        RequestCreditBureau = 10,

        AddAccount = 11,

        ChangeMobileNumber = 12,
        ChangePassword = 13,
        ChangePIN = 14,
        ForgotPassword = 15,
        ForgotPIN = 16,
        UnLockOTP_PIN = 17,
        ManageDevice = 18,
        CancelUser = 19,


        OpeningAccountOnline = 20,

        RegisterAnyID = 21,
        RegisterSMSBanking = 22,

        FirstSignInNewKMA = 23,

        AYCAPGetCashRevolving = 24,
        AYCAPGetCashInstallment = 25,
        AddOrReplaceDevice = 26,
        NotYouDevice = 27,
        MobileAtATM = 28,
        ConfirmNewDevice = 29,
        TopupEWallet = 30,
        ScanTransfer = 31,
        ScanEWallet = 32,

        ApplyDigitalLending = 33,
        ScanToPay = 34,

        RegisterRTP = 35,
        DeRegisterRTP = 36,
        SendRequestRTP = 37,
        TransferRequestRTP = 38,
        PaymentRequestRTP = 39,
    }

    // pittaya.ph (ประเภทการบัตรเครดิต)
    public enum CreditCardType
    {
        /// <summary>ไม่เจอ หรือ error</summary>
        None = 0,
        /// <summary>CreditCard (ยกเว้น PersonalLoan, SaleFinance)</summary>
        CreditCard = 1,
        /// <summary>PersonalLoan (TESCO)</summary>
        PersonalLoan = 2,
        /// <summary>SaleFinance (First Choice)</summary>
        SaleFinance = 3,
        /// <summary>TESCO VISA</summary>
        TescoVisa = 4,
        /// <summary>First Choice 3 IN 1</summary>
        FirstChoice3in1 = 5,

    }

    // ImageProfile - Server
    public enum FileServerType
    {
        /// <remarks/>
        Temporary = 1,

        /// <remarks/>
        FileServer = 2,

        /// <remarks/>
        FileServerAYCAL = 3
    }
    public enum LoadFileType
    {
        // รูปโปรไฟล์ของ user
        Profile = 1,

        // รูปบัญชีที่เปลี่่ยน 
        // *สำหรับเมนูด้านบน และหน้าที่แสดงรูปบัญชี ( rolling, acc summary, etc. )
        Account = 2,

        BillPayment = 3,

        AutoDebit = 4,

        Favorite = 5,

        AYCALSpecialOffer = 6,

        DigitalLending = 7
    }
    public enum CropImageFlow
    {
        EditProfile,
        CompanyProfile,
        FavoriteBiller,
        AccountNickName
    }
    public enum CustomerType
    {
        Other = 0,
        RetailNormal = 1,
        SMEIndividual = 2,
        SMECorp = 3,
        Corp = 4,
        CorpAdmin = 5,
        CorpGeneral = 6,
        DualAdmin = 7,
        DualGeneral = 8,
        SMESingle = 9
    }
    public enum NotificationType
    {
        None = 0,
        Welcome = 1,
        Privilege = 2,
        RequestToPay = 3,
        PushMessage = 4,

        SystemAnnouncement = 99
    }

    public enum IDTypeAYCAP
    {
        /// <summary>0=ไม่ได้ระบุ</summary>
        [EnumMember(Value = "0")]
        None = 0,

        // <summary>1=บัตรประชาชน</summary>
        [EnumMember(Value = "1")]
        ID_Card = 1,

        // <summary>2=บัตรข้าราชการ</summary>
        [EnumMember(Value = "2")]
        Government_Official_Card = 2,

        // <summary>3=หน่วยงานราชการ</summary>
        [EnumMember(Value = "3")]
        Government = 3,

        // <summary>4=ทะเบียนการค้า</summary>
        [EnumMember(Value = "4")]
        Trade_register = 4,

        // <summary>5=ต่างด้าว</summary>
        [EnumMember(Value = "5")]
        Foreigner = 5,

        // <summary>6=Passport คนไทย</summary>
        [EnumMember(Value = "6")]
        Passport_Thai = 6,

        // <summary>7=Passport คนต่างชาติ</summary>
        [EnumMember(Value = "7")]
        Passport_Foreigner = 7,

        /// <summary>9=อื่นๆ</summary>
        [EnumMember(Value = "9")]
        Other = 9,

    }
    public enum MailingAddressAYCAP
    {
        [EnumMember(Value = "")]
        None = 0,

        /// <summary>O : Office Address is mailing adress</summary>
        [EnumMember(Value = "O")]
        Office = 1,

        /// <summary>C : Current Address is mailing adress</summary>   
        [EnumMember(Value = "C")]
        Current = 2,
    }

    public enum Gender
    {
        None = 0,
        Male = 1,
        Female = 2,
    }

    public enum AnyIDType
    {
        Citizen = 1,
        Mobile = 2,
        ChangeAnyID = 3,
        All = 4
    }

    public enum PromptPayChannel
    {
        KMA = 1,
        KOL = 2,
        KBOL = 3,
        ATM = 4,
        BRANCH = 5,
        DOTCOM = 6,
        PPA = 7,
        NONE = 9,
    }

    public enum PointOfInitiationQRC
    {
        QRStatic = 11,
        QRDynamic = 12,
    }

    #region RTP
    public enum ProxyType
    {
        [Description("")]
        NONE = 0,
        [Description("MSISDN")]
        MSISDN = 1,
        [Description("NATID")]
        NATID = 2,
        [Description("BILLERID")]
        BILLERID = 3,
        [Description("EWALLETID")]
        EWALLETID = 4
    }
    public enum RTPTxnType
    {
        [Description("null")]
        NONE = 0,
        [Description("REQUESTER")]
        REQUESTER = 1,
        [Description("RECEIVER")]
        RECEIVER = 2
    }
    public enum RTPType
    {
        [Description("")]
        NONE = 0,
        [Description("BP")]
        BillPayment = 1,
        [Description("CT")]
        CreditTransfer = 2
    }
    public enum RTPSendStatus
    {
        NONE = -1,

        /// <summary>
        /// เมื่อทำการสร้าง RTP แต่ยังไม่ถึง instruction date และสถานะจะเปลี่ยนเป็น SENT เมื่อส่งสำเร็จ หรือ ITMX_ERROR เมื่อส่งไม่สำเร็จ หรือ ITMX_REJECT เมื่อ ITMX reject
        /// </summary>
        PROCESSING = 0,

        /// <summary>
        /// เมื่อ RTP ปลายทางเป็นต่างธนาคาร และทำการส่งให้ ITMX แล้ว หรือปลายทางเป็นลูกค้าธนาคารกรุงศรีจะเป็นสถานะ New ด้านล่าง
        /// </summary>
        SENT = 1,

        /// <summary>
        /// เมื่อมีRTP เข้ามาในระบบ และลูกค้ายังไม่ได้กดเข้ามาดู
        /// </summary>
        NEW_INCOMING = 2,

        /// <summary>
        /// เมื่อลูกค้ารับทราบว่ามี RTP เข้ามาและกดดุรายละเอียดแล้ว
        /// </summary>
        VIEWED = 3,

        /// <summary>
        /// มีการชำระเงินแล้ว
        /// </summary>
        PAID = 4,

        /// <summary>
        /// มีการชำระเงินบางส่วน (Bill payment)
        /// </summary>
        PARTIAL_PAID = 5,

        /// <summary>
        /// เกินระยะเวลาการชำระเงิน
        /// </summary>
        OVERDUE = 6,

        /// <summary>
        /// กรณีที่ PAYMENT_DATE > EXPIRE_DATE
        /// </summary>
        PAID_AFTER_EXPIRE = 7,

        /// <summary>
        /// กรณีที่ลูกค้าไม่ประสงค์จะจ่าย
        /// </summary>
        IGNORE = 8,

        /// <summary>
        /// กรณีที่ ธนาคารอื่น ๆ ปฎิเสธ RTP ITMX จะ return response code ที่ไม่ใช่ 00 กรณีที่ ธนาคาร BAY และไม่เปิดใช้บริการ RTP จะแสดง response code 53 หรือ 55 ตาม Appendix C
        /// </summary>
        ITMX_REJECT = 9,

        /// <summary>
        /// กรณีเรียก services ITMX แล้วเกิด error ขึน
        /// </summary>
        ITMX_EXCEPTION = 10,

        /// <summary>
        /// ระบบจะนำ RTP ที่มีสถานะ ITMX_EXCEPTION มาแสดงบนหน้าจอแล้วให้ MAKER เลือกเพือ? resent RTP Instruction
        /// </summary>
        MAKER_RESEND = 11,

        /// <summary>
        /// Checker approve รายการที่รอ resent ที่ maker check มา
        /// </summary>
        CHECKER_APPROVE = 12,

        /// <summary>
        /// Checker Reject
        /// </summary>
        CHECKER_REJECT = 13,

        /// <summary>
        /// 
        /// </summary>
        PAID_OVER = 14,
    }
    #endregion

    public enum PageCode
    {
        NONE = 99,
        PHILOSOPHY = 0,
        QUESTIONNAIR = 1,
        RECOMMEND_SOLUTION = 2,
        RECOMMEND = 3,
        PASTPERFORMANCE = 4,
        FUNDDETAIL = 5,
        WEALTHPATH = 6,
        DASHBOARD = 7,
        EXISTING = 8,
        RECOMMEND_INVESTMENT = 9,
        ADD_ACCOUNT = 10,
        OPEN_ACCOUNT = 11,
    }

    public enum ActType
    {
        NONE = 99,
        ACCESS = 1,
        LOGOUT = 2,
    }

    public struct Formatting
    {
        public const string Date = "dd/MM/yyyy";
        public const string Time = "HH:mm:ss";
        public const string DateTime = "dd/MM/yyyy HH:mm:ss";
        public const string DecimalZeroPoints = "N0";
        public const string DecimalTwoPoints = "N2";
        public const string DecimalFourPoints = "N4";
        public const string DecimalFivePoints = "N5";
        public const string DecimalEightPoints = "N8";
    }
}