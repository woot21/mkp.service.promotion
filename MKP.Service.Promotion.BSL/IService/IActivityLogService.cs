﻿using System;
using MKP.Service.Promotion.DTO.Log;

namespace MKP.Service.Promotion.BSL.IService
{
    public interface IActivityLogService : Base.IBaseService, IDisposable
    {
        void SaveActivityLog(ActivityLogEntity request);
    }
}
