﻿using System;
using System.Collections.Generic;
using MKP.Service.Promotion.DTO;

namespace MKP.Service.Promotion.BSL.IService
{
    public interface IUsersService : Base.IBaseService
    {
        User Authenticate(string username, string password);
        List<User> GetAll();
    }
}
