﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using Trove.DTO;
using MKP.Service.Promotion.DTO;
//using MKP.Service.Promotion.GIL.Proxy;
using MKP.Service.Promotion.DTO.Enumeration;

namespace MKP.Service.Promotion.BSL.IService
{

    public interface ISystemConfigService : Base.IBaseService, IDisposable
    {
        SystemConfigEntity GetSystemConfig(string moduleCode, string configName);
        List<string> ListModuleCode();
        SystemConfigList ListSystemConfig(string moduleCode, string configName, int pageIndex, int pageSize, string orderBy);
        SystemConfigList ListSystemConfigByModuleCode(string moduleCode);
        bool AddSystemConfig(SystemConfigEntity config);
        bool UpdateSystemConfig(SystemConfigEntity config);
        bool DeleteSystemConfig(string moduleCode, string configName);
    }
}
