﻿using System;
using MKP.Service.Promotion.DTO;

namespace MKP.Service.Promotion.BSL.IService
{
    public interface IErrorMessageService : Base.IBaseService, IDisposable
    {
        ErrorMessageList ListErrorMessage();
        ErrorMessageEntity GetErrorMessageByGlobalCode(string globalCode);
        ErrorMessageEntity GetErrorMessageByOriginalCode(string originalCode);

    }
}
