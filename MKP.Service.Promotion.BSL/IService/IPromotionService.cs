﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MKP.Service.Promotion.DTO.Promotion;

namespace MKP.Service.Promotion.BSL.IService
{
    public interface IPromotionService : Base.IBaseService, IDisposable
    {
        //Task<PromotionList> ListPromotions(PromotionRequestEntity request);
        //PromotionRequestEntity GetPromotion(short PromotionCategory, short PromotionType, int MaxLimit, short DisplayType, int PageIndex, int PageSize);
        PromotionList ListPublishPromotion(PromotionRequestEntity request);
    }
}
