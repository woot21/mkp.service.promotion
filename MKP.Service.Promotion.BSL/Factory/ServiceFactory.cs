﻿using PSC.Frameworks.Utility.Core;
using System;

namespace MKP.Service.Promotion.BSL.Factory
{
    public sealed class ServiceFactory
    {

        public static IService.ISystemConfigService GetSystemConfigService()
        {
            try
            {
                return (IService.ISystemConfigService)Reflection.CreateObject<Service.SystemConfigService>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static IService.ISystemConfigService GetSystemConfigService(string language)
        {
            try
            {
                return (IService.ISystemConfigService)Reflection.CreateObject<Service.SystemConfigService>(language);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static IService.ISystemConfigService GetSystemConfigService(DTO.DeviceInfoObject device, string session)
        {
            try
            {
                return (IService.ISystemConfigService)Reflection.CreateObject<Service.SystemConfigService>(device, session);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IService.IErrorMessageService GetErrorMessageService()
        {
            try
            {
                return (IService.IErrorMessageService)Reflection.CreateObject<Service.ErrorMessageService>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static IService.IErrorMessageService GetErrorMessageService(string language)
        {
            try
            {
                return (IService.IErrorMessageService)Reflection.CreateObject<Service.ErrorMessageService>(language);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static IService.IErrorMessageService GetErrorMessageService(DTO.DeviceInfoObject device, string session)
        {
            try
            {
                return (IService.IErrorMessageService)Reflection.CreateObject<Service.ErrorMessageService>(device, session);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IService.IActivityLogService GetActivityLogService()
        {
            try
            {
                return (IService.IActivityLogService)Reflection.CreateObject<Service.ActivityLogService>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IService.IUsersService GetUsersService()
        {
            try
            {
                return (IService.IUsersService)Reflection.CreateObject<Service.UsersService>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //beem
        public static IService.IPromotionService GetListPublishPromotion()
        {
            try
            {
                return (IService.IPromotionService)Reflection.CreateObject<Service.PromotionService>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IService.IBannerService GetListPublicBanner()
        {
            try
            {
                return (IService.IBannerService)Reflection.CreateObject<Service.BannerService>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //
    }
}