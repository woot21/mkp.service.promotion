﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MKP.Service.Promotion.DAL.Factory;
using MKP.Service.Promotion.DAL.IData;
using MKP.Service.Promotion.DTO.Promotion;

namespace MKP.Service.Promotion.BSL.Service
{
    public class PromotionService : Base.BaseService, IService.IPromotionService
    {
        public PromotionList ListPublishPromotion(PromotionRequestEntity request)
        {
            PromotionList result = new PromotionList();
            try
            {
                using (IPromotionData iData = DataFactory.GetPromotionData())
                {
                    result = iData.ListPublishPromotion(request);

                    foreach (PromotionEntity url in result.Promotion_List)
                    {
                        if (string.IsNullOrEmpty(url.PromotionImageURL))
                        {
                            url.PromotionImageURL = "https://EMPTYPATH/";
                        }
                        else if ((url.PromotionImageURL.Substring(0, 5)).Equals("https"))
                        {
                            //
                        }
                        else
                        {
                            string BeginUrl = ReadAssetsSettings();
                            if (string.IsNullOrEmpty(BeginUrl))
                            {
                                BeginUrl = "https://EMPTYPATH/";
                            }
                            url.PromotionImageURL = BeginUrl + url.PromotionImageURL;
                        }
                    }
                }
                //throw new Exception();
                //return result;
            }
            catch (Exception ex)
            {
                //return null;
                result.Result = false;
                result.ErrorCode = "9999";
                result.ErrorDesc = "ListPublishPromotion : " + ex.Message;
                new BSLException(string.Format("ListPublishPromotion occur an error(s).[{0}]", ex.Message), true);
            }
            return result;
        }

        private string ReadAssetsSettings()
        {
            try
            {
                IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                IConfigurationRoot configuration = builder.Build();
                return configuration.GetSection("AssetsSettings:IMAGE_PATH").Value; //ลิงค์ URL ของเว็บ API
            }
            catch (Exception ex)
            {
                new BSLException("ReadAssetsSettings Error : " + ex, true);
                return null;
            }
        }
    }
}
