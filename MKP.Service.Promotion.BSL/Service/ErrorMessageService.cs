﻿using System;
using MKP.Service.Promotion.DAL.Factory;
using MKP.Service.Promotion.DAL.IData;
using MKP.Service.Promotion.DTO;

namespace MKP.Service.Promotion.BSL.Service
{
    public partial class ErrorMessageService : Base.BaseService, IService.IErrorMessageService
    {
        #region *** Methods section ***

        #region [ Public methods ]

        public ErrorMessageList ListErrorMessage()
        {
            try
            {
                using (IErrorMessageData iData = DataFactory.GetErrorMessageData())
                {
                    return iData.ListErrorMessage();
                }
            }
            catch (Exception ex)
            {
                throw new BSLException(this, "4S-01-0001", "ListErrorMessage method occurs an error.", ex, true);
            }
        }

        public ErrorMessageEntity GetErrorMessageByGlobalCode(string globalCode)
        {
            try
            {
                using (IErrorMessageData iData = DataFactory.GetErrorMessageData())
                {
                    return iData.GetErrorMessageByGlobalCode(globalCode);
                }
            }
            catch (Exception ex)
            {
                throw new BSLException(this, "4S-01-0002", "GetErrorMessageByGlobalCode method occurs an error.", ex, true);
            }
        }

        public ErrorMessageEntity GetErrorMessageByOriginalCode(string originalCode)
        {
            try
            {
                using (IErrorMessageData iData = DataFactory.GetErrorMessageData())
                {
                    return iData.GetErrorMessageByOriginalCode(originalCode);
                }
            }
            catch (Exception ex)
            {
                throw new BSLException(this, "4S-01-0003", "GetErrorMessageByOriginalCode method occurs an error.", ex, true);
            }
        }


        #endregion

        #endregion
    }
}
