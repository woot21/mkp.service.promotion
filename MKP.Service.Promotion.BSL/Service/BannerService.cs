﻿using Microsoft.Extensions.Configuration;
using MKP.Service.Promotion.DAL.Factory;
using MKP.Service.Promotion.DAL.IData;
using MKP.Service.Promotion.DTO.Promotion;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MKP.Service.Promotion.BSL.Service
{
    public class BannerService : Base.BaseService, IService.IBannerService
    {
        public BannerList ListPublicBanner(BannerRequestEntity request) 
        {
            BannerList result = new BannerList();
            try
            {
                using (IBannerData iData = DataFactory.GetBannerData())
                {
                    result = iData.ListPublicBanner(request);

                    foreach (BannerEntity url in result.Banner_List)
                    {
                        if (string.IsNullOrEmpty(url.CONTENT_NAME))
                        {
                            url.CONTENT_NAME = "https://EMPTYPATH/";
                        }
                        else if ((url.CONTENT_NAME.Substring(0, 5)).Equals("https"))
                        {
                            //
                        }
                        else
                        {
                            string BeginUrl = ReadAssetsSettings();
                            if (string.IsNullOrEmpty(BeginUrl))
                            {
                                BeginUrl = "https://EMPTYPATH/";
                            }
                            url.CONTENT_NAME = BeginUrl + url.CONTENT_NAME;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Result = false;
                result.ErrorCode = "9999";
                result.ErrorDesc = "ListPublicBanner : " + ex.Message;
                new BSLException(string.Format("ListPublicBanner occur an error(s).[{0}]", ex.Message), true);
            }
            return result;
        }

        private string ReadAssetsSettings()
        {
            try
            {
                IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
                IConfigurationRoot configuration = builder.Build();
                return configuration.GetSection("AssetsSettings:IMAGE_PATH").Value; //ลิงค์ URL ของเว็บ API
            }
            catch (Exception ex)
            {
                new BSLException("ReadAssetsSettings Error : " + ex, true);
                return null;
            }
        }
    }
}
