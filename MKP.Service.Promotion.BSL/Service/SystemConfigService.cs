﻿using System;
using System.Collections.Generic;
using System.Configuration;

using MKP.Service.Promotion.DAL.IData;
//using Trove.DTO;
using MKP.Service.Promotion.DTO;
using MKP.Service.Promotion.DAL.Factory;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Linq;
//using BAU.SMA.DTO;


using System.Text;

using System.Collections;
using System.Web;
//using BAY.SMA.GIL.EAI;
//using GIL = BAY.SMA.GIL;
//using BAY.SMA.GIL.Proxy;
using MKP.Service.Promotion.DTO.Enumeration;

namespace MKP.Service.Promotion.BSL.Service
{
    public class SystemConfigService : Base.BaseService, IService.ISystemConfigService
    {
        #region *** Properties section ***
        Dictionary<string, string> _config;
        public Dictionary<string, string> Config { get => _config; set => _config = value; }

        #endregion
        public SystemConfigService()
        {
        }

        public SystemConfigEntity GetSystemConfig(string moduleCode, string configName)
        {
            try
            {
                SystemConfigEntity systemConfig = new SystemConfigEntity();
                using (ISystemConfigData iData = DataFactory.GetSystemConfigData())
                    systemConfig = iData.GetSystemConfig(moduleCode, configName);
                return systemConfig;
            }
            catch (Exception ex)
            {
                throw new BSLException(string.Format("GetSystemConfig occur an error(s).[{0}]", ex.Message), true);
            }
        }
        public List<string> ListModuleCode()
        {
            try
            {
                List<string> moduleList = new List<string>();
                using (ISystemConfigData iData = DataFactory.GetSystemConfigData())
                    moduleList = iData.ListModuleCode();
                return moduleList;
            }
            catch (Exception ex)
            {
                throw new BSLException(string.Format("ListModuleCode occur an error(s).[{0}]", ex.Message), true);
            }
        }
        public SystemConfigList ListSystemConfig(string moduleCode, string configName, int pageIndex, int pageSize, string orderBy)
        {
            try
            {
                SystemConfigList configList = new SystemConfigList();
                using (ISystemConfigData iData = DataFactory.GetSystemConfigData())
                    configList = iData.ListSystemConfig(moduleCode, configName, pageIndex, pageSize, orderBy);
                return configList;
            }
            catch (Exception ex)
            {
                throw new BSLException("ListSystemConfig occurs an error.[" + ex.Message + "]", true);
            }
        }
        public SystemConfigList ListSystemConfigByModuleCode(string moduleCode)
        {
            try
            {
                SystemConfigList configList = new SystemConfigList();
                string[] temp = moduleCode.Split(',');
                string newModuleCode = "";
                foreach (string module in temp)
                {
                    newModuleCode += "'" + module + "',";
                }
                newModuleCode = newModuleCode.Substring(0, newModuleCode.Length - 1);
                using (ISystemConfigData iData = DataFactory.GetSystemConfigData())
                    configList = iData.ListSystemConfigByModuleCode(newModuleCode);
                return configList;

            }
            catch (Exception ex)
            {
                throw new BSLException(string.Format("ListSystemConfigByModuleCode occur an error(s).[{0}]", ex.Message), true);
            }
        }
        public bool AddSystemConfig(SystemConfigEntity config)
        {
            try
            {
                using (ISystemConfigData iData = DataFactory.GetSystemConfigData())
                    return iData.AddSystemConfig(config);
            }
            catch (Exception ex)
            {
                throw new BSLException(string.Format("AddSystemConfig occur an error(s).[{0}]", ex.Message), true);
            }
        }
        public bool UpdateSystemConfig(SystemConfigEntity config)
        {
            try
            {
                using (ISystemConfigData iData = DataFactory.GetSystemConfigData())
                    return iData.UpdateSystemConfig(config);
            }
            catch (Exception ex)
            {
                throw new BSLException(string.Format("UpdateSystemConfig occur an error(s).[{0}]", ex.Message), true);
            }
        }
        public bool DeleteSystemConfig(string moduleCode, string configName)
        {
            try
            {
                using (ISystemConfigData iData = DataFactory.GetSystemConfigData())
                    return iData.DeleteSystemConfig(moduleCode, configName);
            }
            catch (Exception ex)
            {
                throw new BSLException(string.Format("DeleteSystemConfig occur an error(s).[{0}]", ex.Message), true);
            }

        }
    }
}
