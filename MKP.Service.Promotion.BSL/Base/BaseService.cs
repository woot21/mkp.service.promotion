﻿using Microsoft.Extensions.Configuration;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
//using PSC.Frameworks.Token.Core;
using System.Runtime.Serialization;
using System.Text;
//using System.Web.SessionState;
//using PSC.Frameworks.SessionState;
using System.Xml;
using System.Xml.Serialization;
using MKP.Service.Promotion.DTO;

/// <summary>
/// Summary description for BaseService
/// </summary>
namespace MKP.Service.Promotion.BSL.Base
{
    public abstract class BaseService : IBaseService, IDisposable
    {
        #region *** Members section ***

        #region [ Private members ]

        private IFormatProvider theCultureInfo = new System.Globalization.CultureInfo("en-AU", true);
        private static readonly object _lockObject = new object();
        public bool isWriteLog;
        private string serverNo = (ConfigurationManager.AppSettings["Server_No"] != null ? ConfigurationManager.AppSettings["Server_No"].ToString() : "");
        private bool isProd = Convert.ToBoolean((ConfigurationManager.AppSettings["SMA_IsProduction"] != null) ? ConfigurationManager.AppSettings["SMA_IsProduction"].ToString() : "true");
        public string RoboMobileUrl = (ConfigurationManager.AppSettings["MobileRobo_WebServiceURL"] != null ? ConfigurationManager.AppSettings["MobileRobo_WebServiceURL"].ToString() : "");

        private bool _Disposed = false;

        private string _KEY_LANG = "__LANG";

        private string _KEY_DEVICE_ID = "__DEVICE_ID";
        private string _KEY_DEVICE_OS = "__DEVICE_OS";
        private string _KEY_MY_DEVICE_GUID = "__KEY_MY_DEVICE_GUID";
        private string _KEY_DEVICE_TOKEN = "__DEVICE_TOKEN";

        private string _KEY_REGISTER_DATE = "__REGISTER_DATE";
        private string _KEY_CITIZEN_ID = "__CITIZEN_ID";
        private string _KEY_CUST_INFO = "__CUST_INFO";
        private string _CUST_CIF_ID = "__CUSTOMER_CIF_ID";

        private string _KEY_MAIN_ACCOUNT = "__MAIN_ACCOUNT";
        private string _KEY_USER_LOGIN_BY = "__KEY_USER_LOGIN_BY";
        private string _KEY_ACCOUNT_ANYID_CASA = "__ACCOUNT_ANYID_CASA";

        //CR33-AYCAP_Session 21/01/2015
        /*
        public string _KEY_ACC_CREDIT_CARD_LIST = "__ACC_CREDIT_CARD_LIST";
        public string _KEY_ACC_CUST_KSE = "__ACC_CUST_KSE";   //Caching KSE 8MAY2017
        public string _KEY_HTTP_SMS_SESSION_INDEX = "__HTTP_SMS_SESSION_INDEX";
        public string _KEY_ACC_CREDIT_CARD_LIST_REST = "__ACC_CREDIT_CARD_LIST_REST";
        */

        //issue prod - black list device
        public string _KEY_BLACK_LIST_DEVICE_ = "__BLACK_LIST_DEVICE";

        // ScanToPay
        public string _KEY_PROMPTPAY_LIST_QRC = "___KEY_PROMPTPAY_LIST_QRC_______";

        // เก็บข้อมูล device ที่ผ่านการ check แล้ว
        public string _KEY_CHECK_DEVICE = "__SS_CHECK_MY_DEVICE_";

        // ----- ISSUE AUDIT ----- // begin
        protected const string KEY_FLOW_SETPIN = "__USER_FLOW_SETPIN";
        protected const string KEY_FLOW_NEW_DEVICE = "__USER_FLOW_NEW_DEVICE";

        protected const string KEY_LOGIN_PIN_SUCCESS = "__USER_LOGIN_PIN_SUCCESS";

        protected const string KEY_FLOW_OTP_PASS = "__USER_FLOW_OTP_PASS";
        protected const string KEY_FLOW_CF_PIN = "__USER_FLOW_CONFIRM_PIN";
        // ----- ISSUE AUDIT ----- // end

        /*
        // dgl check flow otp 
        private string _KEY_DGL_CONFIRM_STATE = "__KEY_DGL_CONFIRM_STATE";


        protected const string PAGE_SERVICE_GET_ACCOUNT = "CustomerService.asmx";
        */

        private static SystemConfigList _kolConfigList = null;
        public SystemConfigList KOLConfigList
        {
            get { return _kolConfigList; }
            set { _kolConfigList = value; }
        }
        /* public static string webServiceExistsCross = "http://192.168.35.49/BAY.KOL.WebService/";
        private string userWebService = "userKOLWS";
        private string passWebService = "passKOLWS"; */
        private static string _currentServerNo = "";
        //public string BZBNew_AppVersion { get; } = "3.2.1";

        //public HttpRequestBase RequestContext
        //{
        //    get
        //    {
        //        var context = new HttpContextWrapper(HttpContext.Current);
        //        HttpRequestBase request = context.Request;

        //        return request;
        //    }
        //}

        //public string Language { get; set; }
        protected string webServiceURL;
        #endregion

        public static string _msgErrException = "An error has occurred. We are sorry for the inconvenience, Please contact administrator for help.";

        #endregion

        #region *** Methods section ***

        #region [ Private methods ]

        private void Dispose(bool disposing)
        {
            if (!this._Disposed)
            {
                if (disposing)
                {
                }
            }
            _Disposed = true;
        }

        #endregion

        #region [ Public methods ]

        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected string ServerNo
        {
            get
            {
                string result = "";

                if (string.IsNullOrEmpty(_currentServerNo.GetValueOrDefault(serverNo.GetValueOrDefault().Trim()).Trim()))
                {
                    string ip = GetIP();

                    ip = ip.GetValueOrDefault().Trim();

                    if (string.IsNullOrEmpty(ip) == false)
                    {
                        // KMA 6 Server
                        // KOL & KBOL 2 Server
                        switch (ip)
                        {
                            case "192.168.22.1": // KMA
                            case "192.168.22.39": // KOL
                            case "192.168.45.4": // KOL & KMA (UAT)
                            case "192.168.35.49": // KOL & KMA (DEV)
                                result = "1";
                                break;
                            case "192.168.22.2": // KMA
                            case "192.168.22.40": // KOL
                                result = "2";
                                break;
                            case "192.168.22.5":
                                result = "3";
                                break;
                            case "192.168.22.6":
                                result = "4";
                                break;
                            case "192.168.22.37":
                                result = "5";
                                break;
                            case "192.168.22.38":
                                result = "6";
                                break;
                        }
                    }
                }
                else
                    result = _currentServerNo.GetValueOrDefault(serverNo.GetValueOrDefault().Trim()).Trim();

                if (string.IsNullOrEmpty(_currentServerNo.GetValueOrDefault().Trim()))
                    _currentServerNo = result;

                return result;
            }
        }

        #region Session

        //protected string SessionID
        //{
        //    get; set;
        //    //get
        //    //{
        //    //    string result = "";
        //    //    try
        //    //    {
        //    //        try
        //    //        {
        //    //            HttpSessionState ss = HttpContext.Current.Session;
        //    //            result = ss.SessionID;
        //    //        }
        //    //        catch (Exception ex)
        //    //        {
        //    //            result = "";
        //    //            if (isWriteLog)
        //    //                new BSLException(this, "3104", "GetSessionID event occurs an error.[" + ex.Message + "]", ex, true);
        //    //        }
        //    //    }
        //    //    catch (Exception ex)
        //    //    {
        //    //        result = "";
        //    //        if (isWriteLog)
        //    //            new BSLException(this, "3104", "GetSessionID event occurs an error.[" + ex.Message + "]", ex, true);
        //    //    }

        //    //    return result;
        //    //}

        //}

        //protected string Language
        //{
        //    get; set;
        //    //get
        //    //{
        //    //    if (!SessionManager.HasSession(_KEY_LANG))
        //    //    {
        //    //        return "EN";
        //    //    }
        //    //    else
        //    //    {
        //    //        //if (isWriteLog)
        //    //        //    new BSLException("GetSession-Language=" + SessionManager.GetSession<string>(_KEY_LANG), true);
        //    //        return SessionManager.GetSession<string>(_KEY_LANG);
        //    //    }
        //    //}
        //    //set
        //    //{
        //    //    SessionManager.SaveSession<string>(_KEY_LANG, value);
        //    //}
        //}

        //protected string DeviceID
        //{
        //    get; set;
        //    //get
        //    //{
        //    //    if (!SessionManager.HasSession(_KEY_DEVICE_ID))
        //    //    {
        //    //        return null;
        //    //    }
        //    //    else
        //    //    {
        //    //        //if (isWriteLog)
        //    //        //    new BSLException("GetSession-DeviceID=" + SessionManager.GetSession<string>(_KEY_DEVICE_ID), true);
        //    //        return SessionManager.GetSession<string>(_KEY_DEVICE_ID);
        //    //    }
        //    //}
        //    //set
        //    //{
        //    //    SessionManager.SaveSession<string>(_KEY_DEVICE_ID, value);
        //    //}
        //}

        //public string MyDeviceOS
        //{
        //    get; set;
        //    //get
        //    //{
        //    //    if (!SessionManager.HasSession(_KEY_DEVICE_OS))
        //    //    {
        //    //        return "";
        //    //    }
        //    //    else
        //    //    {
        //    //        return SessionManager.GetSession<string>(_KEY_DEVICE_OS);
        //    //    }
        //    //}
        //    //set
        //    //{
        //    //    if (value != null)
        //    //        SessionManager.SaveSession<string>(_KEY_DEVICE_OS, value);
        //    //}
        //}

        //protected string DeviceToken
        //{
        //    get; set;
        //    //get
        //    //{
        //    //    if (!SessionManager.HasSession(_KEY_DEVICE_TOKEN))
        //    //    {
        //    //        return null;
        //    //    }
        //    //    else
        //    //    {
        //    //        //if (isWriteLog)
        //    //        //    new BSLException("GetSession-DeviceToken=" + SessionManager.GetSession<string>(_KEY_DEVICE_TOKEN), true);
        //    //        return SessionManager.GetSession<string>(_KEY_DEVICE_TOKEN);
        //    //    }
        //    //}
        //    //set
        //    //{
        //    //    SessionManager.SaveSession<string>(_KEY_DEVICE_TOKEN, value);
        //    //}
        //}

        //protected DateTime? RegisterDate
        //{
        //    get
        //    {
        //        if (!SessionManager.HasSession(_KEY_REGISTER_DATE))
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            if (isWriteLog)
        //                new BSLException("GetSession-RegisterDate=" + SessionManager.GetSession<DateTime?>(_KEY_REGISTER_DATE), true);
        //            return SessionManager.GetSession<DateTime?>(_KEY_REGISTER_DATE);
        //        }
        //    }
        //    set
        //    {
        //        if (value != null)
        //            SessionManager.SaveSession<DateTime?>(_KEY_REGISTER_DATE, value);
        //    }
        //}

        //protected string CitizenID
        //{
        //    get
        //    {
        //        if (!SessionManager.HasSession(_KEY_CITIZEN_ID))
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            if (isWriteLog)
        //                new BSLException("GetSession-CitizenID=" + SessionManager.GetSession<string>(_KEY_CITIZEN_ID), true);
        //            return SessionManager.GetSession<string>(_KEY_CITIZEN_ID);
        //        }
        //    }
        //    set
        //    {
        //        SessionManager.SaveSession<string>(_KEY_CITIZEN_ID, value);
        //    }
        //}

        //protected CustInfoBusEntity CustInfoBusEntity
        //{
        //    get
        //    {
        //        if (!SessionManager.HasSession(_KEY_CUST_INFO))
        //        {
        //            return new CustInfoBusEntity();
        //        }
        //        else
        //        {
        //            //if (isWriteLog)
        //            //    new BSLException("GetSession-CustInfoBusEntity=" + Serialize(SessionManager.GetSession<CustInfoBusEntity>(_KEY_CUST_INFO)), true);
        //            return SessionManager.GetSession<CustInfoBusEntity>(_KEY_CUST_INFO);
        //        }
        //    }
        //    set
        //    {
        //        SessionManager.SaveSession<CustInfoBusEntity>(_KEY_CUST_INFO, value);
        //    }
        //}

        //protected string MainAccount
        //{
        //    get
        //    {
        //        if (!SessionManager.HasSession(_KEY_MAIN_ACCOUNT))
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            if (isWriteLog) new BSLException("GetSession-MainAccount=" + SessionManager.GetSession<string>(_KEY_MAIN_ACCOUNT), true);
        //            return SessionManager.GetSession<string>(_KEY_MAIN_ACCOUNT);
        //        }
        //    }
        //    set
        //    {
        //        if (isWriteLog)
        //            new BSLException("SaveSession-MainAccount=" + value, true);
        //        SessionManager.SaveSession<string>(_KEY_MAIN_ACCOUNT, value);
        //    }
        //}

        //protected string AccountANYID_Casa
        //{
        //    get
        //    {
        //        if (!SessionManager.HasSession(_KEY_ACCOUNT_ANYID_CASA))
        //        {
        //            return null;
        //        }
        //        else
        //        {
        //            if (isWriteLog) new BSLException("GetSession-AccountANYID_Casa=" + SessionManager.GetSession<string>(_KEY_MAIN_ACCOUNT), true);
        //            return SessionManager.GetSession<string>(_KEY_ACCOUNT_ANYID_CASA);
        //        }
        //    }
        //    set
        //    {
        //        if (isWriteLog)
        //            new BSLException("SaveSession-AccountANYID_Casa=" + value, true);
        //        SessionManager.SaveSession<string>(_KEY_ACCOUNT_ANYID_CASA, value);
        //    }
        //}

        //public T GetSession<T>(string key)
        //{
        //    try
        //    {
        //        return SessionManager.GetSession<T>(key);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new BSLException(this, "3104", "GetSession event occurs an error.[" + ex.Message + "]", ex, true);
        //    }
        //}

        //public void SaveSession<T>(string key, object obj)
        //{
        //    try
        //    {
        //        SessionManager.SaveSession<T>(key, (T)obj);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new BSLException(this, "3104", "SaveSession event occurs an error.[" + ex.Message + "]", ex, true);
        //    }
        //}

        //public void RemoveSession(string key)
        //{
        //    try
        //    {
        //        if (SessionManager.HasSession(key))
        //            SessionManager.RemoveSession(key);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new BSLException(this, "3104", "RemoveSession event occurs an error.[" + ex.Message + "]", ex, true);
        //    }
        //}

        //public string CurrentCustomerID
        //{
        //    get
        //    {
        //        if (!SessionManager.HasSession(_KEY_CUST_INFO))
        //        {
        //            return "";
        //        }
        //        else
        //        {
        //            try
        //            {
        //                return SessionManager.GetSession<CustInfoBusEntity>(_KEY_CUST_INFO).CustomerID;
        //            }
        //            catch { return ""; }
        //        }
        //    }
        //}

        //public string CIF_ID
        //{
        //    get
        //    {
        //        if (!SessionManager.HasSession(_CUST_CIF_ID))
        //        {
        //            return "";
        //        }
        //        else
        //        {
        //            return SessionManager.GetSession<string>(_CUST_CIF_ID);
        //        }
        //    }
        //    set
        //    {
        //        if (value != null)
        //            SessionManager.SaveSession<string>(_CUST_CIF_ID, value);
        //    }
        //}

        //protected string MyDeviceGUID //newkma
        //{
        //    get; set;
        //    //get
        //    //{
        //    //    if (!SessionManager.HasSession(_KEY_MY_DEVICE_GUID))
        //    //    {
        //    //        return "";
        //    //    }
        //    //    else
        //    //    {
        //    //        return SessionManager.GetSession<string>(_KEY_MY_DEVICE_GUID);
        //    //    }
        //    //}
        //    //set
        //    //{
        //    //    if (value != null)
        //    //        SessionManager.SaveSession<string>(_KEY_MY_DEVICE_GUID, value);
        //    //}
        //}

        //protected string UserLoginBy
        //{
        //    get
        //    {
        //        //if (isKMAProd == false)
        //        //    return LoginBy.Button.ToString().ToLower(); //button แปลว่ากดมาจากหน้า login

        //        if (!SessionManager.HasSession(_KEY_USER_LOGIN_BY))
        //        {
        //            return LoginBy.None.ToString().ToLower();
        //        }
        //        else
        //        {
        //            return SessionManager.GetSession<string>(_KEY_USER_LOGIN_BY);
        //        }
        //    }
        //    set
        //    {
        //        if (value != null) { value = value.ToLower(); }

        //        SessionManager.SaveSession<string>(_KEY_USER_LOGIN_BY, value);
        //    }
        //}

        //protected string Opercarrier { get; set; }

        //protected string MacAddress { get; set; }

        //protected string DeviceSerialNo { get; set; }

        //protected string DeviceOSVersion { get; set; }

        //protected string AppVersion { get; set; }

        //protected string DeviceModel { get; set; }

        //protected string DeviceHardware { get; set; }
        #endregion

        #region [ XML ]

        public static DataSet ConvertXmlStringToDataSet(string xmlString)
        {
            DataSet ds = new DataSet();
            try
            {
                XmlDocument xm = new XmlDocument();
                xm.LoadXml(xmlString.Trim());
                ds.EnforceConstraints = false;
                ds.ReadXml(new XmlNodeReader(xm));

                return ds;
            }
            catch (Exception ex)
            {
                throw new Exception("ConvertXmlStringToDataSet event occurs an error.[" + ex.Message + "]");
            }
        }

        private static String UTF8ByteArrayToString(Byte[] characters)
        {

            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        private static Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }

        public static string Serialize<T>(T obj)
        {
            try
            {
                String XmlizedString = null;
                MemoryStream memoryStream = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(obj.GetType());
                XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
                xs.Serialize(xmlTextWriter, obj);
                memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
                XmlizedString = UTF8ByteArrayToString(memoryStream.ToArray());
                return XmlizedString.Trim();
            }
            catch (Exception e) { Console.WriteLine(e); return null; }
        }

        public static string Serialize<T>(T obj, Type[] types)
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(obj.GetType(), types);
                StringWriter stringWriter = new StringWriter();
                xs.Serialize(stringWriter, obj);
                stringWriter.Flush();
                return stringWriter.ToString().Trim();
            }
            catch (Exception e) { Console.WriteLine(e); return null; }
        }

        public static T Deserialize<T>(String pXmlizedString)
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
                StringReader stringReader = new StringReader(pXmlizedString);
                return (T)xs.Deserialize(stringReader);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static T Deserialize<T>(String pXmlizedString, Type[] types)
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(T), types);
                StringReader stringReader = new StringReader(pXmlizedString);
                return (T)xs.Deserialize(stringReader);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region [ Unix DateTime ]

        public static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            try
            {
                //DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                DateTime origin = new DateTime(1970, 1, 1, 7, 0, 0, 0);
                return origin.AddSeconds(timestamp);
            }
            catch (Exception ex)
            {
                throw new Exception("ConvertFromUnixTimestamp event occurs an error.[" + ex.Message + "]");
            }
        }

        //public static double ConvertToUnixTimestamp(DateTime date)
        //{
        //    try
        //    {
        //        DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        //        TimeSpan diff = date - origin;
        //        return Math.Floor(diff.TotalSeconds);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("ConvertToUnixTimestamp event occurs an error.[" + ex.Message + "]");
        //    }
        //}     

        #endregion

        #region [ IP ]

        public static string GetIP()
        {
            string ipAddress = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(ipAddress) || ipAddress == "127.1.1.1")
                {
                    string strHostName = "";
                    strHostName = System.Net.Dns.GetHostName();
                    IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
                    IPAddress[] addr = ipEntry.AddressList;
                    //return addr[addr.Length - 1].ToString();
                    if (addr.Length > 0)
                        return addr[0].ToString();
                    else
                        return "127.1.1.1";
                }
                else
                    return ipAddress;
            }
            catch //(Exception ex)
            {
                return "127.1.1.1";
            }
        }

        #endregion

        #region [ Result Error ]

        #region Comment
        //public static string[] MappingResult(string responseCode, ErrorMessageList errList, string configName, SystemConfigList sysList)
        //{
        //    bool notFound = false;
        //    string[] ret = new string[2];
        //    try
        //    {
        //        if (errList != null && errList.Error != null && errList.Error.Count > 0)
        //        {
        //            List<ErrorMessageEntity> errEntity = errList.Error.Where(e => !string.IsNullOrEmpty(e.ResponseCode) && e.ResponseCode.Equals(responseCode)).ToList();
        //            if (errEntity != null && errEntity.Count > 0)
        //            {
        //                ret[0] = errEntity[0].ResponseCode;
        //                ret[1] = errEntity[0].MobileMessage;
        //            }
        //            else
        //                notFound = true;
        //        }
        //        else
        //            notFound = true;

        //        if (notFound.Equals(true))
        //        {
        //            if (sysList != null && sysList.Configs != null && sysList.Configs.Count > 0)
        //            {
        //                List<SystemConfigEntity> lSysEntity = sysList.Configs.Where(s => !string.IsNullOrEmpty(s.ConfigName) && s.ConfigName.ToUpper().Equals(configName.ToUpper())).ToList();
        //                if (lSysEntity != null && lSysEntity.Count > 0)
        //                {
        //                    string[] data = lSysEntity[0].ConfigValue.Split('|');
        //                    ret[0] = data[0];
        //                    ret[1] = data[1];
        //                    notFound = false;
        //                }
        //                else
        //                    notFound = true;
        //            }
        //            else
        //                notFound = true;
        //        }

        //        if (notFound.Equals(true))
        //        {
        //            //ret[0] = "999999";
        //            //ret[1] = "Response message does not match [" + responseCode + "]";
        //            //70999	UNDEFINED ERROR CODE	COMMUNICATION FAILED
        //            List<ErrorMessageEntity> errEntity = errList.Error.Where(e => !string.IsNullOrEmpty(e.ResponseCode) && e.ResponseCode.Equals(70999)).ToList();
        //            if (errEntity != null && errEntity.Count > 0)
        //            {
        //                ret[0] = errEntity[0].ResponseCode;
        //                ret[1] = errEntity[0].MobileMessage;
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new BSLException("MappingResult occurs an error.[" + ex.Message + "]", ex, true);
        //    }

        //    return ret;
        //}
        //public static string[] MappingResult(GateWayMessageCode gateway, string moduleName, ErrorMessageList errList)
        //{
        //    string[] ret = new string[2];
        //    try
        //    {
        //        if (sysList != null && sysList.Configs != null && sysList.Configs.Count > 0)
        //        {
        //            List<SystemConfigEntity> lSysEntity = sysList.Configs.Where(s => !string.IsNullOrEmpty(s.ConfigName) && s.ConfigName.ToUpper().Equals(moduleName.ToUpper())).ToList();
        //            if (lSysEntity != null && lSysEntity.Count > 0)
        //            {
        //                string[] data = lSysEntity[0].ConfigValue.Split('|');
        //                ret[0] = data[0];
        //                ret[1] = data[1];
        //            }
        //        }
        //        else
        //        {
        //            //Error Code
        //            ret[0] = gateway.GetHashCode().ToString();

        //            if (GateWayMessageCode.Success.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Success";
        //            else if (GateWayMessageCode.DataNotFound.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Data not found.";
        //            else if (GateWayMessageCode.InvalidParameter.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Invalid Parameter.";
        //            else if (GateWayMessageCode.Other.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "System Error.";
        //            else if (GateWayMessageCode.OTPVerifyFailed.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Verify OTP Failed.";
        //            else if (GateWayMessageCode.OTPExpired.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "OTP Expired.";
        //            else if (GateWayMessageCode.OTPLocked.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "OTP Locked.";
        //            else if (GateWayMessageCode.OTPRequestFail.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Request OTP failed.";
        //            else if (GateWayMessageCode.FailedDataNotMatch.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Data not match.";
        //            else if (GateWayMessageCode.FailedAgeBelow.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "You have to be at least 15 years old to register.";
        //            else if (GateWayMessageCode.FailedAlready.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Data already.";
        //            else if (GateWayMessageCode.TransactoionFailed.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Transaction Failed.";
        //            else if (GateWayMessageCode.GatewayFailed.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Gateway Failed.";
        //            else if (GateWayMessageCode.ResponseIsNullOrEmpty.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Response is null or empty.";
        //            else if (GateWayMessageCode.RequestIsNullOrEmpty.GetHashCode().Equals(gateway.GetHashCode()))
        //                ret[1] = "Request is null or empty.";

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new BSLException("MappingResult occurs an error.[" + ex.Message + "]", ex, true);
        //    }

        //    return ret;
        //}
        #endregion

        //public static string[] MappingResult(string responseCode, ErrorMessageList errList)
        //{
        //    return MappingResult(responseCode, null, errList);
        //}
        //public static string[] MappingResult(string responseCode, string responseMessage, ErrorMessageList errList)
        //{
        //    string[] ret = new string[2];
        //    try
        //    {
        //        responseCode = responseCode.Trim();
        //        if (errList != null && errList.Error != null && errList.Error.Count > 0)
        //        {
        //            using (IErrorMessageData iData = DataFactory.GetErrorMessageData())
        //            {
        //                errList = iData.ListErrorMessage();
        //            }
        //        }
        //        if (errList != null && errList.Error != null && errList.Error.Count > 0)
        //        {
        //            List<ErrorMessageEntity> errEntity = errList.Error.Where(e => !string.IsNullOrEmpty(e.ResponseCode) && e.ResponseCode.Equals(responseCode)).ToList();
        //            if (errEntity != null && errEntity.Count > 0)
        //            {
        //                ret[0] = errEntity[0].ResponseCode;
        //                ret[1] = errEntity[0].MobileMessage;
        //            }
        //            else
        //            {

        //                ret[0] = responseCode;
        //                ret[1] = responseCode + "-" + responseMessage;
        //                //errEntity = errList.Error.Where(e => !string.IsNullOrEmpty(e.ResponseCode) && e.ResponseCode.Equals("70999")).ToList();
        //                //if (errEntity != null && errEntity.Count > 0)
        //                //{
        //                //    ret[0] = errEntity[0].ResponseCode;
        //                //    ret[1] = errEntity[0].MobileMessage;
        //                //}
        //            }
        //        }
        //        else
        //        {
        //            //70999	UNDEFINED ERROR CODE COMMUNICATION FAILED
        //            //ret[0] = "70999";
        //            //ret[1] = "COMMUNICATION FAILED";

        //            ret[0] = responseCode;
        //            ret[1] = responseCode + "-" + responseMessage;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        //new BSLException("MappingResult occurs an error.[" + ex.Message + "]", ex, true);
        //    }

        //    return ret;
        //}
        #endregion

        #endregion

        #endregion

        #region *** Constructor section ***

        public BaseService()
        {
            isWriteLog = GetSectionValue("AppSettings:TRV_IsWriteLog") != null ? Convert.ToBoolean(GetSectionValue("AppSettings:TRV_IsWriteLog")) : false; //(ConfigurationManager.AppSettings["SMA_IsWriteLog"] != null ? Convert.ToBoolean(ConfigurationManager.AppSettings["SMA_IsWriteLog"].ToString()) : false);
        }

        #endregion

        #region *** Destructor section ***

        ~BaseService()
        {
            Dispose(false);
        }

        #endregion

        //protected string SendQueryString(params object[] parms)
        //{
        //    return "?token=" + PSCToken.Encode(parms);
        //}

        protected string GetSectionValue(string sectionName)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            return configuration.GetSection(sectionName).Value;
        }

        //public PIN_OTP_Flow MappingTransactionToPINTxn(TransactionType tranType)
        //{
        //    PIN_OTP_Flow result = PIN_OTP_Flow.NONE;

        //switch (tranType)
        //{
        //    case TransactionType.TransferP2PMobileOnUs:
        //    case TransactionType.TransferP2PMobileOffUs:
        //    case TransactionType.TransferP2PCitizenOnUs:
        //    case TransactionType.TransferP2PCitizenOffUs:
        //    case TransactionType.TransferP2PMobile:
        //    case TransactionType.TransferP2PCitizen:
        //        result = PIN_OTP_Flow.TransferP2P;
        //        break;
        //    case TransactionType.TransferBay3rd:
        //        result = PIN_OTP_Flow.TransferBay3rd;
        //        break;
        //    case TransactionType.TransferORFT3rd:
        //        result = PIN_OTP_Flow.TransferORFT3rd;
        //        break;
        //    case TransactionType.BillPaymentKSS:
        //    case TransactionType.BillPayment:
        //        result = PIN_OTP_Flow.BillPayment;
        //        break;
        //    case TransactionType.MobileTopup:
        //        result = PIN_OTP_Flow.MobileTopup;
        //        break;
        //    case TransactionType.RegisterAnyIDByPID:
        //    case TransactionType.RegisterAnyIDByMobile:
        //        result = PIN_OTP_Flow.RegisterAnyID;
        //        break;
        //    case TransactionType.LoanWithDraw:
        //        result = PIN_OTP_Flow.LoanWithDraw;
        //        break;
        //    case TransactionType.ApplyAutoDebit:
        //        result = PIN_OTP_Flow.ApplyAutoDebit;
        //        break;
        //    case TransactionType.RequestCreditBureau:
        //        result = PIN_OTP_Flow.RequestCreditBureau;
        //        break;
        //    case TransactionType.LoanOther:
        //        result = PIN_OTP_Flow.LoanOther;
        //        break;
        //    case TransactionType.AYCAPGetCashInstallment:
        //        result = PIN_OTP_Flow.AYCAPGetCashInstallment;
        //        break;
        //    case TransactionType.AYCAPGetCashRevolving:
        //        result = PIN_OTP_Flow.AYCAPGetCashRevolving;
        //        break;
        //    case TransactionType.ApplySMSBanking:
        //        result = PIN_OTP_Flow.RegisterSMSBanking;
        //        break;
        //    case TransactionType.MobileAtATM:
        //        result = PIN_OTP_Flow.MobileAtATM;
        //        break;
        //    case TransactionType.TopupeWallet:
        //        result = PIN_OTP_Flow.TopupEWallet;
        //        break;
        //    case TransactionType.ScanToTransfer:
        //        result = PIN_OTP_Flow.ScanTransfer;
        //        break;
        //    case TransactionType.ScanToEWallet:
        //        result = PIN_OTP_Flow.ScanEWallet;
        //        break;
        //    case TransactionType.ScanToPay:
        //        result = PIN_OTP_Flow.ScanToPay;
        //        break;
        //    case TransactionType.TransferRequestRTP:
        //        result = PIN_OTP_Flow.TransferRequestRTP;
        //        break;
        //    case TransactionType.PaymentRequestRTP:
        //        result = PIN_OTP_Flow.PaymentRequestRTP;
        //        break;

        //    }

        //        return result;
        //    }

        //private static object _objectReq = new object();

        //#region *** Setting Data to Entity ***


        //public int SettingDataInt32(string col)
        //{
        //    int intResult = 0;

        //    if (string.IsNullOrEmpty(col) == false)
        //    {
        //        int _dataInt = -1;
        //        int.TryParse(col, out _dataInt);

        //        if (_dataInt > 0) intResult = _dataInt;
        //    }

        //    return intResult;
        //}

        //public decimal SettingDataDecimal(string col)
        //{
        //    decimal dexResult = 0.00m;

        //    if (string.IsNullOrEmpty(col) == false)
        //    {
        //        decimal _dataDex = -1;
        //        decimal.TryParse(col, out _dataDex);

        //        if (_dataDex > 0)
        //            dexResult = _dataDex;
        //    }

        //    return dexResult;
        //}

        //public DateTime SettingDataDateTime(string col)
        //{
        //    DateTime datetime = DateTime.Now;

        //    if (string.IsNullOrEmpty(col) == false)
        //    {
        //        DateTime _datetime = new DateTime();
        //        try
        //        {
        //            _datetime = DateTime.ParseExact(col, "d", null);
        //        }
        //        catch (Exception)
        //        {
        //            return datetime;
        //        }
        //        datetime = _datetime;
        //    }

        //    return datetime;
        //}

        //public bool SettingDataBool(string col)
        //{
        //    bool Bool = false;
        //    if (string.IsNullOrEmpty(col) == false)
        //    {
        //        bool _bool = false;
        //        _bool = bool.Parse(col);

        //        Bool = _bool;
        //    }

        //    return Bool;
        //}

        ////Fix Int Nullable Type
        //public int? SettingDataIntNullable(string col)
        //{
        //    //return int.Parse(Null.SetNull(col, typeof(int)).ToString());
        //    //return (int?)Null.SetNull(((object)col), typeof(int?));
        //    int? intResult = null;

        //    if (string.IsNullOrEmpty(col) == false)
        //    {
        //        int _dataInt = -1;
        //        int.TryParse(col, out _dataInt);

        //        if (_dataInt > 0) intResult = _dataInt;
        //    }

        //    return intResult;
        //}

        ////Fix DateTime Nullable Type
        //public static DateTime? SettingDataDateTimeNullable(string col)
        //{
        //    //return DateTime.Parse(Null.SetNull(col, typeof(DateTime)).ToString());
        //    DateTime? datetime = null;

        //    if (string.IsNullOrEmpty(col) == false)
        //    {
        //        DateTime _datetime = new DateTime();
        //        try
        //        {
        //            _datetime = DateTime.ParseExact(col, "d", null);
        //        }
        //        catch (Exception)
        //        {
        //            return datetime;
        //        }


        //        datetime = _datetime;
        //    }

        //    return datetime;
        //}

        ////Fix decimal Nullable Type
        //public decimal? SettingDataDecimalNullable(string col)
        //{
        //    //return decimal.Parse(Null.SetNull(col, typeof(decimal)).ToString());
        //    //return (decimal?)Null.SetNull(col, typeof(decimal?));
        //    decimal? dexResult = null;

        //    if (string.IsNullOrEmpty(col) == false)
        //    {
        //        decimal _dataDex = -1;
        //        decimal.TryParse(col, out _dataDex);

        //        if (_dataDex > 0)
        //            dexResult = _dataDex;
        //    }

        //    return dexResult;
        //}

        //public static bool? SettingDataBoolNullable(string col)
        //{
        //    //return bool.Parse(Null.SetNull(col, typeof(bool)).ToString());
        //    bool? Bool = null;
        //    if (string.IsNullOrEmpty(col) == false)
        //    {
        //        bool _bool = false;
        //        _bool = bool.Parse(col);

        //        Bool = _bool;
        //    }

        //    return Bool;
        //}

        //#endregion

        //private int? MappintTranTypeToActType(TransactionType txntype)
        //{
        //    int? acttype = null;
        //    #region Map Activity Type

        //    switch (txntype)
        //    {
        //        case TransactionType.TransferOwner:
        //            acttype = ActivityType.TransferOwner.GetHashCode();
        //            break;
        //        case TransactionType.TransferBay3rd:
        //            acttype = ActivityType.TransferBay3rd.GetHashCode();
        //            break;
        //        case TransactionType.TransferCardless:
        //            acttype = ActivityType.TransferCardless.GetHashCode();
        //            break;
        //        case TransactionType.TransferORFT3rd:
        //            acttype = ActivityType.TransferORFT3rd.GetHashCode();
        //            break;
        //        case TransactionType.BillPayment:
        //            acttype = ActivityType.BillPayment.GetHashCode();
        //            break;
        //        case TransactionType.BillPaymentKSS:
        //            acttype = ActivityType.BillPayment.GetHashCode();
        //            break;
        //        case TransactionType.MobileTopup:
        //            acttype = ActivityType.MobileTopup.GetHashCode();
        //            break;
        //        case TransactionType.RedeemCash:
        //            acttype = ActivityType.RedeemCash.GetHashCode();
        //            break;
        //        case TransactionType.RedeemGift:
        //            acttype = ActivityType.RedeemGift.GetHashCode();
        //            break;
        //        case TransactionType.BuyFund:
        //            acttype = ActivityType.BuyFund.GetHashCode();
        //            break;
        //        case TransactionType.SellFund:
        //            acttype = ActivityType.SellFund.GetHashCode();
        //            break;
        //        case TransactionType.SwitchFund:
        //            acttype = ActivityType.SwitchFund.GetHashCode();
        //            break;
        //        case TransactionType.AYCAPRedemption:
        //            acttype = ActivityType.AYCAPRedemption.GetHashCode();
        //            break;
        //        case TransactionType.AYCAPGetCashRevolving:
        //            acttype = ActivityType.AYCAPGetCashRevolving.GetHashCode();
        //            break;
        //        case TransactionType.AYCAPGetCashInstallment:
        //            acttype = ActivityType.AYCAPGetCashInstallment.GetHashCode();
        //            break;
        //        case TransactionType.MobileAtATM:
        //            acttype = ActivityType.MobileATM.GetHashCode();
        //            break;
        //        case TransactionType.ApplyAutoDebit:
        //            acttype = ActivityType.ApplyAutoDebit.GetHashCode();
        //            break;
        //        case TransactionType.RequestCreditBureau:
        //            acttype = ActivityType.RequestBureau.GetHashCode();
        //            break;
        //        case TransactionType.ApplySMSBanking:
        //            acttype = ActivityType.ApplySMSBanking.GetHashCode();
        //            break;
        //        case TransactionType.TopupeWallet:
        //            acttype = ActivityType.TopupeWallet.GetHashCode();
        //            break;
        //        case TransactionType.ScanToTransfer:
        //            acttype = ActivityType.ScanToTransfer.GetHashCode();
        //            break;
        //        case TransactionType.ScanToEWallet:
        //            acttype = ActivityType.ScanToEWallet.GetHashCode();
        //            break;
        //        case TransactionType.TransferP2PCitizenOffUs:
        //            acttype = ActivityType.TransferP2PCitizenOffUs.GetHashCode();
        //            break;
        //        case TransactionType.TransferP2PCitizenOnUs:
        //            acttype = ActivityType.TransferP2PCitizenOnUs.GetHashCode();
        //            break;
        //        case TransactionType.TransferP2PMobileOffUs:
        //            acttype = ActivityType.TransferP2PMobileOffUs.GetHashCode();
        //            break;
        //        case TransactionType.TransferP2PMobileOnUs:
        //            acttype = ActivityType.TransferP2PMobileOnUs.GetHashCode();
        //            break;
        //        case TransactionType.TransferP2PCitizen:
        //            acttype = ActivityType.TransferP2PCitizen.GetHashCode();
        //            break;
        //        case TransactionType.TransferP2PMobile:
        //            acttype = ActivityType.TransferP2PMobile.GetHashCode();
        //            break;
        //        case TransactionType.GenerateQRC:
        //            acttype = ActivityType.GenerateQRC.GetHashCode();
        //            break;
        //        case TransactionType.ScanToPay:
        //            acttype = ActivityType.ScanToPay.GetHashCode();
        //            break;
        //        case TransactionType.LoanOther:
        //            acttype = ActivityType.LoanOther.GetHashCode();
        //            break;
        //        case TransactionType.LoanOwner:
        //            acttype = ActivityType.LoanOwner.GetHashCode();
        //            break;
        //        case TransactionType.LoanWithDraw:
        //            acttype = ActivityType.LoanWithDraw.GetHashCode();
        //            break;
        //        default:
        //            break;
        //    }

        //    #endregion
        //    return acttype;
        //}

        public static int MatchStringCount(string text, string pattern)
        {
            // Loop through all instances of the string 'text'.
            int count = 0;
            int i = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }
            return count;
        }


    }
}






namespace MKP.Service.Promotion.BSL
{
    public static class BaseClassStatic
    {
        public static string GetValueOrDefault(this string value)
        {
            return value.GetValueOrDefault("");
        }

        public static string GetValueOrDefault(this string value, string defaultValue)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
                return defaultValue;
            else
                return value;
        }

        public static T Clone<T>(this T source)
        {
            var dcs = new DataContractSerializer(typeof(T));
            using (var ms = new System.IO.MemoryStream())
            {
                dcs.WriteObject(ms, source);
                ms.Seek(0, System.IO.SeekOrigin.Begin);
                return (T)dcs.ReadObject(ms);
            }
        }

        public static bool IsNullOrEmpty(this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return true;
            }
            return string.IsNullOrEmpty(value.Trim());
        }

        public static bool EqualWithUpperCase(this string value, string compareValue)
        {
            return value.GetValueOrDefault("").Trim().ToUpper() == compareValue.GetValueOrDefault().Trim().ToUpper();
        }

        public static string ToDescriptionEnumString<T>(this T val)
        {
            string result = "";

            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                result = attributes[0].Description;
            else
                result = string.Empty;

            return result;
        }

    }
}